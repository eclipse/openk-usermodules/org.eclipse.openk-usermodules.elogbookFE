/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { User } from '../model/user';

export class UserMap {
  private mappedUsers: User[];

  constructor(allUsers: User[]) {
    this.mappedUsers = [];
    if (!allUsers) {
      console.log('UserMap was created without any Users!');
      throw new EvalError('UserMap was created without any Users!');
    }
    this.mappedUsers = allUsers;
  }

  public findUser(usrShort: string): User | undefined {
    return this.mappedUsers.find(usr => usr.username === usrShort);
  }

  public findAndRenderUser(shortUsr: string | null | undefined): string {
    if (shortUsr === null || shortUsr === undefined || shortUsr === '') {
      throw new EvalError(
        'UserMap.findAndRenderUser was called with an empty or null user!'
      );
    }
    const usr = this.findUser(shortUsr);
    if (!usr || !usr.name) {
      return '[' + shortUsr + ']';
    } else {
      return usr.name;
    }
  }
}
