/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ErrorType } from '../common/enums';
export class BannerMessage {
  status = 0;
  text = '';
  isActive = false;
  errorType: ErrorType | null = null;
  public hide() {
    this.isActive = false;
  }
}
