/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { Responsibility } from '../model/responsibility';
export const RESPONSIBILITIES: TerritoryResponsibility[] = [
    {
        id: 1,
        gridTerritoryDescription: 'Mannheim',
        responsibilityList: [
            new Responsibility({
                id: 1,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'S',
                isActive: true
            }),
            new Responsibility({
                id: 2,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'W',
                isActive: true
            }),
            new Responsibility({
                id: 3,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: true
            }),
            new Responsibility({
                id: 4,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'G',
                isActive: true
            }),
            new Responsibility({
                id: 13,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'Z',
                isActive: true
            })
        ]
    },
    {
        id: 2,
        gridTerritoryDescription: 'Offenbach',
        responsibilityList: [
            new Responsibility({
                id: 5,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'S',
                isActive: true
            }),
            new Responsibility({
                id: 6,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'W',
                isActive: true
            }),
            new Responsibility({
                id: 7,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: true
            }),
            new Responsibility({
                id: 8,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'G',
                isActive: true
            }),
            new Responsibility({
                id: 14,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'Z',
                isActive: true
            })
        ]
    },
    {
        id: 3,
        gridTerritoryDescription: 'Kassel',
        responsibilityList: [
            new Responsibility({
                id: 9,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'S',
                isActive: false
            }),
            new Responsibility({
                id: 10,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'W',
                isActive: false
            }),
            new Responsibility({
                id: 11,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: false
            }),
            new Responsibility({
                id: 12,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'G',
                isActive: false
            }),
            new Responsibility({
                id: 15,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'Z',
                isActive: true
            })
        ]
    }
];

export const RESPONSIBILITIES_SHIFT_CHANGE: TerritoryResponsibility[] = [
    {
        id: 1,
        gridTerritoryDescription: 'Mannheim',
        responsibilityList: [
            new Responsibility({
                id: 1,
                responsibleUser: 'max',
                newResponsibleUser: 'otto',
                branchName: 'S',
                isActive: true
            }),
            new Responsibility({
                id: 2,
                responsibleUser: 'max',
                newResponsibleUser: 'hugo',
                branchName: 'W',
                isActive: true
            }),
            new Responsibility({
                id: 3,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: true
            }),
            new Responsibility({
                id: 4,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'G',
                isActive: true
            })
        ]
    },
    {
        id: 2,
        gridTerritoryDescription: 'Offenbach',
        responsibilityList: [
            new Responsibility({
                id: 5,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'S',
                isActive: true
            }),
            new Responsibility({
                id: 6,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'W',
                isActive: true
            }),
            new Responsibility({
                id: 7,
                responsibleUser: 'admin',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: true
            }),
            new Responsibility({
                id: 8,
                responsibleUser: 'max',
                newResponsibleUser: '',
                branchName: 'G',
                isActive: true
            })
        ]
    },
    {
        id: 3,
        gridTerritoryDescription: 'Kassel',
        responsibilityList: [
            new Responsibility({
                id: 9,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'S',
                isActive: false
            }),
            new Responsibility({
                id: 10,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'W',
                isActive: false
            }),
            new Responsibility({
                id: 11,
                responsibleUser: 'User1',
                newResponsibleUser: '',
                branchName: 'F',
                isActive: false
            }),
            new Responsibility({
                id: 12,
                responsibleUser: 'max',
                newResponsibleUser: 'hugo',
                branchName: 'G',
                isActive: false
            }),
        ]
    }
];