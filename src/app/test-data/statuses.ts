/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Status } from '../model/status';

export const STATUSES: Status[] = [
    {
        id: 1,
        name: 'offen',
    },
    {
        id: 2,
        name: 'in Arbeit',
    },
    {
        id: 3,
        name: 'erledigt',
    },
    {
        id: 4,
        name: 'geschlossen',
    },
];
