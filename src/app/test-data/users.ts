/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { User } from '../model/user';

export const USERS: User[] = [
  {
    id: 1,
    username: 'max',
    name: 'Max Mustermann',
    specialUser: false,
    itemName: 'max',
  },
  {
    id: 2,
    username: 'admin',
    name: 'Administrator',
    specialUser: true,
    itemName: 'admin',
  },
  {
    id: 3,
    username: 'otto',
    name: 'Otto Normalverbraucher',
    specialUser: false,
    itemName: 'otto',
  },
];
