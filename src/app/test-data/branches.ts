/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Branch } from '../model/branch';

export const BRANCHES: Branch[] = [
    { 'id': 1, 'name': 'S', 'description': 'Strom' },
    { 'id': 2, 'name': 'G', 'description': 'Gas' },
    { 'id': 4, 'name': 'W', 'description': 'Wasser' },
    { 'id': 3, 'name': 'F', 'description': 'Fernwärme' },
    { 'id': 5, 'name': 'Z', 'description': 'ZSM' }
];
