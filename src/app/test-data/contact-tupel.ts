/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { ContactTupel } from 'app/model/contact-tupel';

export const CONTACTTUPEL: ContactTupel[] = [
    { 'contactName': 'user1', 'contactUuid': 'ec2b74b0-bb46-4b77-bee5-bd621f097bc8'}, 
    { 'contactName': 'TestUser1', 'contactUuid': 'ec2b74b0-bb46-4b77-bee5-bd621f097bc8'}, 
    { 'contactName': 'TestUser2', 'contactUuid': 'ec2b74b0-bb46-4b77-bee5-bd621f097bc8'}, 
    { 'contactName': 'user4', 'contactUuid': 'ec2b74b0-bb46-4b77-bee5-bd621f097bc8'}, 
    { 'contactName': 'user5', 'contactUuid': 'ec2b74b0-bb46-4b77-bee5-bd621f097bc8'}     
];
