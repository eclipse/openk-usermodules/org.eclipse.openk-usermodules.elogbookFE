/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OverviewComponent } from './pages/overview/overview.component';
import { ShiftChangeOverviewComponent } from './pages/shift-change-overview/shift-change-overview.component';
import { ReminderComponent } from './pages/reminder/reminder.component';
import { SearchComponent } from './pages/search/search.component';
import { LogoutPageComponent } from './pages/logout/logout.component';

const ROUTES: Routes = [
  {
    path: '',
    redirectTo: '/overview',
    pathMatch: 'full'
  },
  {
    path: 'overview',
    component: OverviewComponent
  },
  {
    path: 'shiftChangeOverview',
    component: ShiftChangeOverviewComponent
  },
  { 
    path: 'reminders',
    component: ReminderComponent
  },
  { 
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'logout',
    component: LogoutPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES, {useHash: true})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
