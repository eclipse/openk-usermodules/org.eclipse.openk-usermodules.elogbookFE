/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Globals } from 'app/common/globals';
import { Notification } from 'app/model/notification';
import { DUMMY_NOTIFICATION } from 'app/test-data/notifications';
import { of, throwError } from 'rxjs';
import { SessionContext } from '../../common/session-context';
import { MessageDefines, MessageService } from '../../services/message.service';
import { ReminderService } from '../../services/reminder.service';
import { ReminderCallerJobService } from './reminder-caller-job.service';

describe('ReminderCallerJobService', () => {
  let service: ReminderCallerJobService;
  let httpMock: HttpTestingController;
  let msgService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ReminderCallerJobService,
        SessionContext,
        MessageService,
        ReminderService,
      ],
    });

    service = TestBed.inject(ReminderCallerJobService);
    httpMock = TestBed.inject(HttpTestingController);
    msgService = TestBed.inject(MessageService);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
    msgService.loginLogoff$.complete();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should do job successfully', () => {
    const mockReminders = [
      { id: 1, name: 'reminder1' },
      { id: 2, name: 'reminder2' },
    ];

    spyOn(
      (service as any).reminderService,
      'getCurrentReminders'
    ).and.returnValue(of(mockReminders));
    spyOn((service as any)._sessionContext, 'setReminderAvailable');

    service.doJob();

    expect(
      (service as any).reminderService.getCurrentReminders
    ).toHaveBeenCalled();
    expect(
      (service as any)._sessionContext.setReminderAvailable
    ).toHaveBeenCalledWith(true);
  });

  it('should handle error when doing job', () => {
    spyOn(
      (service as any).reminderService,
      'getCurrentReminders'
    ).and.returnValue(throwError(() => 'error'));
    spyOn(console, 'log');

    service.doJob();

    expect(
      (service as any).reminderService.getCurrentReminders
    ).toHaveBeenCalled();
    expect(console.log).toHaveBeenCalledWith('error');
  });

  xit('should initialize on login', async done => {
    const spy = spyOn(service, 'init');

    service.onLoginLogoff();

    msgService.loginLogoff$.subscribe(() => {
      expect(spy).toHaveBeenCalled();
      done();
    });
    msgService.loginLogoff$.next(MessageDefines.MSG_LOG_IN_SUCCEEDED);
  });

  it('should not subscribe if REMINDER_JOB_POLLING_ON is false', () => {
    Globals.REMINDER_JOB_POLLING_ON = false;
    spyOn((service as any).msgService.loginLogoff$, 'subscribe');

    service.onLoginLogoff();

    expect(
      (service as any).msgService.loginLogoff$.subscribe
    ).not.toHaveBeenCalled();
  });

  it('should destroy subscription', () => {
    const spy1 = spyOn((service as any).endOfSubscription$, 'next');
    const spy2 = spyOn((service as any).endOfSubscription$, 'complete');

    service.destroy();

    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should set reminder available status to true when notifications are present', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];

    spyOn((service as any)._sessionContext, 'setReminderAvailable');

    service.setReminderAvailableStatus(mockNotifications);

    expect(
      (service as any)._sessionContext.setReminderAvailable
    ).toHaveBeenCalledWith(true);
  });

  it('should set reminder available status to false when notifications are not present', () => {
    spyOn((service as any)._sessionContext, 'setReminderAvailable');

    service.setReminderAvailableStatus([]);

    expect(
      (service as any)._sessionContext.setReminderAvailable
    ).toHaveBeenCalledWith(false);
  });

  xit('should destroy on logoff', async done => {
    const spy = spyOn(service, 'destroy');

    service.onLoginLogoff();

    msgService.loginLogoff$.subscribe(() => {
      expect(spy).toHaveBeenCalled();
      done();
    });
    msgService.loginLogoff$.next(MessageDefines.MSG_LOG_OFF);
  });
});
