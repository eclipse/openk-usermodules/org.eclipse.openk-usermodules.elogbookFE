/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Globals } from 'app/common/globals';
import { NotificationFileModel } from 'app/model/file-model';
import { of, throwError } from 'rxjs';
import { SessionContext } from '../../common/session-context';
import { MessageDefines, MessageService } from '../../services/message.service';
import { ImportFileCallerService } from './import-file-caller.service';
import { ImportService } from '../import.service';

describe('ImportFileCallerService', () => {
  let service: ImportFileCallerService;
  let httpMock: HttpTestingController;
  let msgService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ImportFileCallerService,
        SessionContext,
        MessageService,
        ImportService,
      ],
    });

    service = TestBed.inject(ImportFileCallerService);
    httpMock = TestBed.inject(HttpTestingController);
    msgService = TestBed.inject(MessageService);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
    msgService.loginLogoff$.complete();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  xit('should initialize on login', async done => {
    const spy = spyOn(service, 'init');

    service.onLoginLogoff();

    msgService.loginLogoff$.subscribe(() => {
      expect(spy).toHaveBeenCalled();
      msgService.loginLogoff$.complete();
      done();
    });
    msgService.loginLogoff$.next(MessageDefines.MSG_LOG_IN_SUCCEEDED);
  });

  it('should not subscribe if IMPORT_JOB_POLLING_ON is false', () => {
    Globals.IMPORT_JOB_POLLING_ON = false;
    spyOn((service as any).msgService.loginLogoff$, 'subscribe');

    service.onLoginLogoff();

    expect(
      (service as any).msgService.loginLogoff$.subscribe
    ).not.toHaveBeenCalled();
  });

  it('should destroy subscription', () => {
    const spy1 = spyOn((service as any).endOfSubscription$, 'next');
    const spy2 = spyOn((service as any).endOfSubscription$, 'complete');

    service.destroy();

    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
  });

  it('should set reminder available status to true when notifications are present', () => {
    const mockNotifications: NotificationFileModel[] = [
      new NotificationFileModel(),
    ];

    spyOn((service as any)._sessionContext, 'setImportFileAvailable');

    service.setImportFileAvailableStatus(mockNotifications);

    expect(
      (service as any)._sessionContext.setImportFileAvailable
    ).toHaveBeenCalledWith(true);
  });

  it('should set reminder available status to false when notifications are not present', () => {
    spyOn((service as any)._sessionContext, 'setImportFileAvailable');

    service.setImportFileAvailableStatus([]);

    expect(
      (service as any)._sessionContext.setImportFileAvailable
    ).toHaveBeenCalledWith(false);
  });

  xit('should destroy on logoff', async done => {
    const spy = spyOn(service, 'destroy');

    service.onLoginLogoff();

    msgService.loginLogoff$.subscribe(() => {
      expect(spy).toHaveBeenCalled();
      msgService.loginLogoff$.complete();
      done();
    });
    msgService.loginLogoff$.next(MessageDefines.MSG_LOG_OFF);
  });

  it('should do job successfully', () => {
    const mockFiles = [
      { id: 1, name: 'file1' },
      { id: 2, name: 'file2' },
    ];

    spyOn((service as any).importFileService, 'getImportFiles').and.returnValue(
      of(mockFiles)
    );
    spyOn((service as any).importFileService, 'importFile').and.returnValue(
      of(mockFiles)
    );
    spyOn(service, 'setImportFileAvailableStatus');
    spyOn(console, 'log');

    service.doJob();

    expect(
      (service as any).importFileService.getImportFiles
    ).toHaveBeenCalled();
    expect((service as any).importFileService.importFile).toHaveBeenCalled();
    expect(service.setImportFileAvailableStatus).toHaveBeenCalledWith(
      mockFiles
    );
    expect(console.log).not.toHaveBeenCalled();
  });

  it('should handle error when doing job', () => {
    spyOn((service as any).importFileService, 'getImportFiles').and.returnValue(
      throwError('error')
    );
    spyOn((service as any).importFileService, 'importFile').and.returnValue(
      throwError('error')
    );
    spyOn(service, 'setImportFileAvailableStatus');
    spyOn(console, 'log');

    service.doJob();

    expect(
      (service as any).importFileService.getImportFiles
    ).toHaveBeenCalled();
    expect((service as any).importFileService.importFile).toHaveBeenCalled();
    expect(service.setImportFileAvailableStatus).not.toHaveBeenCalled();
    expect(console.log).toHaveBeenCalledWith('error');
  });
});
