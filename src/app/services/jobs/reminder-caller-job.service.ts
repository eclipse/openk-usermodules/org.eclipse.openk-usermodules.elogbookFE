/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Injectable } from '@angular/core';
import {
  Observable,
  Subject,
  catchError,
  of,
  take,
  takeUntil,
  timer,
} from 'rxjs';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { FilterMatrix } from '../../model/controller-model/filter-matrix';
import { Notification } from '../../model/notification';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { MessageDefines, MessageService } from '../../services/message.service';
import { ReminderService } from '../../services/reminder.service';

@Injectable()
export class ReminderCallerJobService {
  private timer: Observable<number> = of(0);
  private endOfSubscription$: Subject<void> = new Subject();
  currentTime$: string | null = null;
  reminderSearchFilter: ReminderSearchFilter = new ReminderSearchFilter();

  constructor(
    protected _sessionContext: SessionContext,
    protected reminderService: ReminderService,
    protected msgService: MessageService
  ) {}

  onLoginLogoff() {
    if (Globals.REMINDER_JOB_POLLING_ON) {
      this.msgService.loginLogoff$.subscribe((msg: string) => {
        if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
          this.init();
        }
        if (msg === MessageDefines.MSG_LOG_OFF) {
          this.destroy();
        }
      });
    }
  }

  init() {
    this.reminderSearchFilter = new ReminderSearchFilter();
    this.timer = timer(
      Globals.REMINDER_JOB_POLLING_START_DELAY,
      Globals.REMINDER_JOB_POLLING_INTERVALL
    );
    this.timer
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.doJob());
  }

  destroy() {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  doJob() {
    this.currentTime$ = new Date().toISOString();
    this.reminderSearchFilter.reminderDate = this.currentTime$;
    const filterMatrixSession = this._sessionContext.getfilterMatrix();
    if (filterMatrixSession) {
      const filterMatrix: FilterMatrix = new FilterMatrix(
        filterMatrixSession.responsibilityContainerMatrix
      );
      this.reminderSearchFilter.responsibilityFilterList =
        filterMatrix.getNumFilterList();
    }

    this.reminderService
      .getCurrentReminders(this.reminderSearchFilter)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(err => {
          console.log(err);
          return of([]);
        })
      )
      .subscribe(nots => this.setReminderAvailableStatus(nots));
  }

  setReminderAvailableStatus(nots: Notification[]) {
    if (nots && nots.length) {
      // not empty; reminders available
      this._sessionContext.setReminderAvailable(true);
    } else {
      // empty; no reminders available
      this._sessionContext.setReminderAvailable(false);
    }
  }
}
