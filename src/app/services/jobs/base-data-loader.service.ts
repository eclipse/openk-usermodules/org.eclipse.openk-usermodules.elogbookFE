/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Injectable } from '@angular/core';
import { SessionContext } from '../../common/session-context';
import { BaseDataService } from '../base-data.service';
import { MessageDefines, MessageService } from '../message.service';
import { UserService } from '../user.service';
import { catchError, forkJoin, take, tap } from 'rxjs';

@Injectable()
export class BaseDataLoaderService {
  constructor(
    private baseDataService: BaseDataService,
    private userService: UserService,
    private msgService: MessageService,
    private sessionContext: SessionContext
  ) {}

  onLoginLogoff(): void {
    this.msgService.loginLogoff$.subscribe((msg: string) => {
      this.sessionContext.initBannerMessage();
      if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
        this.loadBaseData();
      }
    });
  }

  loadBaseData() {
    forkJoin({
      branches: this.baseDataService.getBranches(),
      statuses: this.baseDataService.getStatuses(),
      priorities: this.baseDataService.getPriorities(),
      gridTerritories: this.baseDataService.getGridTerritories(),
      users: this.userService.getUsers(),
      userSettings: this.userService.getUserSettings(),
    })
      .pipe(
        take(1),
        tap(
          ({
            branches,
            statuses,
            priorities,
            gridTerritories,
            users,
            userSettings,
          }) => {
            this.sessionContext.setBranches(branches);
            this.sessionContext.setStatuses(statuses);
            this.sessionContext.setPriorities(priorities);
            this.sessionContext.setGridTerritories(gridTerritories);
            this.sessionContext.setAllUsers(users);
            this.sessionContext.setUsersSettings(userSettings);
          }
        ),
        catchError(error => {
          console.error(error);
          return [];
        })
      )
      .subscribe();
  }
}
