/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Injectable } from '@angular/core';
import {
  Observable,
  Subject,
  catchError,
  of,
  take,
  takeUntil,
  timer,
} from 'rxjs';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { NotificationFileModel } from '../../model/file-model';
import { ImportService } from '../../services/import.service';
import { MessageDefines, MessageService } from '../../services/message.service';

@Injectable()
export class ImportFileCallerService {
  private timer: Observable<number> = of(0);
  private endOfSubscription$: Subject<void> = new Subject();
  currentTime$: string | null = null;

  constructor(
    protected _sessionContext: SessionContext,
    protected importFileService: ImportService,
    protected msgService: MessageService
  ) {}

  onLoginLogoff() {
    if (Globals.IMPORT_JOB_POLLING_ON) {
      this.msgService.loginLogoff$.subscribe((msg: string) => {
        if (msg === MessageDefines.MSG_LOG_IN_SUCCEEDED) {
          this.init();
        }
        if (msg === MessageDefines.MSG_LOG_OFF) {
          this.destroy();
        }
      });
    }
  }

  init() {
    this.timer = timer(
      Globals.IMPORT_JOB_POLLING_START_DELAY,
      Globals.IMPORT_JOB_POLLING_INTERVALL
    );
    this.timer
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.doJob());
  }

  destroy() {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  doJob() {
    this.currentTime$ = new Date().toISOString();

    this.importFileService
      .getImportFiles()
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError((error: Error) => {
          console.log(error);
          return [];
        })
      )
      .subscribe(files => this.setImportFileAvailableStatus(files));

    this.importFileService
      .importFile()
      .pipe(
        take(1),
        catchError((error: Error) => {
          console.log(error);
          return [];
        })
      )
      .subscribe(files => this.setImportFileAvailableStatus(files));
  }

  setImportFileAvailableStatus(files: NotificationFileModel[]) {
    if (files && files.length) {
      // not empty; file available
      this._sessionContext.setImportFileAvailable(true);
    } else {
      // empty; no file available
      this._sessionContext.setImportFileAvailable(false);
    }
  }
}
