/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { SessionContext } from '../../common/session-context';
import { BaseDataService } from '../base-data.service';
import { MessageService } from '../message.service';
import { UserService } from '../user.service';
import { BaseDataLoaderService } from './base-data-loader.service';

describe('BaseDataLoaderService', () => {
  let service: BaseDataLoaderService;
  let baseDataService: any;
  let userService: any;
  let messageService: MessageService;
  let sessionContext: SessionContext;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        BaseDataLoaderService,
        BaseDataService,
        UserService,
        MessageService,
        SessionContext,
      ],
    });

    service = TestBed.inject(BaseDataLoaderService);
    messageService = TestBed.inject(MessageService);
    sessionContext = TestBed.inject(SessionContext);
    baseDataService = TestBed.inject(BaseDataService);
    userService = TestBed.inject(UserService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should load base data', () => {
    const spy = spyOn(baseDataService, 'getBranches').and.returnValue(of([]));
    const spy1 = spyOn(baseDataService, 'getStatuses').and.returnValue(of([]));
    const spy2 = spyOn(baseDataService, 'getPriorities').and.returnValue(
      of([])
    );
    const spy3 = spyOn(baseDataService, 'getGridTerritories').and.returnValue(
      of([])
    );
    const spy4 = spyOn(userService, 'getUsers').and.returnValue(of([]));
    const spy5 = spyOn(userService, 'getUserSettings').and.returnValue(of([]));

    service.loadBaseData();

    expect(spy).toHaveBeenCalled();
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
    expect(spy4).toHaveBeenCalled();
    expect(spy5).toHaveBeenCalled();
  });
});
