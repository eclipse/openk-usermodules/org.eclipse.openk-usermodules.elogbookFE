/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { VersionInfo } from '../model/version-info';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class VersionInfoService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    public messageService: MessageService
  ) {
    super(messageService);
  }

  public loadBackendServerInfo(): Observable<VersionInfo> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<VersionInfo>(super.getBaseUrl() + '/versionInfo', {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }
}
