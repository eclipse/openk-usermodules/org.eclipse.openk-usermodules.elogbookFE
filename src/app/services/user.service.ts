/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserSettings } from 'app/model/user-settings';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { User } from '../model/user';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class UserService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    public messageService: MessageService,
    private _sessionContext: SessionContext
  ) {
    super(messageService);
  }

  public getUsers(): Observable<User[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<User[]>(super.getBaseUrl() + '/users', { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getUserSettings(): Observable<UserSettings> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<UserSettings>(super.getBaseUrl() + '/user-settings', {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public postUserSettings(userSettings: UserSettings) {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/user-settings/';

    return this._http
      .post<UserSettings>(url, JSON.stringify(userSettings, this.replacer), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  replacer(key: string, value: any) {
    const originalObject = this[key as keyof this];
    if (originalObject instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(originalObject.entries()), // or with spread: value: [...originalObject]
      };
    } else {
      return value;
    }
  }

  reviver(key: any, value: any) {
    if (typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map') {
        return new Map(value.value);
      }
    }
    return value;
  }
}
