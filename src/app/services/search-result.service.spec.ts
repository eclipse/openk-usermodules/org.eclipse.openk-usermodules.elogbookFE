/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { TestBed, inject } from '@angular/core/testing';

import { SearchResultService } from './search-result.service';
/*
describe('SearchResultService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchResultService]
    });
  });

  it('should be created', inject([SearchResultService], (service: SearchResultService) => {
    expect(service).toBeTruthy();
  }));
}); */
