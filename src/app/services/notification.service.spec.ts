/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */

import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Globals } from 'app/common/globals';
import { DUMMY_NOTIFICATION } from 'app/test-data/notifications';
import { SessionContext } from '../common/session-context';
import { Notification } from '../model/notification';
import { MessageService } from './message.service';
import { NotificationService } from './notification.service';
import { ContactTupel } from 'app/model/contact-tupel';
import { NotificationSearchFilter } from 'app/model/notification-search-filter';

describe('NotificationService', () => {
  let service: NotificationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [NotificationService, SessionContext, MessageService],
    });

    service = TestBed.inject(NotificationService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];

    service
      .getNotifications(NotificationService.ALL)
      .subscribe(notifications => {
        expect(notifications).toEqual(mockNotifications);
      });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.ALL}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting notifications', () => {
    service.getNotifications(NotificationService.ALL).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETNOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.ALL}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETNOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get a notification successfully', () => {
    const mockNotification: Notification = DUMMY_NOTIFICATION;

    service.getNotification(1).subscribe(notification => {
      expect(notification).toEqual(mockNotification);
    });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notification/1`);
    expect(req.request.method).toBe('GET');
    req.flush(mockNotification);
  });

  it('should handle error when getting a notification', () => {
    service.getNotification(1).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETNOTIFICATION_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notification/1`);
    expect(req.request.method).toBe('GET');
    req.flush('GETNOTIFICATION_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get assigned user suggestions successfully', () => {
    const mockUserSuggestions: ContactTupel[] = [
      { contactName: 'name', contactUuid: 'uuid1' },
      { contactName: 'name2', contactUuid: 'uuid2' },
    ];

    service.getAssignedUserSuggestions().subscribe(userSuggestions => {
      expect(userSuggestions).toEqual(mockUserSuggestions);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/assignedUserSuggestions/`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockUserSuggestions);
  });

  it('should handle error when getting assigned user suggestions', () => {
    service.getAssignedUserSuggestions().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETASSIGNEDUSERSUGGESTIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/assignedUserSuggestions/`
    );
    expect(req.request.method).toBe('GET');
    req.flush('GETASSIGNEDUSERSUGGESTIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get notification versions successfully', () => {
    const mockNotificationVersions: Notification[] = [DUMMY_NOTIFICATION];

    service.getNotificationVersions(1).subscribe(notificationVersions => {
      expect(notificationVersions).toEqual(mockNotificationVersions);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notificationsByIncident/1`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockNotificationVersions);
  });

  it('should handle error when getting notification versions', () => {
    service.getNotificationVersions(1).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETNOTIFICATIONVERSIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notificationsByIncident/1`
    );
    expect(req.request.method).toBe('GET');
    req.flush('GETNOTIFICATIONVERSIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should create a notification successfully', () => {
    const mockNotification: Notification = DUMMY_NOTIFICATION;

    service.createNotification(mockNotification).subscribe(notification => {
      expect(notification).toEqual(mockNotification);
    });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notifications/create/`);
    expect(req.request.method).toBe('PUT');
    req.flush(mockNotification);
  });

  it('should handle error when creating a notification', () => {
    const mockNotification: Notification = DUMMY_NOTIFICATION;

    service.createNotification(mockNotification).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('CREATENOTIFICATION_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notifications/create/`);
    expect(req.request.method).toBe('PUT');
    req.flush('CREATENOTIFICATION_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should update a notification successfully', () => {
    const mockNotification: Notification = DUMMY_NOTIFICATION;

    service.updateNotification(mockNotification).subscribe(notification => {
      expect(notification).toEqual(mockNotification);
    });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notifications/update/`);
    expect(req.request.method).toBe('POST');
    req.flush(mockNotification);
  });

  it('should handle error when updating a notification', () => {
    const mockNotification: Notification = DUMMY_NOTIFICATION;

    service.updateNotification(mockNotification).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('UPDATENOTIFICATION_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/notifications/update/`);
    expect(req.request.method).toBe('POST');
    req.flush('UPDATENOTIFICATION_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get all notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];

    service.getAllNotifications().subscribe(notifications => {
      expect(notifications).toEqual(mockNotifications);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.ALL}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting all notifications', () => {
    service.getAllNotifications().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETALLNOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.ALL}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETALLNOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get current notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];

    service.getCurrentNotifications().subscribe(notifications => {
      expect(notifications).toEqual(mockNotifications);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.CURRENT}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting current notifications', () => {
    service.getCurrentNotifications().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETCURRENTNOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.CURRENT}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETCURRENTNOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get future notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getFutureNotifications(mockFilter).subscribe(notifications => {
      expect(notifications).toEqual(mockNotifications);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.FUTURE}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting future notifications', () => {
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getFutureNotifications(mockFilter).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETFUTURENOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.FUTURE}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETFUTURENOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get open notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getOpenNotifications(mockFilter).subscribe(notifications => {
      expect(notifications).toEqual(mockNotifications);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.OPEN}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting open notifications', () => {
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getOpenNotifications(mockFilter).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETOPENNOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.OPEN}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETOPENNOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get finished notifications successfully', () => {
    const mockNotifications: Notification[] = [DUMMY_NOTIFICATION];
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getFinishedNotifications(mockFilter).subscribe(notifications => {
      expect(notifications).toEqual(mockNotifications);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.FINISHED}`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockNotifications);
  });

  it('should handle error when getting finished notifications', () => {
    const mockFilter: NotificationSearchFilter = new NotificationSearchFilter();

    service.getFinishedNotifications(mockFilter).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETFINISHEDNOTIFICATIONS_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/notifications/${NotificationService.FINISHED}`
    );
    expect(req.request.method).toBe('POST');
    req.flush('GETFINISHEDNOTIFICATIONS_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });
});
