/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */

import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { ImportService } from './import.service';
import { SessionContext } from '../common/session-context';
import { MessageService } from './message.service';
import { NotificationFileModel } from '../model/file-model';

describe('ImportService', () => {
  let service: ImportService;
  let httpMock: HttpTestingController;
  let sessionContext: SessionContext;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ImportService, SessionContext, MessageService],
    });

    service = TestBed.inject(ImportService);
    httpMock = TestBed.inject(HttpTestingController);
    sessionContext = TestBed.inject(SessionContext);
    messageService = TestBed.inject(MessageService);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get import files', () => {
    const mockFiles: NotificationFileModel[] = [
      { fileName: 'file1', creationDate: '2020-01-01T00:00:00' },
      { fileName: 'file2', creationDate: '2020-01-01T00:00:00' },
    ];

    service.getImportFiles().subscribe(files => {
      expect(files.length).toBe(2);
      expect(files).toEqual(mockFiles);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/getImportFiles/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockFiles);
  });

  it('should delete import file', () => {
    const fileName = 'file1';

    service.deleteImportFile(fileName).subscribe(res => {
      expect(res).toBeTruthy();
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/deleteImportedFiles/' + fileName
    );
    expect(req.request.method).toBe('DELETE');
    req.flush(true);
  });

  it('should import file', () => {
    const mockFiles: NotificationFileModel[] = [
      { fileName: 'file1', creationDate: '2020-01-01T00:00:00' },
      { fileName: 'file2', creationDate: '2020-01-01T00:00:00' },
    ];

    service.importFile().subscribe(files => {
      expect(files.length).toBe(2);
      expect(files).toEqual(mockFiles);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/importFile/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockFiles);
  });
});
