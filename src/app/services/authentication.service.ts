/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class AuthenticationService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    public messageService: MessageService,
    private _sessionContext: SessionContext
  ) {
    super(messageService);
  }

  public logout(): Observable<any> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<any>(super.getBaseUrl() + '/logout', { headers: headers })
      .pipe(
        map(res => {
          super.extractSessionId(res.headers, this._sessionContext);
        }),
        catchError(super.handleErrorPromise)
      );
  }
}
