/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { Notification } from '../model/notification';
import { ReminderSearchFilter } from '../model/reminder-search-filter';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class ReminderService extends BaseHttpService {
  public itemAdded$: EventEmitter<Notification>;
  public itemChanged$: EventEmitter<Notification>;

  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    public messageService: MessageService
  ) {
    super(messageService);
    this.itemAdded$ = new EventEmitter();
    this.itemChanged$ = new EventEmitter();
  }

  public getCurrentReminders(
    reminderSearchFilter: ReminderSearchFilter
  ): Observable<Notification[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/currentReminders/';
    const filter = reminderSearchFilter || {};

    return this._http
      .post<Notification[]>(url, JSON.stringify(filter), { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }
}
