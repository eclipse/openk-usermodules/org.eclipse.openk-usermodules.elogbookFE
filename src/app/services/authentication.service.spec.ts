/* eslint-disable @typescript-eslint/no-unused-vars */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */

import { TestBed, inject } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { SessionContext } from '../common/session-context';
import { MessageService } from '../services/message.service';
import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpMock: HttpTestingController;
  let sessionContext: SessionContext;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthenticationService, SessionContext, MessageService],
    });

    service = TestBed.inject(AuthenticationService);
    httpMock = TestBed.inject(HttpTestingController);
    sessionContext = TestBed.inject(SessionContext);
    messageService = TestBed.inject(MessageService);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('can instantiate service when inject service', inject(
    [AuthenticationService],
    (service: AuthenticationService) => {
      expect(service instanceof AuthenticationService).toBe(true);
    }
  ));

  it('can instantiate service with "new"', inject(
    [HttpClient],
    (http: HttpClient) => {
      expect(http).not.toBeNull('http should be provided');
      const service = new AuthenticationService(
        http,
        messageService,
        sessionContext
      );
      expect(service instanceof AuthenticationService).toBe(
        true,
        'new service should be ok'
      );
    }
  ));

  it('can provide the HttpTestingController as HttpClient', inject(
    [HttpTestingController],
    (backend: HttpTestingController) => {
      expect(backend).not.toBeNull('backend should be provided');
    }
  ));
});
