/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
/* tslint:disable:no-unused-variable */

import { TestBed } from '@angular/core/testing';

import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { SessionContext } from '../common/session-context';
import { MessageService } from '../services/message.service';
import { ReminderService } from './reminder.service';

describe('Http-ReminderService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  let service: ReminderService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ReminderService, SessionContext, MessageService],
    }).compileComponents();
    messageService = new MessageService();
    sessionContext = new SessionContext();

    service = TestBed.inject(ReminderService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('can instantiate service when inject service', () => {
    expect(service).toBeTruthy();
  });

  it('can instantiate service with "new"', () => {
    const http = TestBed.inject(HttpClient);
    expect(http).not.toBeNull('http should be provided');
    const service = new ReminderService(http, sessionContext, messageService);
    expect(service instanceof ReminderService).toBe(
      true,
      'new service should be ok'
    );
  });

  it('can provide the HttpTestingController as HttpClient', () => {
    const backend = TestBed.inject(HttpTestingController);
    expect(backend).not.toBeNull('backend should be provided');
  });
});
