/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Priority } from 'app/model/priority';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { Branch } from '../model/branch';
import { GridTerritory } from '../model/gridterritory';
import { Status } from '../model/status';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class BaseDataService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    public messageService: MessageService,
    private _sessionContext: SessionContext
  ) {
    super(messageService);
  }

  public getStatuses(): Observable<Status[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notificationStatuses/';

    return this._http
      .get<Status[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getPriorities(): Observable<Priority[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notificationPriorities/';

    return this._http
      .get<Priority[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getBranches(): Observable<Branch[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/branches/';

    return this._http
      .get<Branch[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getGridTerritories(): Observable<GridTerritory[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/gridTerritories/';

    return this._http
      .get<GridTerritory[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }
}
