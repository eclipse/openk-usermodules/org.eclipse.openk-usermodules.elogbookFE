/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { GlobalSearchFilter } from '../model/global-search-filter';
import { Notification } from '../model/notification';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class SearchResultService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    public messageService: MessageService
  ) {
    super(messageService);
  }

  public getSearchResults(
    searchResultFilter?: GlobalSearchFilter
  ): Observable<Notification[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/searchResults/';
    const globalSearchFilter = searchResultFilter || {};

    return this._http
      .post<Notification[]>(url, JSON.stringify(globalSearchFilter), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }
}
