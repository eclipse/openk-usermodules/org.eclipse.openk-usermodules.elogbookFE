/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { ContactTupel } from 'app/model/contact-tupel';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { Notification } from '../model/notification';
import { NotificationSearchFilter } from '../model/notification-search-filter';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class NotificationService extends BaseHttpService {
  public static ALL = 'active';
  public static CURRENT = 'current';
  public static FUTURE = 'future';
  public static OPEN = 'open';
  public static FINISHED = 'past';

  public itemAdded$: EventEmitter<Notification>;
  public itemChanged$: EventEmitter<Notification>;

  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    public messageService: MessageService
  ) {
    super(messageService);
    this.itemAdded$ = new EventEmitter();
    this.itemChanged$ = new EventEmitter();
  }

  public getNotifications(
    notificationType: string,
    notificationSearchFilter:
      | NotificationSearchFilter
      | null
      | undefined = undefined
  ): Observable<Notification[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notifications/' + notificationType;
    const filter = notificationSearchFilter || {};

    return this._http
      .post<Notification[]>(url, JSON.stringify(filter), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getNotification(notificationId: number): Observable<Notification> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notification/' + notificationId;

    return this._http
      .get<Notification>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getAssignedUserSuggestions(): Observable<ContactTupel[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    // return of(
    //   Array.from({ length: 5 }, (_, i) => ({
    //     contactName: `Contact${i}`,
    //     contactUuid: `uuid-${i}`,
    //   })) as ContactTupel[]
    // );

    return this._http
      .get<ContactTupel[]>(super.getBaseUrl() + '/assignedUserSuggestions/', {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getNotificationVersions(
    incidentId: number
  ): Observable<Notification[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<Notification[]>(
        super.getBaseUrl() + '/notificationsByIncident/' + incidentId,
        { headers: headers }
      )
      .pipe(catchError(super.handleErrorPromise));
  }

  public createNotification(
    notification: Notification
  ): Observable<Notification> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notifications/create/';

    return this._http
      .put<Notification>(url, JSON.stringify(notification), {
        headers: headers,
      })
      .pipe(
        map(res => {
          this.itemAdded$.emit(res);
          return res;
        }),
        catchError(super.handleErrorPromise)
      );
  }

  public updateNotification(
    notification: Notification
  ): Observable<Notification> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/notifications/update/';

    return this._http
      .post<Notification>(url, JSON.stringify(notification), {
        headers: headers,
      })
      .pipe(
        map(res => {
          this.itemChanged$.emit(res);
          return res;
        }),
        catchError(super.handleErrorPromise)
      );
  }

  public getAllNotifications(): Observable<Notification[]> {
    return this.getNotifications(NotificationService.ALL);
  }
  public getCurrentNotifications(): Observable<Notification[]> {
    return this.getNotifications(NotificationService.CURRENT);
  }
  public getFutureNotifications(
    notificationSearchFilter: NotificationSearchFilter | null | undefined
  ): Observable<Notification[]> {
    return this.getNotifications(
      NotificationService.FUTURE,
      notificationSearchFilter
    );
  }
  public getOpenNotifications(
    notificationSearchFilter: NotificationSearchFilter | null | undefined
  ): Observable<Notification[]> {
    return this.getNotifications(
      NotificationService.OPEN,
      notificationSearchFilter
    );
  }
  public getFinishedNotifications(
    notificationSearchFilter: NotificationSearchFilter | null | undefined
  ): Observable<Notification[]> {
    return this.getNotifications(
      NotificationService.FINISHED,
      notificationSearchFilter
    );
  }
}
