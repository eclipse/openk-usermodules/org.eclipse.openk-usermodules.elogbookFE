/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable, EventEmitter } from '@angular/core';
import { StatusEn, BannerMessageStatusEn, ErrorType } from '../common/enums';

import { BannerMessage } from '../common/banner-message';

export class MessageDefines {
  static MSG_LOG_IN_SUCCEEDED = 'LOG_IN_SUCCEEDED';
  static MSG_LOG_OFF = 'LOG_OFF';
}

@Injectable()
export class MessageService {

  public loginLogoff$: EventEmitter<string> = new EventEmitter<string>();

  public matrixFilterChanged$: EventEmitter<number[]> = new EventEmitter<number[]>();
  public errorOccured$: EventEmitter<BannerMessage> = new EventEmitter<BannerMessage>();
  public respChangedForCurrentUser$: EventEmitter<boolean> = new EventEmitter<boolean>();
  public respConfirmed$: EventEmitter<boolean> = new EventEmitter<boolean>();

  public emitError(location: string, errorType?: ErrorType) {
    let message = '';
    switch (errorType) {
      case ErrorType.create:
        message = 'Fehler beim Erstellen des Objekts ' + location + '. Bitte kontaktieren Sie den Administrator';
        break;

      case ErrorType.update:
        message = 'Fehler beim Aktualiseren des Objekts ' + location + '. Bitte kontaktieren Sie den Administrator';
        break;

      case ErrorType.delete:
        message = 'Fehler beim Löschen des Objekts ' + location + '. Bitte kontaktieren Sie den Administrator';
        break;
      case ErrorType.retrieve:
        message = 'Fehler beim Zugriff auf  ' + location + '. Bitte kontaktieren Sie den Administrator';

        break;
      default:
        message = 'Es ist ein unbekannter Fehler aufgetreten. Bitte kontaktieren Sie den Administrator';
        break;
    }
    this.emitMessage(message, BannerMessageStatusEn.error);
  }

  public emitInfo(errorMessage: string) {
    this.emitMessage(errorMessage, BannerMessageStatusEn.info);
  }

  public emitWarning(errorMessage: string) {
    this.emitMessage(errorMessage, BannerMessageStatusEn.warning);
  }
  public deactivateMessage() {
    const bannerMessage: BannerMessage = new BannerMessage();
    bannerMessage.isActive = false;
    this.errorOccured$.emit(bannerMessage);
  }
  private emitMessage(message: string, status: BannerMessageStatusEn) {
    const bannerMessage: BannerMessage = new BannerMessage();
    bannerMessage.isActive = true;
    bannerMessage.status = status;
    bannerMessage.text = message;
    this.errorOccured$.emit(bannerMessage);
    console.log(message);
  }
  constructor() { }

}
