/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { TestBed } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { Globals } from 'app/common/globals';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { MessageService } from '../services/message.service';
import { USERS } from '../test-data/users';
import { UserService } from './user.service';

describe('Http-UserService (mockBackend)', () => {
  let sessionContext: SessionContext;
  let messageService: MessageService;
  let service: UserService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService, SessionContext, MessageService],
    }).compileComponents();
    messageService = new MessageService();
    sessionContext = new SessionContext();

    service = TestBed.inject(UserService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('can instantiate service when inject service', () => {
    expect(service instanceof UserService).toBe(true);
  });

  it('can instantiate service with "new"', () => {
    const httpClient = TestBed.inject(HttpClient);
    expect(httpClient).not.toBeNull('httpClient should be provided');
    const newService = new UserService(
      httpClient,
      messageService,
      sessionContext
    );
    expect(newService instanceof UserService).toBe(
      true,
      'new service should be ok'
    );
  });

  it('can provide the HttpTestingController as HttpClient', () => {
    expect(httpMock).not.toBeNull('backend should be provided');
  });

  describe('when getUsers()', () => {
    it('should have expected fake users', async () => {
      service.getUsers().subscribe(users => {
        expect(users.length).toBe(3);
        expect(users[0]).not.toBeNull();
        expect(users[1]).not.toBeNull();
        expect(users[2]).not.toBeNull();
        expect(users[0].id).toBe(1);
        expect(users[1].id).toBe(2);
        expect(users[2].id).toBe(3);
      });

      const req = httpMock.expectOne(Globals.BASE_URL + '/users');
      req.flush(USERS);
    });

    it('should treat 404 as an Observable error', async () => {
      const spy = spyOn(service, 'getUsers').and.returnValue(
        throwError(() => {})
      );
      service
        .getUsers()
        .pipe(
          catchError(err => {
            expect(spy).toHaveBeenCalled();
            return of(err);
          })
        )
        .subscribe();
    });
  });
});
