/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Globals } from 'app/common/globals';
import { ResponsibilitySearchFilter } from 'app/model/responsibility-search-filter';
import { SessionContext } from '../common/session-context';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { MessageService } from './message.service';
import { ResponsibilityService } from './responsibility.service';

describe('ResponsibilityService', () => {
  let service: ResponsibilityService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ResponsibilityService, SessionContext, MessageService],
    });

    service = TestBed.inject(ResponsibilityService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve all responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service.getAllResponsibilities().subscribe(responsibilities => {
      expect(responsibilities).toEqual(mockResponsibilities);
    });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/allResponsibilities`);
    expect(req.request.method).toBe('GET');
    req.flush(mockResponsibilities);
  });

  it('should handle error when retrieving all responsibilities', () => {
    service.getAllResponsibilities().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETALLRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/allResponsibilities`);
    expect(req.request.method).toBe('GET');
    req.flush('GETALLRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should retrieve responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service.getResponsibilities().subscribe(responsibilities => {
      expect(responsibilities).toEqual(mockResponsibilities);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/currentResponsibilities`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockResponsibilities);
  });

  it('should handle error when retrieving responsibilities', () => {
    service.getResponsibilities().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/currentResponsibilities`
    );
    expect(req.request.method).toBe('GET');
    req.flush('GETRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should retrieve planned responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service.getPlannedResponsibilities().subscribe(responsibilities => {
      expect(responsibilities).toEqual(mockResponsibilities);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/plannedResponsibilities`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockResponsibilities);
  });

  it('should handle error when retrieving planned responsibilities', () => {
    service.getPlannedResponsibilities().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/plannedResponsibilities`
    );
    expect(req.request.method).toBe('GET');
    req.flush('GETRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should post responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service
      .planResponsibilities(mockResponsibilities)
      .subscribe(responsibilities => {
        expect(responsibilities).toEqual(mockResponsibilities);
      });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/postResponsibilities/`);
    expect(req.request.method).toBe('POST');
    req.flush(mockResponsibilities);
  });

  it('should handle error when posting responsibilities', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];
    service.planResponsibilities(mockResponsibilities).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('POSTRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/postResponsibilities/`);
    expect(req.request.method).toBe('POST');
    req.flush('POSTRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should fetch responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service
      .fetchResponsibilities(mockResponsibilities)
      .subscribe(responsibilities => {
        expect(responsibilities).toEqual(mockResponsibilities);
      });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/fetchResponsibilities/`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockResponsibilities);
  });

  it('should handle error when fetching responsibilities', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service.fetchResponsibilities(mockResponsibilities).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('FETCHRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/fetchResponsibilities/`
    );
    expect(req.request.method).toBe('POST');
    req.flush('FETCHRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should confirm responsibilities successfully', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service
      .confirmResponsibilities(mockResponsibilities)
      .subscribe(responsibilities => {
        expect(responsibilities).toEqual(mockResponsibilities);
      });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/confirmResponsibilities/`
    );
    expect(req.request.method).toBe('POST');
    req.flush(mockResponsibilities);
  });

  it('should handle error when confirming responsibilities', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    service.confirmResponsibilities(mockResponsibilities).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('CONFIRMRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/confirmResponsibilities/`
    );
    expect(req.request.method).toBe('POST');
    req.flush('CONFIRMRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get historical shift change list successfully', () => {
    const mockShiftChangeList = [
      { id: 1, name: 'shift1' },
      { id: 2, name: 'shift2' },
    ];

    service
      .getHistoricalShiftChangeList(new ResponsibilitySearchFilter())
      .subscribe(shiftChangeList => {
        expect(shiftChangeList).toEqual(mockShiftChangeList);
      });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/shiftChangeList/`);
    expect(req.request.method).toBe('POST');
    req.flush(mockShiftChangeList);
  });

  it('should handle error when getting historical shift change list', () => {
    service
      .getHistoricalShiftChangeList(new ResponsibilitySearchFilter())
      .subscribe(
        () => {},
        error => {
          expect(error.error).toEqual('GETHISTORICALSHIFTCHANGELIST_ERROR');
        }
      );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/shiftChangeList/`);
    expect(req.request.method).toBe('POST');
    req.flush('GETHISTORICALSHIFTCHANGELIST_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });

  it('should get historical responsibilities successfully', () => {
    const mockResponsibilities = [
      { id: 1, name: 'responsibility1' },
      { id: 2, name: 'responsibility2' },
    ];

    service.getHistoricalResponsibilities(2).subscribe(responsibilities => {
      expect(responsibilities).toEqual(mockResponsibilities);
    });

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/historicalResponsibilities/2`
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockResponsibilities);
  });

  it('should handle error when getting historical responsibilities', () => {
    service.getHistoricalResponsibilities(2).subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('GETHISTORICALRESPONSIBILITIES_ERROR');
      }
    );

    const req = httpMock.expectOne(
      `${Globals.BASE_URL}/historicalResponsibilities/2`
    );
    expect(req.request.method).toBe('GET');
    req.flush('GETHISTORICALRESPONSIBILITIES_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });
});
