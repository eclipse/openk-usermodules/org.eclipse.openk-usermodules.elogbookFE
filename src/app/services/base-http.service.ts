/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UUID } from 'angular2-uuid';
import { throwError } from 'rxjs';
import { BannerMessage } from '../common/banner-message';
import { ErrorType } from '../common/enums';
import { Globals } from '../common/globals';
import { SessionContext } from '../common/session-context';
import { MessageService } from './message.service';

@Injectable()
export class BaseHttpService {
  constructor(protected messageService: MessageService) {}

  protected getBaseUrl(): string {
    return Globals.BASE_URL;
  }

  protected createCommonHeaders(
    headers: HttpHeaders,
    sessionContext: SessionContext
  ): HttpHeaders {
    headers = headers.append('Accept', 'application/json');
    headers = headers.append('content-Type', 'application/json');
    headers = headers.append('Access-Control-Allow-Origin', '*');
    headers = headers.set(
      'Authorization',
      'Bearer ' + sessionContext.getAccessToken()
    );
    headers = headers.append('unique-TAN', UUID.UUID());
    const sessionId = sessionContext.getCurrSessionId();
    if (sessionId !== null) {
      headers = headers.append(Globals.SESSION_TOKEN_TAG, sessionId);
    }

    return headers;
  }

  protected extractSessionId(
    headers: HttpHeaders,
    sessionContext: SessionContext
  ) {
    if (headers != null) {
      const sessionId = headers.get(Globals.SESSION_TOKEN_TAG);
      if (sessionId) {
        sessionContext.setCurrSessionId(sessionId);
      }
    }
  }

  protected handleErrorPromise(response: any) {
    if (
      response.error !== null &&
      response.error.errorCode === 401 &&
      this.messageService
    ) {
      const bannerMessage = new BannerMessage();
      bannerMessage.errorType = ErrorType.authentication;
      this.messageService.errorOccured$.emit(bannerMessage);
    }

    return throwError(() => response);
  }
}
