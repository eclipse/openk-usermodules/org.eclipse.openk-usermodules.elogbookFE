/**
******************************************************************************
* Copyright © 2017-2018 PTA GmbH.
* All rights reserved. This program and the accompanying materials
* are made available under the terms of the Eclipse Public License v1.0
* which accompanies this distribution, and is available at
* 
*     http://www.eclipse.org/legal/epl-v10.html
* 
******************************************************************************
*/
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService, MessageDefines } from '../services/message.service';
import { BannerMessage } from '../common/banner-message';
import { SessionContext } from '../common/session-context';
import { BannerMessageStatusEn, ErrorType } from '../common/enums';

@Injectable()
export class HttpResponseInterceptorService {

  constructor(
    private messageService: MessageService,
    private router: Router,
    private sessionContext: SessionContext) { this.init(); }

  private init(): void {
    this.subscribeToMessageService();
  }

  private subscribeToMessageService() {
    this.messageService.errorOccured$.subscribe((errorMessage: BannerMessage) => {
      if ((errorMessage.errorType !== ErrorType.authentication)) {
        this.sessionContext.bannerMessage = errorMessage;

      } else {
        this.router.navigate(['logout']);
      }
    });
  }
}
