/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { NotificationFileModel } from '../model/file-model';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';
@Injectable()
export class ImportService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    protected messageService: MessageService
  ) {
    super(messageService);
  }

  public getImportFiles(): Observable<NotificationFileModel[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/getImportFiles/';

    return this._http
      .get<NotificationFileModel[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public deleteImportFile(fileName: string): Observable<boolean> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/deleteImportedFiles/' + fileName;

    return this._http
      .delete<boolean>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }

  public importFile(): Observable<NotificationFileModel[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/importFile/';

    return this._http
      .get<NotificationFileModel[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }
}
