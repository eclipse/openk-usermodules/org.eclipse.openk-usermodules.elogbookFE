/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { BaseDataService } from './base-data.service';
import { MessageService } from './message.service';
import { SessionContext } from '../common/session-context';
import { Status } from '../model/status';
import { Priority } from '../model/priority';
import { Branch } from '../model/branch';
import { GridTerritory } from '../model/gridterritory';

describe('BaseDataService', () => {
  let service: BaseDataService;
  let httpMock: HttpTestingController;
  let messageService: MessageService;
  let sessionContext: SessionContext;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BaseDataService, MessageService, SessionContext],
    });

    service = TestBed.inject(BaseDataService);
    httpMock = TestBed.inject(HttpTestingController);
    messageService = TestBed.inject(MessageService);
    sessionContext = TestBed.inject(SessionContext);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get statuses', () => {
    const mockStatuses: Status[] = [
      { id: 1, name: 'status1' },
      { id: 2, name: 'status2' },
    ];

    service.getStatuses().subscribe(statuses => {
      expect(statuses.length).toBe(2);
      expect(statuses).toEqual(mockStatuses);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/notificationStatuses/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockStatuses);
  });

  it('should get priorities', () => {
    const mockPriorities: Priority[] = [
      { id: 1, name: 'priority1', weighting: 1, imageName: 'image1' },
      { id: 2, name: 'priority2', weighting: 1, imageName: 'image1' },
    ];

    service.getPriorities().subscribe(priorities => {
      expect(priorities.length).toBe(2);
      expect(priorities).toEqual(mockPriorities);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/notificationPriorities/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockPriorities);
  });

  it('should get branches', () => {
    const mockBranches: Branch[] = [
      { id: 1, name: 'branch1', description: 'description1' },
      { id: 2, name: 'branch2', description: 'description2' },
    ];

    service.getBranches().subscribe(branches => {
      expect(branches.length).toBe(2);
      expect(branches).toEqual(mockBranches);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/branches/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockBranches);
  });

  it('should get grid territories', () => {
    const mockTerritories: GridTerritory[] = [
      {
        id: 1,
        name: 'territory1',
        description: 'description1',
        fkRefMaster: 1,
      },
      {
        id: 2,
        name: 'territory2',
        description: 'description2',
        fkRefMaster: 2,
      },
    ];

    service.getGridTerritories().subscribe(territories => {
      expect(territories.length).toBe(2);
      expect(territories).toEqual(mockTerritories);
    });

    const req = httpMock.expectOne(
      (service as any).getBaseUrl() + '/gridTerritories/'
    );
    expect(req.request.method).toBe('GET');
    req.flush(mockTerritories);
  });
});
