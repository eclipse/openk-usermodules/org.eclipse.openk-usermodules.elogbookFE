/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { VersionInfoService } from './version-info.service';
import { SessionContext } from '../common/session-context';
import { MessageService } from './message.service';
import { VersionInfo } from '../model/version-info';
import { Globals } from 'app/common/globals';

describe('VersionInfoService', () => {
  let service: VersionInfoService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [VersionInfoService, SessionContext, MessageService],
    });

    service = TestBed.inject(VersionInfoService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify(); // Ensure that there are no outstanding requests.
  });

  it('should load backend server info successfully', () => {
    const mockVersionInfo: VersionInfo = {
      backendVersion: '1.0.0',
      frontendVersion: '2022-01-01',
    };

    service.loadBackendServerInfo().subscribe(versionInfo => {
      expect(versionInfo).toEqual(mockVersionInfo);
    });

    const req = httpMock.expectOne(`${Globals.BASE_URL}/versionInfo`);
    expect(req.request.method).toBe('GET');
    req.flush(mockVersionInfo);
  });

  it('should handle error when loading backend server info', () => {
    service.loadBackendServerInfo().subscribe(
      () => {},
      error => {
        expect(error.error).toEqual('LOADBACKENDSERVERINFO_ERROR');
      }
    );

    const req = httpMock.expectOne(`${Globals.BASE_URL}/versionInfo`);
    expect(req.request.method).toBe('GET');
    req.flush('LOADBACKENDSERVERINFO_ERROR', {
      status: 500,
      statusText: 'Internal Server Error',
    });
  });
});
