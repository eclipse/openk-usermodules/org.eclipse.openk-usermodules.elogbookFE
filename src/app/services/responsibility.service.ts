/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HistoricalShiftChanges } from 'app/model/historical-shift-changes';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SessionContext } from '../common/session-context';
import { ResponsibilitySearchFilter } from '../model/responsibility-search-filter';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { BaseHttpService } from './base-http.service';
import { MessageService } from './message.service';

@Injectable()
export class ResponsibilityService extends BaseHttpService {
  constructor(
    private _http: HttpClient,
    private _sessionContext: SessionContext,
    public messageService: MessageService
  ) {
    super(messageService);
  }

  public getAllResponsibilities(): Observable<TerritoryResponsibility[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<TerritoryResponsibility[]>(
        super.getBaseUrl() + '/allResponsibilities',
        { headers: headers }
      )
      .pipe(catchError(super.handleErrorPromise));
  }

  public getResponsibilities(): Observable<TerritoryResponsibility[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<TerritoryResponsibility[]>(
        super.getBaseUrl() + '/currentResponsibilities',
        { headers: headers }
      )
      .pipe(catchError(super.handleErrorPromise));
  }

  public getPlannedResponsibilities(): Observable<TerritoryResponsibility[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    return this._http
      .get<TerritoryResponsibility[]>(
        super.getBaseUrl() + '/plannedResponsibilities',
        { headers: headers }
      )
      .pipe(catchError(super.handleErrorPromise));
  }

  public planResponsibilities(responsibilities: TerritoryResponsibility[]) {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/postResponsibilities/';

    return this._http
      .post<TerritoryResponsibility[]>(url, JSON.stringify(responsibilities), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public fetchResponsibilities(responsibilities: TerritoryResponsibility[]) {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/fetchResponsibilities/';

    return this._http
      .post<TerritoryResponsibility[]>(url, JSON.stringify(responsibilities), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public confirmResponsibilities(responsibilities: TerritoryResponsibility[]) {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/confirmResponsibilities/';

    return this._http
      .post<TerritoryResponsibility[]>(url, JSON.stringify(responsibilities), {
        headers: headers,
      })
      .pipe(catchError(super.handleErrorPromise));
  }

  public getHistoricalShiftChangeList(
    notificationSearchFilter: ResponsibilitySearchFilter
  ): Observable<HistoricalShiftChanges> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url = super.getBaseUrl() + '/shiftChangeList/';

    return this._http
      .post<HistoricalShiftChanges>(
        url,
        JSON.stringify(notificationSearchFilter),
        { headers: headers }
      )
      .pipe(catchError(super.handleErrorPromise));
  }

  public getHistoricalResponsibilities(
    transactionId: number
  ): Observable<TerritoryResponsibility[]> {
    let headers = new HttpHeaders();
    headers = this.createCommonHeaders(headers, this._sessionContext);
    const url =
      super.getBaseUrl() + '/historicalResponsibilities/' + transactionId;

    return this._http
      .get<TerritoryResponsibility[]>(url, { headers: headers })
      .pipe(catchError(super.handleErrorPromise));
  }
}
