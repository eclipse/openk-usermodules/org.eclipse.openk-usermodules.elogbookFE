/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { DebugElement, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { UserService } from 'app/services/user.service';
import { AbstractMockObservableService } from '../common/abstract-mock-observable.service';
import { SessionContext } from '../common/session-context';
import { FilterMatrix } from '../model/controller-model/filter-matrix';
import { MessageService } from '../services/message.service';
import { ResponsibilityService } from '../services/responsibility.service';
import { RESPONSIBILITIES } from '../test-data/responsibilities';
import { USERS } from '../test-data/users';
import { MockComponent } from '../testing/mock.component';
import { FilterComponent } from './filter.component';
import { of, throwError } from 'rxjs';

export class FilterMocker {
  public static getComponentMocks() {
    return [
      MockComponent({
        selector: 'app-filter',
        inputs: [
          'shiftChangeProtocolConfirmed',
          'shiftChangeProtocolOpened',
          'filterExpanded',
        ],
      }),
    ];
  }
}

class MockUserService extends AbstractMockObservableService {
  getUsers() {
    return this;
  }

  getUserSettings() {
    return this;
  }

  postUserSettings() {
    return this;
  }
}

class MockBtbService extends AbstractMockObservableService {
  getAllResponsibilities() {
    return of([]);
  }
}

describe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;
  let mockService: MockBtbService;
  let sessionContext: SessionContext;
  let messageService: MessageService;
  let mockUserService;

  beforeEach(async () => {
    mockService = new MockBtbService();
    messageService = new MessageService();
    sessionContext = new SessionContext();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [FilterComponent],
      providers: [
        { provide: MessageService, useValue: messageService },
        { provide: ResponsibilityService, useValue: mockService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    sessionContext.clearStorage();
    sessionContext.setCurrUser(USERS[0]);
    component.sessionContext = sessionContext;
  });

  it('should ignore getSessionFilterMatrix on first init', async () => {
    spyOn(component, 'getSessionFilterMatrix').and.callThrough();
    const resps = RESPONSIBILITIES;
    mockService.content = resps;
    fixture.detectChanges();

    component.ngOnChanges({
      shiftChangeProtocolConfirmed: new SimpleChange(null, false, true),
    });

    fixture.whenStable().then(() => {
      expect(component.filterMatrix).toBeNull();
      expect(component.user).toEqual(sessionContext.getCurrUser());
      expect(component.getSessionFilterMatrix).toHaveBeenCalled();
    });
  });

  it('should retrieve all responsibilities onInit/onChange and put the filtermatrix into SessionContext', async () => {
    spyOn(component, 'initFilterMatrixWithDefaults').and.callThrough();
    spyOn(mockService, 'getAllResponsibilities').and.returnValue(
      of(RESPONSIBILITIES)
    );
    const resps = RESPONSIBILITIES;
    sessionContext.setAllUsers(USERS);
    mockService.content = resps;
    fixture.detectChanges();

    component.ngOnChanges({
      shiftChangeProtocolOpened: new SimpleChange(null, false, true),
    });

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(
        sessionContext.getfilterMatrix()?.responsibilityContainerMatrix?.length
      ).toBeGreaterThan(0);
      expect(component.initFilterMatrixWithDefaults).toHaveBeenCalled();
    });
  });

  it('should show an error message on the banner when an error occurs while running "initFilterMatrixWithDefaults"', async () => {
    spyOn(mockService, 'getAllResponsibilities').and.returnValue(
      throwError(() => 'initFilterMatrixWithDefaults_ERROR')
    );
    let hasBeenCalled = false;
    mockService.error = 'initFilterMatrixWithDefaults_ERROR';
    messageService.errorOccured$.subscribe(() => (hasBeenCalled = true));
    fixture.detectChanges();

    component.ngOnChanges({
      shiftChangeProtocolOpened: new SimpleChange(null, false, true),
    });

    fixture.whenStable().then(() => {
      expect(hasBeenCalled).toBeTruthy();
    });
  });

  xit('should refresh responsibilities (filtermatrix) on checkbox change', async () => {
    spyOn(component, 'responsibilitiesSelectionChanged').and.callThrough();
    const resps = RESPONSIBILITIES;
    sessionContext.setAllUsers(USERS);
    mockService.content = resps;
    fixture.detectChanges();

    component.ngOnChanges({
      shiftChangeProtocolOpened: new SimpleChange(null, false, true),
    });
    fixture.detectChanges();

    const des: DebugElement[] = fixture.debugElement.queryAll(By.css('input'));
    fixture.detectChanges();
    des[1].nativeElement.click();
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const checkedResp =
        component.responsibilityContainerMatrix[0].responsibilityList.find(
          responsibility => responsibility.branchName === 'G'
        );

      expect(checkedResp?.isActive).toBe(true);
      expect(component.responsibilitiesSelectionChanged).toHaveBeenCalled();
    });
  });

  it('should retrieve the updated responsibilities', async () => {
    spyOn(component, 'getUpdatedResponsibilities').and.callThrough();
    const responsibilityContainerMatrixOld = RESPONSIBILITIES;
    const resps = RESPONSIBILITIES;
    sessionContext.setAllUsers(USERS);
    const newResponsibleUser = 'otto';
    resps[0].responsibilityList[0].responsibleUser = newResponsibleUser;
    const filterMatrix: FilterMatrix = new FilterMatrix(
      responsibilityContainerMatrixOld
    );

    sessionContext.setfilterMatrix(filterMatrix);
    mockService.content = resps;
    fixture.detectChanges();

    component.ngOnChanges({
      shiftChangeProtocolConfirmed: new SimpleChange(null, false, true),
    });

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(
        component.responsibilityContainerMatrix[0].responsibilityList[0]
          .responsibleUser
      ).toEqual(newResponsibleUser);
      expect(
        component.responsibilityContainerMatrix[0].responsibilityList[0]
          .isActive
      ).toBe(
        responsibilityContainerMatrixOld[0].responsibilityList[0].isActive
      );
      expect(component.getUpdatedResponsibilities).toHaveBeenCalled();
    });
  });

  it('should select all responsibilities after button "Alle setzen" pressed', async () => {
    spyOn(component, 'selectAllResponsibilities').and.callThrough();
    const resps = RESPONSIBILITIES;
    sessionContext.setAllUsers(USERS);
    component.responsibilityContainerMatrix = resps;
    fixture.detectChanges();

    const de: DebugElement = fixture.debugElement.query(
      By.css('#selectAllButton')
    );
    de.nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      component.responsibilityContainerMatrix.forEach(
        responsibilityContainerMatrix => {
          responsibilityContainerMatrix.responsibilityList.forEach(
            responsibility => {
              expect(responsibility.isActive).toBe(true);
            }
          );
        }
      );
      expect(component.selectAllResponsibilities).toHaveBeenCalled();
    });
  });

  it('should deselect all responsibilities after button "Alle löschen" pressed', async () => {
    spyOn(component, 'deselectAllResponsibilities').and.callThrough();
    const resps = RESPONSIBILITIES;
    sessionContext.setAllUsers(USERS);
    component.responsibilityContainerMatrix = resps;
    fixture.detectChanges();

    const de: DebugElement = fixture.debugElement.query(
      By.css('#deselectAllButton')
    );
    de.nativeElement.click();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        component.responsibilityContainerMatrix.forEach(
          responsibilityContainerMatrix => {
            responsibilityContainerMatrix.responsibilityList.forEach(
              responsibility => {
                expect(responsibility.isActive).toBe(false);
              }
            );
          }
        );
        expect(component.deselectAllResponsibilities).toHaveBeenCalled();
      });
    });
  });
});
