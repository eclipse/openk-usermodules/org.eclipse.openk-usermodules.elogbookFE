/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { UserSettings } from 'app/model/user-settings';
import { UserService } from 'app/services/user.service';
import { Subject, catchError, takeUntil } from 'rxjs';
import { BannerMessage } from '../common/banner-message';
import { BannerMessageStatusEn, ErrorType } from '../common/enums';
import { SessionContext } from '../common/session-context';
import { FilterMatrix } from '../model/controller-model/filter-matrix';
import { FilterSelection } from '../model/filter-selection';
import { Responsibility } from '../model/responsibility';
import { TerritoryResponsibility } from '../model/territory-responsibility';
import { User } from '../model/user';
import { MessageService } from '../services/message.service';
import { ResponsibilityService } from '../services/responsibility.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],
})
export class FilterComponent implements OnInit, OnChanges, OnDestroy {
  user: User | null = null;
  bannerMessage: BannerMessage = new BannerMessage();
  bannerMessageStatus = BannerMessageStatusEn;

  @Input() shiftChangeProtocolConfirmed: boolean = false;
  @Input() shiftChangeProtocolOpened: boolean = false;
  @Input() filterExpanded: boolean = false;

  responsibilityContainerMatrix: TerritoryResponsibility[] = [];
  filterSelection: FilterSelection = new FilterSelection();
  filterMatrix: FilterMatrix | null = null;

  private endOfSubscription$: Subject<void> = new Subject<void>();

  constructor(
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService,
    private messageService: MessageService,
    private userService: UserService
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['shiftChangeProtocolConfirmed'] !== undefined &&
      changes['shiftChangeProtocolConfirmed'].currentValue !== undefined
    ) {
      this.getSessionFilterMatrix();
    }

    if (
      changes['shiftChangeProtocolOpened'] !== undefined &&
      changes['shiftChangeProtocolOpened'].currentValue !== undefined
    ) {
      if (!changes['shiftChangeProtocolOpened'].currentValue) {
        this.initFilterMatrixWithDefaults();
      }
    }
  }

  ngOnInit() {
    this.user = this.sessionContext.getCurrUser();
    this.messageService.respConfirmed$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.onRespForCurrentUserChanged());
  }

  ngOnDestroy(): void {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  getResponsiblity(
    responsibilityContainer: TerritoryResponsibility,
    branchName: string
  ): Responsibility | null {
    if (responsibilityContainer.responsibilityList) {
      const resp = responsibilityContainer.responsibilityList.find(
        responsibility => responsibility.branchName === branchName
      );
      if (resp) {
        return resp;
      } else {
        return null;
      }
    } else {
      console.warn('No responsibilityList found');
      return null;
    }
  }

  onRespForCurrentUserChanged() {
    this.getSessionFilterMatrix();
    this.filterExpanded = true;
  }

  initFilterMatrixWithDefaults() {
    this.filterMatrix = this.sessionContext.getfilterMatrix();
    if (!this.filterMatrix) {
      const userSettings: UserSettings = this.sessionContext.getUsersSettings();

      this.responsibilityService
        .getAllResponsibilities()
        .pipe(
          takeUntil(this.endOfSubscription$),
          catchError(() => {
            this.messageService.emitError('Filtermatrix', ErrorType.retrieve);
            return [];
          })
        )
        .subscribe(resps => {
          this.responsibilityContainerMatrix = resps;
          for (const responsibilityContainer of this
            .responsibilityContainerMatrix) {
            for (const responsibility of responsibilityContainer.responsibilityList) {
              if (
                userSettings &&
                userSettings.activeFilterRespIds &&
                responsibility.id
              ) {
                responsibility.isActive =
                  userSettings.activeFilterRespIds.indexOf(responsibility.id) >
                  -1;
              } else {
                if (this.user) {
                  //If no there is no custom user setting, default value will be used
                  responsibility.isActive =
                    responsibility.responsibleUser === this.user.username;
                }
              }
            }
          }
          this.responsibilitiesSelectionChanged(false);
        });
    }
  }

  getSessionFilterMatrix() {
    this.filterMatrix = this.sessionContext.getfilterMatrix();
    if (this.filterMatrix) {
      this.getUpdatedResponsibilities();
    }
  }

  getUpdatedResponsibilities() {
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();
    let updatedMatrix: TerritoryResponsibility[] = [];
    this.responsibilityService
      .getAllResponsibilities()
      .pipe(
        takeUntil(this.endOfSubscription$),
        catchError(() => {
          this.messageService.emitError('Filtermatrix', ErrorType.retrieve);
          return [];
        })
      )
      .subscribe(resps => {
        updatedMatrix = resps;
        for (const updatedContainer of updatedMatrix) {
          for (const updatedResp of updatedContainer.responsibilityList) {
            if (
              userSettings &&
              userSettings.activeFilterRespIds &&
              updatedResp.id
            ) {
              updatedResp.isActive =
                userSettings.activeFilterRespIds.indexOf(updatedResp.id) > -1;
            } else {
              if (this.user) {
                //If no there is no custom user setting, default value will be used
                updatedResp.isActive =
                  updatedResp.responsibleUser === this.user.username;
              }
            }
          }
        }

        this.responsibilityContainerMatrix = updatedMatrix;
        const filterMatrix: FilterMatrix = new FilterMatrix(updatedMatrix);
        this.messageService.matrixFilterChanged$.emit(
          filterMatrix.getNumFilterList()
        );
      });
  }

  selectAllResponsibilities() {
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = true;
      }
    }
    this.responsibilitiesSelectionChanged(true);
  }

  deselectAllResponsibilities() {
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = false;
      }
    }
    this.responsibilitiesSelectionChanged(true);
  }

  resetToDefaultAllResponsibilities() {
    for (const responsibilityContainer of this.responsibilityContainerMatrix) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        if (this.user) {
          responsibility.isActive =
            responsibility.responsibleUser === this.user.username;
        }
      }
    }
    this.responsibilitiesSelectionChanged(false);
    this.resetUserSettingsToDefault();
  }

  responsibilitiesSelectionChanged(updateUserSettings: boolean) {
    const filterMatrix: FilterMatrix = new FilterMatrix(
      this.responsibilityContainerMatrix
    );
    this.sessionContext.setfilterMatrix(filterMatrix);

    if (updateUserSettings) {
      this.updateUserSettingsResponsibilities(filterMatrix.getNumFilterList());
    }

    this.messageService.matrixFilterChanged$.emit(
      filterMatrix.getNumFilterList()
    );
  }

  mapUserName(shortUsr: string | null | undefined): string | null | undefined {
    const userMap = this.sessionContext.getUserMap();
    return userMap
      ? this.sessionContext.getUserMap().findAndRenderUser(shortUsr)
      : shortUsr;
  }

  updateUserSettingsResponsibilities(numFilterList: number[]) {
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();
    userSettings.activeFilterRespIds = numFilterList;
    this.updateUserSettings(userSettings);
  }

  updateUserSettings(userSettings: UserSettings) {
    this.sessionContext.setUsersSettings(userSettings);
    this.userService
      .postUserSettings(userSettings)
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe();
  }

  resetUserSettingsToDefault() {
    const userSettings: UserSettings = new UserSettings();
    this.updateUserSettings(userSettings);
  }
}
