/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';

import { Overlay } from '@angular/cdk/overlay';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { Responsibility } from '../../model/responsibility';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import {
  DUMMY_CREATED_NOTIFICATION,
  OPEN_NOTIFICATIONS,
} from '../../test-data/notifications';
import { RESPONSIBILITIES } from '../../test-data/responsibilities';
import { MockComponent } from '../../testing/mock.component';
import { ShiftChangeProtocolComponent } from './shift-change-protocol.component';
import { MessageBannerComponent } from 'app/common-components/message-banner/message-banner.component';
import { DateTimePickerComponent } from 'app/common-components/date-time-picker/date-time-picker.component';

describe('ShiftChangeProtocolComponent', () => {
  let component: ShiftChangeProtocolComponent;
  let fixture: ComponentFixture<ShiftChangeProtocolComponent>;
  let mockService: any;
  let mockNotificationService: MockNotificationService;

  const correctUser: User = {
    id: 44,
    username: 'carlo',
    name: 'Carlo Cottura',
    specialUser: true,
    itemName: 'carlo',
  };

  class MockDialogRef extends AbstractMockObservableService {
    close() {}
  }

  class MockBtbService extends AbstractMockObservableService {
    getPlanedResponsibilities() {
      return of([]);
    }
    confirmResponsibilities(resp: Responsibility[]) {
      console.log(resp);
      return of([]);
    }
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getFinishedNotifications(notificationType: string) {
      console.log(notificationType);
      this.loadCalled = true;
      return of([]);
    }

    public getNotificationVersions(incidentId: number) {
      console.log(incidentId);

      const notifaction = DUMMY_CREATED_NOTIFICATION;
      return of(notifaction);
    }
  }

  let sessionContext: any;
  let messageService: any;

  beforeEach(async () => {
    sessionContext = new SessionContext();
    mockService = new MockBtbService();
    mockNotificationService = new MockNotificationService();
    messageService = new MessageService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {});

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        DateTimePickerComponent,
      ],
      declarations: [
        StringToDatePipe,
        FormattedTimestampPipe,
        ShiftChangeProtocolComponent,
        EntryComponent,
        MockComponent({ selector: 'input', inputs: ['options'] }),
        EntryComponentMocker.getComponentMocks(),
        MessageBannerComponent,
        MockComponent({
          selector: 'app-responsibility',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible',
          ],
        }),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-open-notifications',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible',
            'stayHidden',
            'enforceShowReadOnly',
            'gridId',
          ],
        }),
        MockComponent({
          selector: 'app-finished-notifications ',
          inputs: [
            'responsiblitySelection',
            'withCheckboxes',
            'withEditButtons',
            'isCollapsible',
            'stayHidden',
            'enforceShowReadOnly',
            'gridId',
          ],
        }),
      ],
      providers: [
        { provide: MatDialogRef, useClass: MockDialogRef },
        { provide: Overlay, useClass: Overlay },
        { provide: MessageService, useValue: messageService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: ResponsibilityService, useValue: mockService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftChangeProtocolComponent);
    component = fixture.componentInstance;
  });

  it('should open dialog to lookup a notification on button click', async () => {
    spyOn(component.dialogRef, 'close').and.callThrough();
    spyOn(component.messageService, 'deactivateMessage').and.callThrough();
    let entryOpend = false;
    let dialog: any;

    sessionContext.setCurrUser(correctUser);
    mockNotificationService.content = OPEN_NOTIFICATIONS;
    const testNotification = OPEN_NOTIFICATIONS[0];
    mockService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });

    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);
      component.close();
      expect(component.dialogRef.close).toHaveBeenCalled();
      expect(component.messageService.deactivateMessage).toHaveBeenCalled();
    });
  });
});
