/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  Component,
  EventEmitter,
  OnInit,
  Optional,
  Output,
} from '@angular/core';
import {
  MatDialog,
  MatDialogConfig,
  MatDialogRef,
} from '@angular/material/dialog';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { Notification } from '../../model/notification';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';

@Component({
  selector: 'app-shift-change-protocol',
  templateUrl: './shift-change-protocol.component.html',
  styleUrls: [
    './shift-change-protocol.component.css',
    '../../lists/abstract-list/abstract-list.component.css',
  ],
})
export class ShiftChangeProtocolComponent implements OnInit {
  @Output() lookUpNotificationEmitter = new EventEmitter<Notification>();

  private dialogConfig = new MatDialogConfig();
  user: User | null = null;
  responsibilitiesContainer: TerritoryResponsibility[] = [];

  constructor(
    public dialog: MatDialog,
    @Optional() public dialogRef: MatDialogRef<ShiftChangeProtocolComponent>,
    private responsibilityService: ResponsibilityService,
    public messageService: MessageService,
    private sessionContext: SessionContext
  ) {}

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
  }

  setResponsibilitiesContainer(
    responsibilitiesContainer: TerritoryResponsibility[]
  ) {
    this.responsibilitiesContainer = responsibilitiesContainer;
  }

  close() {
    this.dialogRef.close();
    this.messageService.deactivateMessage();
  }

  openDialogLookUpEntry(notification: Notification) {
    const lookupDlgRef = this.dialog.open(EntryComponent, this.dialogConfig);
    // TODO: check init of sessionContext
    lookupDlgRef.componentInstance.user = this.user;
    lookupDlgRef.componentInstance.setNotification(notification);
    lookupDlgRef.componentInstance.isReadOnlyDialog = true;
    lookupDlgRef.afterClosed().subscribe(() => {});
  }
}
