/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { catchError, of, throwError } from 'rxjs';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { UserService } from '../../services/user.service';
import {
  RESPONSIBILITIES,
  RESPONSIBILITIES_SHIFT_CHANGE,
} from '../../test-data/responsibilities';
import { USERS } from '../../test-data/users';
import { click } from '../../testing';
import { MockComponent } from '../../testing/mock.component';
import { ShiftChangeComponent } from './shift-change.component';
import { ErrorType } from 'app/common/enums';

let component: ShiftChangeComponent;
let fixture: ComponentFixture<ShiftChangeComponent>;
let page: Page;

describe('ShiftChangeComponent-Post/Plan', () => {
  class MockDialogRef extends AbstractMockObservableService {
    close() {}
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return of(USERS);
    }
  }
  let mockUserService: MockUserService;

  class MockResponsibilityService extends AbstractMockObservableService {
    getResponsibilities() {
      return of([]);
    }
    planResponsibilities(newResponsibility: TerritoryResponsibility[]) {
      console.log(newResponsibility);

      return of([]);
    }
  }
  let mockService: MockResponsibilityService;
  let sessionContext: SessionContext;
  let messageService: MessageService;

  beforeEach(async () => {
    mockUserService = new MockUserService();
    mockService = new MockResponsibilityService();
    sessionContext = new SessionContext();
    messageService = new MessageService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        ShiftChangeComponent,
        MockComponent({
          selector: 'multiselect',
          inputs: ['data', 'settings'],
        }),
      ],
      providers: [
        { provide: MatDialogRef, useClass: MockDialogRef },
        { provide: UserService, useValue: mockUserService },
        { provide: ResponsibilityService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: SessionContext, useValue: sessionContext },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ShiftChangeComponent);
    component = fixture.componentInstance;
  });
  it('should show banner info when confirmResponsibilities clicked but responsibilities changed meanwhile from other user', async () => {
    let hasBeenCalled = false;
    messageService.errorOccured$.subscribe(() => (hasBeenCalled = true));
    fixture.componentInstance.responsibilityContainers = RESPONSIBILITIES;
    fixture.detectChanges();
    page = new Page();
    page.addPageElements();
    mockService.subscribe(() => {
      mockService.content = [];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(hasBeenCalled).toBeTruthy();
      });
    });
    if (page.confirmBtn) {
      click(page.confirmBtn);
    }
  });

  it(
    'should not show banner info when confirmResponsibilities clicked ' +
      'but responsibilities changed meanwhile from other user',
    async () => {
      fixture.componentInstance.responsibilityContainers = RESPONSIBILITIES;
      page = new Page();
      page.addPageElements();

      mockService.subscribe(() => {
        fixture.detectChanges();
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          const des: DebugElement[] = fixture.debugElement.queryAll(
            By.css('.alert-info')
          );

          expect(des.length).toBe(0);
        });
      });
      if (page.confirmBtn) {
        click(page.confirmBtn);
      }
    }
  );

  it('selectboxes should be preselected', async () => {
    const resp = RESPONSIBILITIES;
    const testUser = USERS[0];
    resp.forEach(responsibilityContainer => {
      responsibilityContainer.responsibilityList.forEach(responsibility => {
        responsibility.newResponsibleUser = testUser.username;
      });
    });

    fixture.componentInstance.preSelectedUsers = USERS;
    fixture.componentInstance.responsibilityContainers = resp;
    page = new Page();
    page.addPageElements();

    let elLocal: HTMLSelectElement;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const des: DebugElement[] = fixture.debugElement.queryAll(
        By.css('.shc-table-select')
      );

      des.forEach(de => {
        elLocal = de.nativeElement;

        const option = elLocal.options[elLocal.options.selectedIndex];
        expect(option.textContent).toBe(testUser.name);
      });
    });
  });

  it('selectboxes should be set to empty option after cancle click', async () => {
    const resp = RESPONSIBILITIES;
    const testUser = USERS[0];
    resp.forEach(responsibilityContainer => {
      responsibilityContainer.responsibilityList.forEach(responsibility => {
        responsibility.newResponsibleUser = testUser.username;
      });
    });

    fixture.componentInstance.preSelectedUsers = USERS;
    fixture.componentInstance.responsibilityContainers = resp;
    fixture.detectChanges();
    page = new Page();
    page.addPageElements();

    let elLocal: HTMLSelectElement;

    if (page.stornoBtn) {
      click(page.stornoBtn);
    }
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const des: DebugElement[] = fixture.debugElement.queryAll(
        By.css('.shc-table-select')
      );

      des.forEach(de => {
        elLocal = de.nativeElement;

        elLocal.options[elLocal.options.selectedIndex];
        expect(elLocal.options.selectedIndex).toBe(0);
      });
    });
  });

  it('should call getResponsibilities service method and return an error', async () => {
    spyOn(mockService, 'getResponsibilities').and.returnValue(
      throwError(() => 'GETRESPONSIBILITIES_ERROR')
    );
    const spy = spyOn(messageService, 'emitError');

    fixture.detectChanges();
    component.getResponsibilities();
    fixture.detectChanges();
    mockService
      .getResponsibilities()
      .pipe(
        catchError(() => {
          fixture.detectChanges();
          expect(spy).toHaveBeenCalledWith(
            'Schichtübergabe getResponsibilities',
            ErrorType.retrieve
          );
          return of([]);
        })
      )
      .subscribe();
  });

  it('should call planResponsibilities service method and return an error', async () => {
    spyOn(component.dialogRef, 'close').and.callThrough();
    spyOn(mockService, 'planResponsibilities').and.returnValue(
      throwError(() => 'PLANRESPONSIBILITIES_ERROR')
    );
    const spy = spyOn(messageService, 'emitError');

    const responsibilityContainers = RESPONSIBILITIES_SHIFT_CHANGE;
    const serviceUsers = USERS;
    mockUserService.content = serviceUsers;
    const serviceResp = RESPONSIBILITIES;
    mockService.content = serviceResp;
    sessionContext.setCurrUser(USERS[1]);

    component.shiftChange(true);

    fixture.detectChanges();
    mockService
      .planResponsibilities(responsibilityContainers)
      .pipe(
        catchError(() => {
          fixture.detectChanges();
          expect(spy).toHaveBeenCalledWith('Schichtübergabe', ErrorType.update);
          expect(component.dialogRef.close).toHaveBeenCalled();
          return of([]);
        })
      )
      .subscribe();
  });

  it('should call getUsers service method and return an error', async () => {
    spyOn(mockUserService, 'getUsers').and.returnValue(
      throwError(() => 'GETUSERS_ERROR')
    );
    const spy = spyOn(messageService, 'emitError');
    fixture.detectChanges();
    component.getUsers();
    fixture.detectChanges();

    mockUserService
      .getUsers()
      .pipe(
        catchError(() => {
          fixture.detectChanges();
          expect(spy).toHaveBeenCalledWith(
            'Schichtübergabe getUsers',
            ErrorType.retrieve
          );
          return of([]);
        })
      )
      .subscribe();
  });
});

describe('ShiftChangeComponent-Fetch', () => {
  class MockDialogRef extends AbstractMockObservableService {
    close() {}
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return of([]);
    }
  }

  class MockResponsibilityService extends AbstractMockObservableService {
    public fetchCalled = false;
    getAllResponsibilities() {
      return of([]);
    }
    fetchResponsibilities(newResponsibility: TerritoryResponsibility[]) {
      console.log(newResponsibility);

      this.fetchCalled = true;
      return of([]);
    }
  }
  let mockService: MockResponsibilityService;
  let sessionContext: SessionContext;
  let messageService: MessageService;
  let mockUserService: MockUserService;

  beforeEach(async () => {
    mockUserService = new MockUserService();
    mockService = new MockResponsibilityService();
    sessionContext = new SessionContext();
    messageService = new MessageService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        ShiftChangeComponent,
        MockComponent({
          selector: 'multiselect',
          inputs: ['data', 'settings'],
        }),
      ],
      providers: [
        { provide: MatDialogRef, useClass: MockDialogRef },
        { provide: UserService, useValue: mockUserService },
        { provide: ResponsibilityService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: SessionContext, useValue: sessionContext },
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(ShiftChangeComponent);
    component = fixture.componentInstance;
    component.isFetchingResp = true;
  });
  it('should init for fetch correctly', async () => {
    sessionContext.setAllUsers(USERS);
    sessionContext.setCurrUser(USERS[1]);

    mockService.content = RESPONSIBILITIES;
    mockService.fetchCalled = false;
    fixture.detectChanges();
    page = new Page();
    page.addPageElements();
    mockService.subscribe(() => {
      mockService.content = [];
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(mockService.fetchCalled).toBeTruthy();
      });
    });
    if (page.confirmBtn) {
      click(page.confirmBtn);
    }
  });
});

class Page {
  //  gotoSpy:      jasmine.Spy;
  //  navSpy:       jasmine.Spy;

  cancelBtn: DebugElement | null = null;
  stornoBtn: DebugElement | null = null;
  confirmBtn: DebugElement | null = null;

  constructor() {
    /*    const router = TestBed.get(Router); // get router from root injector
        this.gotoSpy = spyOn(comp, 'gotoList').and.callThrough();
        this.navSpy  = spyOn(router, 'navigate');
        */
  }

  addPageElements() {
    // have a hero so these elements are now in the DOM
    const buttons = fixture.debugElement.queryAll(By.css('button'));

    this.cancelBtn = buttons[2];
    this.stornoBtn = buttons[1];
    this.confirmBtn = buttons[0];
  }
}
