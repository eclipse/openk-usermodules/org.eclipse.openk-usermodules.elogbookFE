/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { AfterViewInit, Component, Input, Optional } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { BannerMessageStatusEn, ErrorType } from '../../common/enums';
import { SessionContext } from '../../common/session-context';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { ShiftChangeProtocolComponent } from '../shift-change-protocol/shift-change-protocol.component';
import { Responsibility } from 'app/model/responsibility';
import { catchError, take } from 'rxjs';

@Component({
  selector: 'app-responsibility',
  templateUrl: './responsibility.component.html',
  styleUrls: ['./responsibility.component.css'],
})
export class ResponsibilityComponent implements AfterViewInit {
  @Input()
  responsiblitySelection: TerritoryResponsibility[] = [];
  @Input()
  withButtons = true;
  @Input()
  withNames = false;

  public readonly bannerMessageStatus = BannerMessageStatusEn;

  constructor(
    @Optional() public dialogRef: MatDialogRef<ShiftChangeProtocolComponent>,
    private messageService: MessageService,
    private sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService
  ) {}

  ngAfterViewInit() {
    this.responsibilitiesSelectionChanged();
  }

  confirm(): void {
    this.responsibilityService
      .confirmResponsibilities(this.responsiblitySelection)

      .pipe(
        take(1),
        catchError(() => {
          this.dialogRef.close();
          this.messageService.emitError(
            'Verantwortlichkeiten',
            ErrorType.update
          );
          return [];
        })
      )
      .subscribe((resps: TerritoryResponsibility[]) => {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        if (resps['ret' as any] && (resps['ret' as any] as any) === 'OK') {
          this.responsiblitySelection = [];
          this.messageService.deactivateMessage();
          this.dialogRef.close();
          //this.messageService.respConfirmed$.emit(true);
        } else {
          const message =
            'Ihre Verantwortlichkeiten haben sich geändert. ' +
            'Bitte prüfen Sie Ihre Eingaben und versuchen Sie es erneut.';
          this.messageService.emitWarning(message);
          this.responsiblitySelection = resps;
        }
      });
  }

  responsibilitiesSelectionChanged() {
    const filterList: number[] = [];

    if (!this.responsiblitySelection) {
      return;
    }

    for (const responsibilityContainer of this.responsiblitySelection) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        if (responsibility.isActive && responsibility.id) {
          filterList.push(responsibility.id);
        }
      }
    }
    this.messageService.matrixFilterChanged$.emit(filterList);
  }

  getResponsiblity(
    responsibilityContainer: TerritoryResponsibility,
    branchName: string
  ): Responsibility | null {
    if (responsibilityContainer.responsibilityList) {
      const resp = responsibilityContainer.responsibilityList.find(
        responsibility => responsibility.branchName === branchName
      );
      if (resp) {
        return resp;
      } else {
        return null;
      }
    } else {
      console.warn('No responsibilityList found');
      return null;
    }
  }

  selectAllResponsibilities() {
    for (const responsibilityContainer of this.responsiblitySelection) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = true;
      }
    }
    this.responsibilitiesSelectionChanged();
  }

  deselectAllResponsibilities() {
    for (const responsibilityContainer of this.responsiblitySelection) {
      for (const responsibility of responsibilityContainer.responsibilityList) {
        responsibility.isActive = false;
      }
    }
    this.responsibilitiesSelectionChanged();
  }

  mapUserName(shortUsr: string | null | undefined): string | null | undefined {
    const userMap = this.sessionContext.getUserMap();
    return userMap
      ? this.sessionContext.getUserMap().findAndRenderUser(shortUsr)
      : shortUsr;
  }
}
