/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Globals } from 'app/common/globals';
import { Priority } from 'app/model/priority';
import { ErrorType, StatusEn } from '../../common/enums';
import { SessionContext } from '../../common/session-context';
import { Branch } from '../../model/branch';
import { GridTerritory } from '../../model/gridterritory';
import { Notification } from '../../model/notification';
import { Status } from '../../model/status';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { take, takeUntil, catchError, Subject } from 'rxjs';
@Component({
  selector: 'app-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.css'],
})
export class EntryComponent implements OnInit, OnDestroy {
  isEditDialog = false;
  isReadOnlyDialog = false;
  isInstructionDialog = false;
  isPresenceReminderDialog = false;
  user: User | null = null;
  notification: Notification = new Notification();
  selectedStatusId: number = 0;
  notificationVersion: Notification = new Notification();
  notificationVersions: Notification[] = [];
  gridTerritories: GridTerritory[] | null = null;
  branches: Branch[] | null = null;
  statuses: Status[] | null = null;
  priorities: Priority[] | null = null;
  defaultBranch = '';
  defaultGridTerritory = '';

  private endOfSubscription$ = new Subject<void>();

  constructor(
    public dialogRef: MatDialogRef<EntryComponent>,
    private notificationService: NotificationService,
    public sessionContext: SessionContext,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.gridTerritories = this.sessionContext.getMasterGridTerritories();
    this.branches = this.sessionContext.getBranches();
    this.statuses = this.sessionContext.getStatuses();
    this.priorities = this.sessionContext.getPriorities();
    this.user = this.sessionContext.getCurrUser();

    if (this.notification && !this.notification.beginDate) {
      this.notification.beginDate = this.getCurrentDateTime();
    }

    this.initPresenceReminderDialog();

    if (this.notification && !this.notification.createUser) {
      this.notification.createUser = this.user.name;
    }

    if (
      this.notification &&
      !this.notification.fkRefBranch &&
      !this.isEditDialog
    ) {
      const filterMatrix = this.sessionContext.getfilterMatrix();
      const username = this.sessionContext.getCurrUser().username;

      if (filterMatrix) {
        const responsibilityContainerMatrix =
          filterMatrix.responsibilityContainerMatrix;
        if (responsibilityContainerMatrix && username) {
          this.defaultBranch = this.getUniqueBranchNameForUser(
            responsibilityContainerMatrix,
            username
          );
        }
      }

      if (this.branches) {
        const branchTmp = this.branches.filter(
          b => b.name === this.defaultBranch
        )[0];
        if (branchTmp && this.notification) {
          this.notification.fkRefBranch = branchTmp.id;
        }
      }
    }

    if (
      this.notification &&
      !this.notification.fkRefGridTerritory &&
      !this.isEditDialog
    ) {
      const filterMatrix = this.sessionContext.getfilterMatrix();
      const username = this.sessionContext.getCurrUser().username;

      if (filterMatrix) {
        const responsibilityContainerMatrix =
          filterMatrix.responsibilityContainerMatrix;
        if (responsibilityContainerMatrix && username) {
          this.defaultGridTerritory = this.getUniqueGridTerritoryNameForUser(
            responsibilityContainerMatrix,
            username
          );
        }
      }
      const gridTerritories = this.sessionContext.getGridTerritories();
      if (gridTerritories) {
        const gridTerritoryTmp = gridTerritories.filter(
          gt => gt.description === this.defaultGridTerritory
        )[0];
        if (gridTerritoryTmp && this.notification) {
          this.notification.fkRefGridTerritory = gridTerritoryTmp.id;
        }
      }
    }
  }

  ngOnDestroy(): void {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  initPresenceReminderDialog() {
    if (this.isPresenceReminderDialog && !this.isEditDialog) {
      const presenceCheckDate: Date = new Date();
      presenceCheckDate.setHours(16);
      presenceCheckDate.setMinutes(0);
      presenceCheckDate.setSeconds(0);
      if (this.notification) {
        this.notification.reminderDate = presenceCheckDate.toISOString();
        this.notification.fkRefNotificationStatus =
          Globals.NOTIFICATION_STATUS_OFFEN;
        this.notification.notificationText = Globals.PRESENCE_CHECK_TEXT;
      }
    }
  }

  onNotificationVersionChange(version: number) {
    const foundedNotification = this.notificationVersions.find(
      notification => Number(notification.version) === Number(version)
    );
    if (foundedNotification) {
      this.notification = foundedNotification;
    } else {
      this.notification = new Notification();
      console.warn('Notification not found');
    }
  }

  setNotification(notification: Notification) {
    this.notification = Object.assign(new Notification(), notification);
    this.notificationVersion = notification;
    if (!this.notification.beginDate) {
      this.notification.beginDate = this.getCurrentDateTime();
    }

    if (!notification.incidentId) {
      console.error('IncidentId not found');
      return;
    }
    this.notificationService
      .getNotificationVersions(notification.incidentId)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(() => {
          this.messageService.emitError('Versionen', ErrorType.retrieve);
          this.dialogRef.close();
          return [];
        })
      )
      .subscribe(notes => (this.notificationVersions = notes));
  }

  edit() {
    if (this.notification && this.user) {
      this.notification.responsibilityControlPoint = this.user.name;
    }
    const notificationClone = Object.assign(
      new Notification(),
      this.notification
    );
    notificationClone.decoratorNotificationVersions = undefined;

    this.notificationService
      .updateNotification(notificationClone)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(() => {
          this.messageService.emitError('Eintrag', ErrorType.update);
          this.dialogRef.close();
          return [];
        })
      )
      .subscribe(() => this.dialogRef.close());
  }

  add() {
    if (this.notification && this.user) {
      this.notification.responsibilityControlPoint = this.user.name;
      this.setNotificationType();
      this.notificationService
        .createNotification(this.notification)
        .pipe(
          take(1),
          takeUntil(this.endOfSubscription$),
          catchError(() => {
            this.messageService.emitError('Eintrags', ErrorType.create);
            this.dialogRef.close();
            return [];
          })
        )
        .subscribe(() => this.dialogRef.close());
    }
  }

  setNotificationType() {
    if (this.notification) {
      this.notification.adminFlag = this.isInstructionDialog;
      if (this.isPresenceReminderDialog) {
        this.notification.type = Globals.NOTIFICATION_TYPE_PRESENCE_CHECK;
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  statusChanged(statusId: number, notification: Notification) {
    const statusIdNum: number = Number(statusId);
    if (statusIdNum === StatusEn.done || statusIdNum === StatusEn.closed) {
      notification.finishedDate = this.getCurrentDateTime();
    } else {
      notification.finishedDate = null;
    }
  }

  getCurrentDateTime() {
    return new Date().toISOString();
  }

  getUniqueBranchNameForUser(
    responsibilityContainerMatrix: TerritoryResponsibility[],
    currentUserName: string
  ): string {
    let uniqueBranchName = '';
    let branchNames: string[] = [];

    responsibilityContainerMatrix.forEach(gridTerritory => {
      gridTerritory.responsibilityList.forEach(responsibility => {
        if (
          responsibility.responsibleUser === currentUserName &&
          responsibility.branchName
        ) {
          branchNames.push(responsibility.branchName);
        }
      });
    });

    branchNames = branchNames.filter(
      (item, pos, ar) => ar.indexOf(item) === pos
    );

    if (branchNames.length === 1) {
      uniqueBranchName = branchNames[0];
    } else {
      uniqueBranchName = '';
    }

    return uniqueBranchName;
  }

  getUniqueGridTerritoryNameForUser(
    responsibilityContainerMatrix: TerritoryResponsibility[],
    currentUserName: string
  ): string {
    let uniqueGridTerritoryName = '';
    let gridTerritoryNames: string[] = [];
    const gridTerritories = this.sessionContext.getGridTerritories();

    if (gridTerritories) {
      responsibilityContainerMatrix.forEach(gridTerritory => {
        gridTerritory.responsibilityList.forEach(responsibility => {
          if (responsibility.responsibleUser === currentUserName) {
            const gtTmp = gridTerritories.filter(
              gt => gt.description === gridTerritory.gridTerritoryDescription
            )[0];
            if (gtTmp) {
              const masterGridTerritoryID = gtTmp.fkRefMaster;
              const masterGridTerritory = gridTerritories.filter(
                mGT => mGT.id === masterGridTerritoryID
              )[0];
              if (!masterGridTerritory.description) {
                console.error('MasterGridTerritory not found');
                return;
              }
              gridTerritoryNames.push(masterGridTerritory.description);
            }
          }
        });
      });
    }

    gridTerritoryNames = gridTerritoryNames.filter(
      (item, pos, ar) => ar.indexOf(item) === pos
    );

    if (gridTerritoryNames.length === 1) {
      uniqueGridTerritoryName = gridTerritoryNames[0];
    } else {
      uniqueGridTerritoryName = '';
    }
    return uniqueGridTerritoryName;
  }
}
