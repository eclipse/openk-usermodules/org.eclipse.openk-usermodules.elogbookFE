/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { ErrorType, StatusEn } from '../../common/enums';
import { SessionContext } from '../../common/session-context';
import { Notification } from '../../model/notification';
import { NotificationService } from '../../services/notification.service';
import { BRANCHES } from '../../test-data/branches';
import {
  FILTER_MATRIX_NONE_SELECTED,
  FILTER_MATRIX_UNIQUE_BRANCH,
  FILTER_MATRIX_UNIQUE_GRIDTERRITORY,
} from '../../test-data/filter-matrix';
import { GRIDTERRITORIES } from '../../test-data/gridterritories';
import {
  FINISHED_NOTIFICATIONS,
  NOTIFICATION_VERSIONS,
  OPEN_NOTIFICATIONS,
} from '../../test-data/notifications';
import { STATUSES } from '../../test-data/statuses';
import { USERS } from '../../test-data/users';
import { click, newEvent } from '../../testing';
import { MockComponent } from '../../testing/mock.component';
import { EntryComponent } from './entry.component';

import { MatDialogRef } from '@angular/material/dialog';
import { DateTimePickerComponent } from 'app/common-components/date-time-picker/date-time-picker.component';
import { MessageService } from '../../services/message.service';
import { catchError, of, throwError } from 'rxjs';

export class EntryComponentMocker {
  public static getComponentMocks() {
    return [
      MockComponent({ selector: 'app-entry' }),
      MockComponent({
        selector: 'app-remove-button',
        inputs: ['nullableAttribute'],
        outputs: ['nullableAttribute'],
      }),
      MockComponent({
        selector: 'app-autocomplete',
        inputs: ['notification'],
        outputs: ['notification'],
      }),
    ];
  }
}

describe('EntryComponent', () => {
  let component: EntryComponent;
  let fixture: ComponentFixture<EntryComponent>;
  let sessionContext: SessionContext;

  class MockDialogRef extends AbstractMockObservableService {
    close() {}
  }

  class MockNotificationService extends AbstractMockObservableService {
    createNotification(newNotification: any) {
      this.content = newNotification;
      return this;
    }

    updateNotification(updatedNotification: any) {
      this.content = updatedNotification;
      return this;
    }

    getNotificationVersions(incidentId: number) {
      console.log(incidentId);
      return of([]);
    }
  }
  let mockNotificationService: any;
  let messageService: any;

  beforeEach(async () => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    sessionContext.setBranches(BRANCHES);
    sessionContext.setGridTerritories(GRIDTERRITORIES);
    sessionContext.setfilterMatrix(FILTER_MATRIX_NONE_SELECTED);
    mockNotificationService = new MockNotificationService();

    TestBed.configureTestingModule({
      imports: [FormsModule, DateTimePickerComponent],
      declarations: [
        EntryComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({
          selector: 'app-autocomplete',
          inputs: ['notification'],
          outputs: ['notification'],
        }),
        MockComponent({
          selector: 'app-remove-button',
          inputs: ['nullableAttribute'],
          outputs: ['nullableAttribute'],
        }),
      ],
      providers: [
        { provide: MatDialogRef, useClass: MockDialogRef },
        DateTimePickerComponent,
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: MessageService, useValue: messageService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntryComponent);
    component = fixture.componentInstance;
    component.user = USERS[1];
    component.sessionContext.setCurrUser(USERS[1]);
    component.sessionContext.setBranches(BRANCHES);
    component.sessionContext.setGridTerritories(GRIDTERRITORIES);
    component.sessionContext.setfilterMatrix(FILTER_MATRIX_NONE_SELECTED);
    component.branches = BRANCHES;
    component.gridTerritories = GRIDTERRITORIES;
  });

  it('should change the Version and set the according notification', async () => {
    component.notificationVersions = NOTIFICATION_VERSIONS;
    // spyOn(mockNotificationService, 'getNotificationVersions').and.returnValue(
    //   of(NOTIFICATION_VERSIONS)
    // );
    component.notification = NOTIFICATION_VERSIONS[0];
    component.isReadOnlyDialog = true;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      const select = fixture.debugElement.query(
        By.css('select[name=notificationSelection]')
      );
      select.nativeElement.target.value = 3;
      select.nativeElement.dispatchEvent(newEvent('change'));
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.notification.version).toBe(
          NOTIFICATION_VERSIONS[2].version
        );
      });
    });
  });

  it('should call statusChanged on changing the status (inWork)', async () => {
    spyOn(component, 'statusChanged').and.callThrough();
    component.sessionContext.setStatuses(STATUSES);
    const currNotification: Notification = NOTIFICATION_VERSIONS[0];
    component.notification = currNotification;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.notification.fkRefNotificationStatus).toBe(
        NOTIFICATION_VERSIONS[0].fkRefNotificationStatus
      );

      const select = fixture.debugElement.query(
        By.css('select[name=statusSelection]')
      );
      select.nativeElement.value = StatusEn.inWork;
      select.nativeElement.dispatchEvent(newEvent('change'));
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        expect(component.statusChanged).toHaveBeenCalledWith(
          StatusEn.inWork.toString(),
          currNotification
        );
      });
    });
  });

  it('should call statusChanged on changing the status (done)', async () => {
    spyOn(component, 'statusChanged').and.callThrough();
    component.sessionContext.setStatuses(STATUSES);
    const currNotification: Notification = NOTIFICATION_VERSIONS[0];
    component.notification = currNotification;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      expect(component.notification.fkRefNotificationStatus).toBe(
        NOTIFICATION_VERSIONS[0].fkRefNotificationStatus
      );

      const select = fixture.debugElement.query(
        By.css('select[name=statusSelection]')
      );
      select.nativeElement.value = StatusEn.done;
      select.nativeElement.dispatchEvent(newEvent('change'));
      fixture.detectChanges();

      fixture.whenStable().then(() => {
        expect(component.statusChanged).toHaveBeenCalledWith(
          StatusEn.done.toString(),
          currNotification
        );
      });
    });
  });

  it('should call setNotification service methode and return an error', async () => {
    const currNotification: Notification = OPEN_NOTIFICATIONS[0];
    currNotification.beginDate = '';
    spyOn(mockNotificationService, 'getNotificationVersions').and.returnValue(
      throwError(() => 'SETNOTIFICATION_ERROR')
    );
    const spy = spyOn(messageService, 'emitError');

    component.isReadOnlyDialog = true;
    fixture.detectChanges();
    component.setNotification(currNotification);
    fixture.detectChanges();

    mockNotificationService
      .getNotificationVersions(currNotification.id)
      .pipe(
        catchError(() => {
          fixture.detectChanges();
          expect(component.notification.id).toBe(currNotification.id);
          expect(component.notificationVersions.length).toBe(0);
          expect(spy).toHaveBeenCalledWith('Versionen', ErrorType.retrieve);
          return [];
        })
      )
      .subscribe();
  });

  it('should call setNotification service methode and set right notificationVersions', async () => {
    const currNotification: Notification = OPEN_NOTIFICATIONS[0];
    spyOn(mockNotificationService, 'getNotificationVersions').and.returnValue(
      of(OPEN_NOTIFICATIONS)
    );
    component.isReadOnlyDialog = true;
    fixture.detectChanges();
    mockNotificationService
      .getNotificationVersions(currNotification.id)
      .subscribe(() => {
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          expect(component.notification.id).toBe(currNotification.id);
          expect(component.notificationVersions.length).toBe(
            OPEN_NOTIFICATIONS.length
          );
        });
      });

    component.setNotification(currNotification);
    fixture.detectChanges();
  });

  it('should call getNotificationVersions service methode and set current user as create user', async () => {
    const currNotification = FINISHED_NOTIFICATIONS[0];
    currNotification.createUser = 'Test 7';
    component.isReadOnlyDialog = true;
    spyOn(mockNotificationService, 'getNotificationVersions').and.returnValue(
      of(FINISHED_NOTIFICATIONS)
    );

    mockNotificationService
      .getNotificationVersions(currNotification.id)
      .subscribe((resp: any) => {
        component.notificationVersions = resp;
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          const des = fixture.debugElement.queryAll(By.css('.nvo'));
          expect(des.length).toBe(FINISHED_NOTIFICATIONS.length);
        });
      });
  });

  it('InstructionDialog: only statusSelection should be reuqired on instructionDialog', async () => {
    component.isInstructionDialog = true;
    fixture.detectChanges();

    let de = fixture.debugElement.query(
      By.css('.form-control[name=statusSelection]')
    );
    expect(de.nativeElement.required).toBe(true);

    de = fixture.debugElement.query(
      By.css('.form-control[name=gridTerritorySelection]')
    );
    expect(de.nativeElement.required).toBe(false);

    de = fixture.debugElement.query(
      By.css('.form-control[name=branchSelection]')
    );
    expect(de.nativeElement.required).toBe(false);
  });

  it('InstructionDialog: should call add on click button "Hinzufügen"', async () => {
    spyOn(component, 'add').and.callThrough();
    component.isInstructionDialog = true;
    fixture.detectChanges();

    component.notification.notificationText = 'TestContent';
    component.notification.fkRefNotificationStatus = 1;
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.btn-success'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.notification.responsibilityControlPoint).toBe(
        USERS[1].name
      );
      expect(component.notification.adminFlag).toBe(true);
    });
  });

  it('should call add on click button "Hinzufügen" and return an error', async () => {
    let hasBeenCalled = false;
    spyOn(component, 'add').and.callThrough();
    component.isInstructionDialog = true;
    mockNotificationService.error = 'ADD_CLICK_ERROR';

    component.notification.notificationText = 'TestContentChanged';
    component.notification.fkRefNotificationStatus = 1;

    messageService.errorOccured$.subscribe(() => (hasBeenCalled = true));
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.btn-success'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.add).toHaveBeenCalled();
      expect(hasBeenCalled).toBeTruthy();
    });
  });

  it('should call edit on click button "Ändern"', async () => {
    spyOn(component, 'edit').and.callThrough();
    component.isInstructionDialog = true;
    component.isEditDialog = true;
    component.notification = OPEN_NOTIFICATIONS[0];
    fixture.detectChanges();

    component.notification.notificationText = 'TestContentChanged';
    component.notification.fkRefNotificationStatus = 1;
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.btn-success'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.edit).toHaveBeenCalled();
    });
  });

  it('should call edit on click button "Ändern" and return an error', async () => {
    let hasBeenCalled = false;
    spyOn(component, 'edit').and.callThrough();
    component.isInstructionDialog = true;
    component.isEditDialog = true;
    mockNotificationService.error = 'EDIT_CLICK_ERROR';

    component.notification.notificationText = 'TestContentChanged';
    component.notification.fkRefNotificationStatus = 1;
    messageService.errorOccured$.subscribe(() => (hasBeenCalled = true));
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('.btn-success'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.edit).toHaveBeenCalled();
      expect(hasBeenCalled).toBeTruthy();
    });
  });

  it('close dialog on cancel', async () => {
    spyOn(component, 'cancel').and.callThrough();
    component.isInstructionDialog = true;
    fixture.detectChanges();

    const de = fixture.debugElement.query(By.css('button:nth-child(2)'));
    click(de);

    fixture.whenStable().then(() => {
      expect(component.cancel).toHaveBeenCalled();
    });
  });

  it('should return an emty branch name for a given user if it is not unique', () => {
    let returnBranchName = '';

    if (!FILTER_MATRIX_NONE_SELECTED.responsibilityContainerMatrix) return;
    returnBranchName = component.getUniqueBranchNameForUser(
      FILTER_MATRIX_NONE_SELECTED.responsibilityContainerMatrix,
      'max'
    );

    expect(returnBranchName).toBe('');
  });

  it('should return a branch name for a given user if it is unique', () => {
    component.sessionContext.setGridTerritories(GRIDTERRITORIES);
    let returnBranchName = '';
    let returnBranchName2 = '';

    if (!FILTER_MATRIX_UNIQUE_BRANCH.responsibilityContainerMatrix) return;
    returnBranchName = component.getUniqueBranchNameForUser(
      FILTER_MATRIX_UNIQUE_BRANCH.responsibilityContainerMatrix,
      'max'
    );
    returnBranchName2 = component.getUniqueBranchNameForUser(
      FILTER_MATRIX_UNIQUE_BRANCH.responsibilityContainerMatrix,
      'pete'
    );
    expect(returnBranchName).toBe('S');
    expect(returnBranchName2).toBe('F');
  });

  it('should return an emty gridterritory name for a given user if it is not unique', () => {
    component.sessionContext.setGridTerritories(GRIDTERRITORIES);
    let returnGridTerritoryName = '';

    if (!FILTER_MATRIX_NONE_SELECTED.responsibilityContainerMatrix) return;

    returnGridTerritoryName = component.getUniqueBranchNameForUser(
      FILTER_MATRIX_NONE_SELECTED.responsibilityContainerMatrix,
      'max'
    );
    expect(returnGridTerritoryName).toBe('');
  });

  it('should return a gridterritory name for a given user if it is unique', () => {
    component.sessionContext.setGridTerritories(GRIDTERRITORIES);

    let returnGridTerritoryName = '';
    let returnGridTerritoryName2 = '';
    let returnGridTerritoryName3 = '';

    if (!FILTER_MATRIX_UNIQUE_GRIDTERRITORY.responsibilityContainerMatrix)
      return;
    returnGridTerritoryName = component.getUniqueGridTerritoryNameForUser(
      FILTER_MATRIX_UNIQUE_GRIDTERRITORY.responsibilityContainerMatrix,
      'max'
    );
    expect(returnGridTerritoryName).toBe('Mannheim');

    returnGridTerritoryName2 = component.getUniqueGridTerritoryNameForUser(
      FILTER_MATRIX_UNIQUE_GRIDTERRITORY.responsibilityContainerMatrix,
      'pete'
    );
    expect(returnGridTerritoryName2).toBe('Kassel');

    returnGridTerritoryName3 = component.getUniqueGridTerritoryNameForUser(
      FILTER_MATRIX_UNIQUE_GRIDTERRITORY.responsibilityContainerMatrix,
      'otto'
    );
    expect(returnGridTerritoryName3).toBe('Offenbach 1');
  });
});
