/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { AuthenticationService } from '../../services/authentication.service';
import { MessageService } from '../../services/message.service';
import { LogoutComponent } from './logout.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

class MockAuthService extends AbstractMockObservableService {
  logout() {
    return of('');
  }
}

class MockDialogRef extends AbstractMockObservableService {
  config: MdDialogConfigMock = new MdDialogConfigMock();
  close() {}
}

class MdDialogConfigMock {
  data: any;
}

class MockMainNavigation {
  logout() {}
}

describe('LogoutComponent', () => {
  let component: LogoutComponent;
  let fixture: ComponentFixture<LogoutComponent>;
  const routerStub: FakeRouter = {
    navigate: jasmine.createSpy('navigate').and.callThrough(),
  };
  let messageService;
  let mockAuthService: any;
  let mockDialog: MockDialogRef;

  beforeEach(async () => {
    mockAuthService = new MockAuthService();
    mockDialog = new MockDialogRef();
    messageService = new MessageService();

    TestBed.configureTestingModule({
      declarations: [LogoutComponent],
      providers: [
        { provide: AuthenticationService, useValue: mockAuthService },
        { provide: MatDialogRef, useValue: mockDialog },
        { provide: Router, useValue: routerStub },
        { provide: MessageService, useValue: messageService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogoutComponent);
    component = fixture.componentInstance;
  });

  it('should navigate to logout page on abmelden click', async () => {
    const configMock: MdDialogConfigMock = new MdDialogConfigMock();
    const mockMainNav: MockMainNavigation = new MockMainNavigation();
    configMock.data = mockMainNav;
    mockDialog.config = configMock;

    mockAuthService.content = of('');
    fixture.detectChanges();

    component.logout();
    fixture.detectChanges();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/logout']);
  });

  it('shouldnt navigate to logout page on error', async () => {
    const configMock: MdDialogConfigMock = new MdDialogConfigMock();
    const mockMainNav: MockMainNavigation = new MockMainNavigation();
    configMock.data = mockMainNav;
    mockDialog.config = configMock;

    mockAuthService.error = 'MOCKERROR';
    fixture.detectChanges();

    component.logout();
    fixture.detectChanges();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/logout']);
  });
});
