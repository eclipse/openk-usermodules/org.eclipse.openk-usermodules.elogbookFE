/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, Optional } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { MessageDefines, MessageService } from '../../services/message.service';
import { catchError, take } from 'rxjs';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css'],
})
export class LogoutComponent {
  constructor(
    @Optional() public dialogRef: MatDialogRef<LogoutComponent>,
    private msgService: MessageService,
    private authService: AuthenticationService,
    private router: Router
  ) {}

  logout() {
    this.authService
      .logout()
      .pipe(
        take(1),
        catchError((error: any) => {
          console.log(error);
          return error;
        })
      )
      .subscribe(() => {
        this.msgService.loginLogoff$.emit(MessageDefines.MSG_LOG_OFF);
        this.dialogRef.close();
        this.router.navigate(['/logout']);
      });
  }
}
