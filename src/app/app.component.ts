/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Subject, take, takeUntil } from 'rxjs';
import { Globals } from './common/globals';
import { SessionContext } from './common/session-context';
import { User } from './model/user';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { ImportFileCallerService } from './services/jobs/import-file-caller.service';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { MessageDefines, MessageService } from './services/message.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'app works!';
  private endOfSubscription$: Subject<void> = new Subject<void>();

  constructor(
    private http: HttpClient,
    public router: Router,
    private baseDataLoaderService: BaseDataLoaderService,
    private activatedRoute: ActivatedRoute,
    public httpInterceptor: HttpResponseInterceptorService,
    private sessionContext: SessionContext,
    private messageService: MessageService,
    private reminderCallerJobService: ReminderCallerJobService,
    private importFileCallerService: ImportFileCallerService
  ) {
    this.baseDataLoaderService.onLoginLogoff();
    this.reminderCallerJobService.onLoginLogoff();
    this.importFileCallerService.onLoginLogoff();

    this.http
      .get('assets/settings.json')
      .pipe(take(1), takeUntil(this.endOfSubscription$))
      .subscribe(res => (this.sessionContext.settings = res));

    this.sessionContext.centralHttpResultCode$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe((rc: number) => {
        this.onRcFromHttpService(rc);
      });
  }

  ngOnInit() {
    this.extractTokenFromParameters();
  }

  ngOnDestroy(): void {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  private processAccessToken(accessToken: string) {
    const jwtHelper: JwtHelperService = new JwtHelperService();
    const decoded = jwtHelper.decodeToken(accessToken);
    const user: User = new User();
    user.id = decoded.sub;
    user.username = decoded.preferred_username;
    user.itemName = decoded.preferred_username;
    let firstName = decoded.given_name;
    if (!firstName) {
      firstName = '';
    }
    let lastName = decoded.family_name;
    if (!lastName) {
      lastName = '';
    }

    user.name = firstName + ' ' + lastName;

    if (
      decoded.realm_access.roles.filter(
        (role: string) => role === Globals.OAUTH2CONF_SUPERUSER_ROLE
      ).length >= 1
    ) {
      user.specialUser = true;
    }

    this.sessionContext.setCurrUser(user);
    this.sessionContext.setAccessToken(accessToken);
  }

  /**
   * Extract the params (suscribe to router event) and store them in the sessionContext.
   */
  private extractTokenFromParameters() {
    this.activatedRoute.params
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => {
        const accessToken = this.getParametersFromUrl();

        if (accessToken) {
          this.processAccessToken(accessToken);
        }
        this.messageService.loginLogoff$.emit(
          MessageDefines.MSG_LOG_IN_SUCCEEDED
        );
      });
  }

  private getParametersFromUrl() {
    const parameter = window.location.search.substr(1);
    return parameter != null && parameter !== ''
      ? this.readParamAccessToken(parameter)
      : null;
  }

  private readParamAccessToken(prmstr: string) {
    const params: { [key: string]: string } = {}; // add index signature
    const prmarr = prmstr.split('&');
    for (let i = 0; i < prmarr.length; i++) {
      const tmparr = prmarr[i].split('=');
      params[tmparr[0]] = tmparr[1];
    }
    return params['accessToken'];
  }

  //handle different adresses for redirect (test, local, production environment)
  private onRcFromHttpService(rc: number): void {
    if (rc === 401) {
      this.router.navigate(['/logout']);
    }
  }
}
