/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AutocompleteComponent } from './common-components/autocomplete/autocomplete.component';
import { DateTimePickerComponent } from './common-components/date-time-picker/date-time-picker.component';
import { DateTimeRangePickerComponent } from './common-components/date-time-range-picker/date-time-range-picker.component';
import { MainNavigationComponent } from './common-components/main-navigation/main-navigation.component';
import { MessageBannerComponent } from './common-components/message-banner/message-banner.component';
import { ClickOutsideDirective } from './common-components/multiselect-dropdown/clickOutside';
import { ListFilterPipe } from './common-components/multiselect-dropdown/list-filter';
import { AngularMultiSelectComponent } from './common-components/multiselect-dropdown/multiselect.component';
import { FormattedDatePipe } from './common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from './common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from './common-components/pipes/string-to-date.pipe';
import { RemoveButtonComponent } from './common-components/remove-button/remove-button.component';
import { VersionInfoComponent } from './common-components/version-info/version-info.component';
import { SessionContext } from './common/session-context';
import { AlertComponent } from './dialogs/alert/alert.component';
import { EntryComponent } from './dialogs/entry/entry.component';
import { FileImportComponent } from './dialogs/file-import/file-import.component';
import { LoadingSpinnerComponent } from './dialogs/loading-spinner/loading-spinner.component';
import { LogoutComponent } from './dialogs/logout/logout.component';
import { ResponsibilityComponent } from './dialogs/responsibility/responsibility.component';
import { ShiftChangeProtocolComponent } from './dialogs/shift-change-protocol/shift-change-protocol.component';
import { ShiftChangeComponent } from './dialogs/shift-change/shift-change.component';
import { FilterComponent } from './filter/filter.component';
import { AbstractListComponent } from './lists/abstract-list/abstract-list.component';
import { CurrentRemindersComponent } from './lists/current-reminders/current-reminders.component';
import { FinishedNotificationsComponent } from './lists/finished-notifications/finished-notifications.component';
import { FutureNotificationsComponent } from './lists/future-notifications/future-notifications.component';
import { HistoricalShiftChangesComponent } from './lists/historical-shift-changes/historical-shift-changes.component';
import { OpenNotificationsComponent } from './lists/open-notifications/open-notifications.component';
import { SearchResultListComponent } from './lists/search-result-list/search-result-list.component';
import { SortingComponent } from './lists/sorting/sorting.component';
import { LogoutPageComponent } from './pages/logout/logout.component';
import { OverviewComponent } from './pages/overview/overview.component';
import { ReminderComponent } from './pages/reminder/reminder.component';
import { SearchComponent } from './pages/search/search.component';
import { ShiftChangeOverviewComponent } from './pages/shift-change-overview/shift-change-overview.component';
import { AuthenticationService } from './services/authentication.service';
import { BaseDataService } from './services/base-data.service';
import { HttpResponseInterceptorService } from './services/http-response-interceptor.service';
import { ImportService } from './services/import.service';
import { BaseDataLoaderService } from './services/jobs/base-data-loader.service';
import { ImportFileCallerService } from './services/jobs/import-file-caller.service';
import { ReminderCallerJobService } from './services/jobs/reminder-caller-job.service';
import { MessageService } from './services/message.service';
import { NotificationService } from './services/notification.service';
import { ReminderService } from './services/reminder.service';
import { ResponsibilityService } from './services/responsibility.service';
import { SearchResultService } from './services/search-result.service';
import { UserService } from './services/user.service';
import { VersionInfoService } from './services/version-info.service';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    FutureNotificationsComponent,
    OpenNotificationsComponent,
    FinishedNotificationsComponent,
    SearchResultListComponent,
    ResponsibilityComponent,
    EntryComponent,
    ShiftChangeComponent,
    SearchComponent,
    ShiftChangeProtocolComponent,
    LogoutComponent,
    AlertComponent,
    VersionInfoComponent,
    StringToDatePipe,
    FormattedDatePipe,
    FormattedTimestampPipe,
    AbstractListComponent,
    AngularMultiSelectComponent,
    ClickOutsideDirective,
    ListFilterPipe,
    AutocompleteComponent,
    MainNavigationComponent,
    ReminderComponent,
    ShiftChangeOverviewComponent,
    HistoricalShiftChangesComponent,
    CurrentRemindersComponent,
    FilterComponent,
    LoadingSpinnerComponent,
    SortingComponent,
    FileImportComponent,
    MessageBannerComponent,
    LogoutPageComponent,
    RemoveButtonComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    DateTimeRangePickerComponent,
    DateTimePickerComponent,
    HttpClientModule,
    MatDialogModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
  ],
  providers: [
    NotificationService,
    AuthenticationService,
    ResponsibilityService,
    ReminderService,
    SearchResultService,
    VersionInfoService,
    UserService,
    SessionContext,
    BaseDataService,
    ReminderCallerJobService,
    ImportFileCallerService,
    MessageService,
    BaseDataLoaderService,
    ImportService,
    HttpResponseInterceptorService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
