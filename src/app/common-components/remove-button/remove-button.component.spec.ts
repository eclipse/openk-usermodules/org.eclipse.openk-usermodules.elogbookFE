/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RemoveButtonComponent } from './remove-button.component';

describe('RemoveButtonComponent', () => {
  let component: RemoveButtonComponent;
  let fixture: ComponentFixture<RemoveButtonComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [RemoveButtonComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RemoveButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should set the bound model to null', async () => {
    let emit = false;
    expect(component).toBeTruthy();
    fixture.detectChanges();
    component.nullableAttribute = 'INIT TEXT';
    component.nullableAttributeChange.subscribe(
      changedText => (emit = changedText === null)
    );

    component.clear();

    fixture.whenStable().then(() => {
      expect(emit).toBeTruthy();
    });
  });
});
