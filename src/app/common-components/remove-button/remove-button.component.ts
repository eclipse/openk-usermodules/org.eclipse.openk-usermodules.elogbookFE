/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-remove-button',
  templateUrl: './remove-button.component.html',
  styleUrls: ['./remove-button.component.css'],
})
export class RemoveButtonComponent {
  @Input()
  public nullableAttribute: any;

  @Output()
  public nullableAttributeChange: EventEmitter<any> = new EventEmitter<any>();

  public clear() {
    this.nullableAttribute = null;
    this.nullableAttributeChange.emit(this.nullableAttribute);
  }
}
