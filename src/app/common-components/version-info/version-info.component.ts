/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, OnInit } from '@angular/core';
import { Globals } from 'app/common/globals';
import { VersionInfo } from '../../model/version-info';
import { VersionInfoService } from '../../services/version-info.service';

@Component({
  selector: 'app-version-info',
  templateUrl: './version-info.component.html',
  styleUrls: ['./version-info.component.css'],
})
export class VersionInfoComponent implements OnInit {
  currVersion: VersionInfo = {
    frontendVersion: '?',
    backendVersion: '?',
    dbVersion: '?',
  };

  constructor(private _btbService: VersionInfoService) {}

  ngOnInit() {
    this._btbService.loadBackendServerInfo().subscribe(
      vinfo => this.setVersionInfo(vinfo),
      error => console.log(error)
    );
  }

  private setVersionInfo(vinfo: VersionInfo) {
    this.currVersion = vinfo;
    this.currVersion.frontendVersion = Globals.FRONTEND_VERSION;
  }
}
