/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { Globals } from 'app/common/globals';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { VersionInfoService } from '../../services/version-info.service';
import { VersionInfoComponent } from './version-info.component';

describe('VersionInfoComponent', () => {
  let component: VersionInfoComponent;
  let fixture: ComponentFixture<VersionInfoComponent>;
  let de: DebugElement; // the DebugElement with the welcome message
  let el: HTMLElement; // the DOM element with the welcome message
  class MockBtbService extends AbstractMockObservableService {
    loadBackendServerInfo() {
      return this;
    }
  }
  let mockService: any;

  beforeEach(async () => {
    mockService = new MockBtbService();

    TestBed.configureTestingModule({
      declarations: [VersionInfoComponent],
      providers: [{ provide: VersionInfoService, useValue: mockService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionInfoComponent);
    component = fixture.componentInstance;
  });

  it('should show VersionString after loadBackendServerInfo', () => {
    const vinfo = {
      frontendVersion: Globals.FRONTEND_VERSION,
      backendVersion: '1.0',
      dbVersion: '2.0',
    };
    mockService.content = vinfo;
    de = fixture.debugElement.query(By.css('.version-info'));
    el = de.nativeElement;

    fixture.detectChanges();

    const targetString =
      vinfo.frontendVersion +
      ' / ' +
      vinfo.backendVersion +
      ' / ' +
      vinfo.dbVersion;
    expect(el.textContent).toContain(targetString);
  });

  it('should show ??? while not init', () => {
    mockService.error = 'any Error';
    de = fixture.debugElement.query(By.css('.version-info'));
    el = de.nativeElement;

    fixture.detectChanges();

    const targetString = '? / ? / ?';
    expect(el.textContent).toContain(targetString);
  });
});
