/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialog } from '@angular/material/dialog';
import { RouterTestingModule } from '@angular/router/testing';
import { MessageService } from 'app/services/message.service';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { MockComponent } from '../../testing/mock.component';
import { Router } from '../../testing/router-stubs';
import { MainNavigationComponent } from './main-navigation.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('MainNavigationComponent', () => {
  let component: MainNavigationComponent;
  let fixture: ComponentFixture<MainNavigationComponent>;
  const routerStub: FakeRouter = {
    navigate: jasmine.createSpy('navigate').and.callThrough(),
  };
  let sessionContext: SessionContext;

  class MockShiftChangeDialog extends AbstractMockObservableService {
    componentInstance = {
      isFetchingResp: false,
      isChangingShift: false,
    };
    afterClosed() {
      return this;
    }
  }

  beforeEach(async () => {
    sessionContext = new SessionContext();
    TestBed.configureTestingModule({
      imports: [RouterTestingModule.withRoutes([])],
      declarations: [
        MainNavigationComponent,
        MockComponent({ selector: 'input', inputs: ['options'] }),
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: MatDialog, useClass: MatDialog },
        { provide: SessionContext, useValue: sessionContext },
        { provide: MessageService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainNavigationComponent);
    component = fixture.componentInstance;
  });

  it('should navigate to overview on home-button click', () => {
    component.goToOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should navigate to search on search-button click', () => {
    component.goToSearchPage();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/search']);
  });

  it('should navigate to reminder on calendar-button click', () => {
    component.goToReminderPage();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/reminders']);
  });

  it('should navigate to shiftChangeOverview on clock-button click', () => {
    component.goToShiftChangeOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/shiftChangeOverview']);
  });

  it('should navigate to mainview on logout', () => {
    spyOn(sessionContext, 'clearStorage').and.callThrough();
    component.logout();
    expect(sessionContext.clearStorage).toHaveBeenCalled();
  });

  it('should navigate to ShiftChange Dialog', async () => {
    const mocker = new MockShiftChangeDialog();
    mocker.componentInstance.isChangingShift = false;
    mocker.componentInstance.isFetchingResp = false;

    spyOn(component.dialog, 'open').and.callFake(() => mocker);
    component.openDialogShiftChange(false);
    expect(component.dialog.open).toHaveBeenCalled();

    // play the other combinations
    mocker.componentInstance.isChangingShift = true;
    mocker.componentInstance.isFetchingResp = true;
    mocker.content = {};
    component.openDialogShiftChange(true);

    // play the other combinations
    mocker.componentInstance.isChangingShift = false;
    mocker.componentInstance.isFetchingResp = true;
    mocker.content = {};
    component.openDialogShiftChange(true);

    // play the other combinations
    mocker.componentInstance.isChangingShift = true;
    mocker.componentInstance.isFetchingResp = false;
    mocker.content = {};
    component.openDialogShiftChange(false);
  });
});
