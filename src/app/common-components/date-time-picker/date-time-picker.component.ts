/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatDateFormats,
} from '@angular/material/core';
import {
  MatDatepickerInputEvent,
  MatDatepickerModule,
} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as moment from 'moment';

export const GERMAN_MAT_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: 'DD.MM.YYYY HH:mm',
  },
  display: {
    dateInput: 'DD.MM.YYYY HH:mm',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

export type DatePicker = {
  currentDate: moment.Moment;
};

@Component({
  standalone: true,
  selector: 'app-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.css'],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: GERMAN_MAT_DATE_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'de' },
  ],
})
export class DateTimePickerComponent {
  private _currentDate!: moment.Moment | null;
  @Input()
  set currentDate(date: Date | moment.Moment | null) {
    this._currentDate = date ? moment(date) : null;

    this.currentDateformGroup.setValue({
      current: this._currentDate,
      currentHours: this._currentDate?.hours() || 0,
      currentMinutes: this._currentDate?.minutes() || 0,
    });
  }

  get currentDate(): moment.Moment | null {
    return this._currentDate;
  }

  private _disabled = false;
  @Input()
  public get disabled() {
    return this._disabled;
  }
  public set disabled(value) {
    this._disabled = value;
    this._disabled && this.currentDateformGroup.disable();
  }

  private _required = false;
  @Input()
  public get required() {
    return this._required;
  }
  public set required(value) {
    this._required = value;
  }

  @Output()
  public date: EventEmitter<DatePicker> = new EventEmitter<DatePicker>();

  public readonly minHours = 0;
  public readonly maxHours = 23;

  public readonly minMinutes = 0;
  public readonly maxMinutes = 59;

  public readonly hoursValidators: ValidatorFn[] = [
    Validators.min(this.minHours),
    Validators.max(this.maxHours),
    Validators.pattern(/^\d+$/),
  ];

  public readonly minutesValidators: ValidatorFn[] = [
    Validators.min(this.minMinutes),
    Validators.max(this.maxMinutes),
    Validators.pattern(/^\d+$/),
  ];

  public currentDateformGroup = new FormGroup({
    current: new FormControl<moment.Moment | null>(null),
    currentHours: new FormControl<number>(0, this.hoursValidators),
    currentMinutes: new FormControl<number>(0, this.minutesValidators),
  });

  constructor() {}

  public validateInput(event: MatDatepickerInputEvent<moment.Moment>): void {
    if (!event.value) {
      this.currentDateformGroup.setValue({
        current: this._currentDate,
        currentHours: this._currentDate?.hours() || 0,
        currentMinutes: this._currentDate?.minutes() || 0,
      });
    }
  }

  public emitDate(): void {
    let currentDate: moment.Moment | null = null;
    if (this.currentDateformGroup.value.current) {
      currentDate = moment(this.currentDateformGroup.value.current)
        .hour(this.currentDateformGroup.value.currentHours || 0)
        .minute(this.currentDateformGroup.value.currentMinutes || 0);
    }
    if (currentDate) {
      this.date.emit({ currentDate: currentDate });
    } else {
      console.warn('No Date selected');
    }
  }
}
