/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  Component,
  ElementRef,
  EventEmitter,
  HostListener,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';
import { ContactTupel } from 'app/model/contact-tupel';
import { ErrorType } from '../../common/enums';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { Subject, catchError, takeUntil } from 'rxjs';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.css'],
})
export class AutocompleteComponent implements OnDestroy {
  @Input()
  public notification: any = {};

  @Output()
  public notificationChange = new EventEmitter();

  private endOfSubscription$: Subject<void> = new Subject<void>();

  inputElementList: ContactTupel[] = [];
  filteredList: any[] = [];
  item: any;
  items: any[] = [];
  elementRef: any;
  alreadyFocused = false;
  position: number = 0;

  constructor(
    autoCompleteElement: ElementRef,
    private messageService: MessageService,
    private notificationService: NotificationService
  ) {
    this.elementRef = autoCompleteElement;
  }

  ngOnDestroy(): void {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  searchChanged(newValue: string | null) {
    if (newValue === '') {
      newValue = null;
    }

    this.notification.responsibilityForwarding = newValue;
    console.log(this.notification.responsibilityForwarding);

    this.notificationChange.emit(this.notification);
  }

  setAssignedUserSuggestions() {
    this.notificationService
      .getAssignedUserSuggestions()
      .pipe(
        takeUntil(this.endOfSubscription$),
        catchError(() => {
          this.messageService.emitError('Benutzervoschlag', ErrorType.retrieve);
          return [];
        })
      )
      .subscribe(res => {
        this.inputElementList = res;
        this.createItemList();
      });
  }

  filterQuery() {
    this.filteredList = this.items.filter((el: any) => {
      const responsibilityForwarding =
        this.notification.responsibilityForwarding;
      if (responsibilityForwarding) {
        return (
          el.name
            .toLowerCase()
            .indexOf(responsibilityForwarding.toLowerCase()) > -1
        );
      } else {
        return el.name.toLowerCase().indexOf(responsibilityForwarding) > -1;
      }
    });
  }

  filter(event: any) {
    if (this.notification.responsibilityForwarding !== '') {
      if (
        (event.keyCode >= 48 && event.keyCode <= 57) ||
        (event.keyCode >= 65 && event.keyCode <= 90) ||
        event.keyCode === 8
      ) {
        this.position = -1;
        this.filterQuery();
      }
    } else {
      this.filteredList = [];
    }

    for (let i = 0; i < this.filteredList.length; i++) {
      this.filteredList[i].selected = false;
    }

    // Arrow-key Down
    if (event.keyCode === 40) {
      if (this.position + 1 !== this.filteredList.length) {
        this.position++;
      }
    }

    // Arrow-key Up
    if (event.keyCode === 38) {
      if (this.position > 0) {
        this.position--;
      }
    }

    if (this.filteredList[this.position] !== undefined) {
      this.filteredList[this.position].selected = true;
    }

    //enter
    if (event.keyCode === 13) {
      if (this.filteredList[this.position] !== undefined) {
        this.select(this.filteredList[this.position]);
      }
    }

    // Handle scroll positionition of item
    const suggestionsGroup = document.getElementById('suggestions-id');
    const listItem = document.getElementById('true');
    if (listItem && suggestionsGroup) {
      suggestionsGroup.scrollTop = listItem.offsetTop - 100;
    }
  }

  select(item: any) {
    this.filteredList = [];
    this.position = -1;
    if (item.name === '') {
      item.name = null;
      item.uuid = null;
    }

    this.notification.responsibilityForwarding = item.name;
    this.notification.responsibilityForwardingUuid = item.uuid;
    this.notificationChange.emit(this.notification);
  }

  @HostListener('keydown', ['$event'])
  handleKeyDown(event: any) {
    // Prevent default actions of arrows
    if (event.keyCode === 40 || event.keyCode === 38) {
      event.preventDefault();
    }
  }

  handleFocus() {
    if (!this.alreadyFocused) {
      this.setAssignedUserSuggestions();
      this.alreadyFocused = true;
    }
  }

  createItemList() {
    this.inputElementList.forEach(element => {
      this.items.push({
        name: element.contactName,
        uuid: element.contactUuid,
        selected: false,
      });
    });
  }

  @HostListener('document:click', ['$event'])
  handleClick(event: any) {
    let clickedComponent = event.target;
    let inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);
    if (!inside) {
      this.filteredList = [];
    }
    this.position = -1;
  }

  @HostListener('document:keypress', ['$event'])
  handleKeyboardEvent(event: any) {
    let clickedComponent = event.target;
    let inside = false;
    do {
      if (clickedComponent === this.elementRef.nativeElement) {
        inside = true;
      }
      clickedComponent = clickedComponent.parentNode;
    } while (clickedComponent);

    if (inside) {
      if (event.keyCode === 13) {
        if (this.filteredList[this.position] !== undefined) {
          this.select(this.filteredList[this.position]);
        }
        //stops the key event to be propagated
        event.preventDefault();
      }
    }
  }
}
