/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
  ValidatorFn,
  Validators,
} from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import {
  DateAdapter,
  MAT_DATE_FORMATS,
  MAT_DATE_LOCALE,
  MatDateFormats,
} from '@angular/material/core';
import {
  MatDatepickerInputEvent,
  MatDatepickerModule,
} from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import * as moment from 'moment';

export const GERMAN_MAT_DATE_FORMATS: MatDateFormats = {
  parse: {
    dateInput: 'DD.MM.YYYY HH:mm',
  },
  display: {
    dateInput: 'DD.MM.YYYY HH:mm',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};

export type DatePickerRange = {
  startDate: moment.Moment;
  endDate: moment.Moment;
};

@Component({
  standalone: true,
  selector: 'app-date-time-range-picker',
  templateUrl: './date-time-range-picker.component.html',
  styleUrls: ['./date-time-range-picker.component.css'],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatSelectModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatInputModule,
  ],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE],
    },
    { provide: MAT_DATE_FORMATS, useValue: GERMAN_MAT_DATE_FORMATS },
    { provide: MAT_DATE_LOCALE, useValue: 'de' },
  ],
})
export class DateTimeRangePickerComponent {
  private _startDate!: moment.Moment;
  private _endDate!: moment.Moment;

  @Input()
  set startDate(date: Date | moment.Moment) {
    this._startDate = date ? moment(date) : moment();
    if (this._startDate && this.endDate) {
      this.rangeformGroup.setValue({
        start: this._startDate,
        end: this.endDate,
        startHours: this._startDate.hours() || 0,
        startMinutes: this._startDate.minutes() || 0,
        endHours: this.endDate.hours() || 0,
        endMinutes: this.endDate.minutes() || 0,
      });
    }
  }

  get startDate(): moment.Moment {
    return this._startDate;
  }

  @Input()
  set endDate(date: Date | moment.Moment) {
    this._endDate = date ? moment(date) : moment();
    if (this.startDate && this._endDate) {
      this.rangeformGroup.setValue({
        start: this.startDate,
        end: this._endDate,
        startHours: this.startDate.hours() || 0,
        startMinutes: this.startDate.minutes() || 0,
        endHours: this._endDate.hours() || 0,
        endMinutes: this._endDate.minutes() || 0,
      });
    }
  }

  get endDate(): moment.Moment {
    return this._endDate;
  }

  @Output()
  public range: EventEmitter<DatePickerRange> =
    new EventEmitter<DatePickerRange>();

  public readonly minHours = 0;
  public readonly maxHours = 23;

  public readonly minMinutes = 0;
  public readonly maxMinutes = 59;

  public readonly hoursValidators: ValidatorFn[] = [
    Validators.min(this.minHours),
    Validators.max(this.maxHours),
    Validators.pattern(/^\d+$/),
  ];

  public readonly minutesValidators: ValidatorFn[] = [
    Validators.min(this.minMinutes),
    Validators.max(this.maxMinutes),
    Validators.pattern(/^\d+$/),
  ];

  public rangeformGroup = new FormGroup({
    start: new FormControl<moment.Moment>(moment()),
    startHours: new FormControl<number>(0, this.hoursValidators),
    startMinutes: new FormControl<number>(0, this.minutesValidators),
    end: new FormControl<moment.Moment>(moment()),
    endHours: new FormControl<number>(0, this.hoursValidators),
    endMinutes: new FormControl<number>(0, this.minutesValidators),
  });

  constructor() {}

  public emitDateRange(): void {
    let startDate: moment.Moment = moment();
    let endDate: moment.Moment = moment();
    if (this.rangeformGroup.value.start) {
      startDate = moment(this.rangeformGroup.value.start)
        .hour(this.rangeformGroup.value.startHours || 0)
        .minute(this.rangeformGroup.value.startMinutes || 0);
    }
    if (this.rangeformGroup.value.end) {
      endDate = moment(this.rangeformGroup.value.end)
        .hour(this.rangeformGroup.value.endHours || 0)
        .minute(this.rangeformGroup.value.endMinutes || 0);
    }
    this.range.emit({ startDate: startDate, endDate: endDate });
  }

  public validateInput(event: MatDatepickerInputEvent<moment.Moment>): void {
    if (!event.value) {
      this.rangeformGroup.setValue({
        start: this._startDate,
        end: this._endDate,
        startHours: this._startDate?.hours() || 0,
        startMinutes: this._startDate?.minutes() || 0,
        endHours: this._endDate?.hours() || 0,
        endMinutes: this._endDate?.minutes() || 0,
      });
    }
  }
}
