/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
/* tslint:disable:no-unused-variable */
import { Overlay } from '@angular/cdk/overlay';
import { DebugElement, EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { DateTimePickerComponent } from 'app/common-components/date-time-picker/date-time-picker.component';
import { TerritoryResponsibility } from 'app/model/territory-responsibility';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { FilterMocker } from '../../filter/filter.component.spec';
import { AbstractListComponent } from '../../lists/abstract-list/abstract-list.component';
import { AbstractListMocker } from '../../lists/abstract-list/abstract-list.component.spec';
import { FinishedNotificationsMocker } from '../../lists/finished-notifications/finished-notifications.component.spec';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from '../../lists/open-notifications/open-notifications.component.spec';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { OPEN_NOTIFICATIONS } from '../../test-data/notifications';
import { RESPONSIBILITIES } from '../../test-data/responsibilities';
import { click } from '../../testing/index';
import { MockComponent } from '../../testing/mock.component';
import { OverviewComponent } from './overview.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('OverviewComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: OverviewComponent;
  let fixture: ComponentFixture<OverviewComponent>;
  let router: Router;
  let mockNotificationService: MockNotificationService;
  let mockRespService: MockResponsibilityService;
  let messageService: MessageService;

  const correctUser: User = {
    id: 44,
    username: 'carlo',
    name: 'Carlo Cottura',
    specialUser: true,
    itemName: 'carlo',
  };
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getFinishedNotifications(notificationType: string) {
      console.log(notificationType);

      this.loadCalled = true;
      return this;
    }
  }
  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities: TerritoryResponsibility[] = [];
    responsibilities = null;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    }
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
  }

  beforeEach(async () => {
    mockRespService = new MockResponsibilityService();
    mockNotificationService = new MockNotificationService();
    router = new FakeRouter() as any as Router;
    messageService = new MessageService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {});

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        DateTimePickerComponent,
      ],
      declarations: [
        StringToDatePipe,
        FormattedTimestampPipe,
        AbstractListComponent,
        OverviewComponent,
        EntryComponent,
        MainNavigationComponent,
        ShiftChangeProtocolComponent,
        MockComponent({ selector: 'app-version-info' }),
        MockComponent({ selector: 'app-reminder-caller-job-component' }),
        EntryComponentMocker.getComponentMocks(),
        FilterMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] }),
        AbstractListMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FinishedNotificationsMocker.getComponentMocks(),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-responsibility',
          inputs: ['responsiblitySelection'],
        }),
        MockComponent({
          selector: 'app-message-banner',
        }),
      ],
      providers: [
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: Router, useValue: router },
        { provide: Overlay, useClass: Overlay },
        { provide: SessionContext, useValue: sessionContext },
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: ReminderService, useClass: ReminderService },
        { provide: MessageService, useValue: messageService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OverviewComponent);
    component = fixture.componentInstance;
  });

  it('should init correctly with plannedResponsibility if available', async () => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(component.responsibilitiesContainer.length).toBe(
        RESPONSIBILITIES.length
      );
    });
  });

  it('should init correctly with plannedResponsibility and open a shiftChangeProtocoll dialog', async () => {
    let shiftChangeProtocollOpened = false;
    let dialog: any;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      shiftChangeProtocollOpened = true;
    });

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      console.log(shiftChangeProtocollOpened);
      console.log(dialog);

      expect(shiftChangeProtocollOpened).toBe(true);
    });
  });

  it('should init correctly with empty plannedResponsibility', async () => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = [];
    fixture.detectChanges();
    expect(component.responsibilitiesContainer).toEqual([]);
  });

  it('should init correctly with plannedResponsibility not available', async () => {
    sessionContext.setCurrUser(correctUser);
    mockRespService.error = 'error';
    fixture.detectChanges();
    expect(component.responsibilitiesContainer).toEqual([]);
  });

  it('should open dialog to create new notification on button click', async () => {
    let entryOpend = false;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;

    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // wait for async getFinishedNotifications
      fixture.detectChanges(); // update view with array
      const des: DebugElement[] = fixture.debugElement.queryAll(
        By.css('.btn-primary')
      );
      component.dialog.afterOpened.subscribe(() => (entryOpend = true));
      click(des[0]);
      expect(entryOpend).toBe(true);
    });
  });

  it('should open special dialog to create new notification on button click', async () => {
    let entryOpend = false;
    let de: DebugElement;

    sessionContext.setCurrUser(correctUser);
    mockRespService.plannedResponsibilities = RESPONSIBILITIES;
    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(() => {
      entryOpend = true;
    });
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      de = fixture.debugElement.query(By.css('.btn-info'));
      click(de);
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
    });
  });

  it('should open a dialog to edit a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = OPEN_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogEditEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(true);
    });
  });

  it('should open a dialog to view a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = OPEN_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);
    });
  });
});
