/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, HostBinding, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ErrorType, StatusEn } from '../../common/enums';
import { slideInOpacity } from '../../common/router.animations';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { FileImportComponent } from '../../dialogs/file-import/file-import.component';
import { LogoutComponent } from '../../dialogs/logout/logout.component';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { ShiftChangeComponent } from '../../dialogs/shift-change/shift-change.component';
import { Notification } from '../../model/notification';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css'],
  animations: [slideInOpacity()],
})
export class OverviewComponent implements OnInit {
  @HostBinding('@slideInOpacity') get slideInOpacity() {
    return '';
  }
  private dialogConfig = new MatDialogConfig();

  showReminder = true;
  user: User | null = null;
  responsiblitiesRetrieveDone = false;

  responsibilitiesContainer: TerritoryResponsibility[] = [];
  shiftChangeClosed = false;
  shiftChangeOpened = false;
  filterExpanded_ = false;

  constructor(
    public dialog: MatDialog,
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService,
    private messageService: MessageService
  ) {}

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
    this.filterExpanded_ = this.sessionContext.getFilterExpansionState();
    this.getResponsibilities();
    this.messageService.respChangedForCurrentUser$.subscribe((b: number) =>
      this.getResponsibilities()
    );
  }

  private getResponsibilities(): void {
    this.responsibilityService.getPlannedResponsibilities().subscribe(
      resps => {
        this.responsiblitiesRetrieveDone = true;
        if (resps.length > 0) {
          this.responsibilitiesContainer = resps;
          this.openDialogShiftChangeProtocol(resps);
        } else {
          this.responsibilityService.getResponsibilities().subscribe(
            respsCurrent => {
              this.responsibilitiesContainer = respsCurrent;
            },
            error => this.handleGetRespError(error)
          );
        }
      },
      error => this.handleGetRespError(error)
    );
  }

  private handleGetRespError(error: any) {
    console.log(error);
    this.messageService.emitError('Verantwortlichkeiten', ErrorType.retrieve);
    this.responsiblitiesRetrieveDone = true;
  }

  openDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isInstructionDialog = false;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openInstructionDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isInstructionDialog = true;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openPresenceReminderDialogNewEntry() {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.isPresenceReminderDialog = true;
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isEditDialog = false;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openDialogEditEntry(notification: Notification) {
    const notificationObj = Object.assign(new Notification(), notification);
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
    dialogRef.componentInstance.isPresenceReminderDialog =
      notificationObj.isTypePresenceCheck();
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isEditDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openDialogLookUpEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isReadOnlyDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openDialogShiftChange() {
    const dialogRef = this.dialog.open(ShiftChangeComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.isChangingShift) {
        this.openDialogLogout();
      }
    });
  }

  openDialogShiftChangeProtocol(
    plannedRespContainer: TerritoryResponsibility[]
  ) {
    const dialogRef = this.dialog.open(
      ShiftChangeProtocolComponent,
      this.dialogConfig
    );
    this.shiftChangeOpened = true;
    dialogRef.componentInstance.setResponsibilitiesContainer(
      plannedRespContainer
    );
    dialogRef.afterClosed().subscribe(() => {
      this.shiftChangeClosed = true;
      this.shiftChangeOpened = false;
    });
  }

  openDialogDataImport() {
    const dialogRef = this.dialog.open(FileImportComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(() => {
      if (dialogRef.componentInstance.isImportFileSelected) {
        const importNotification = new Notification();
        const importFileModel = dialogRef.componentInstance.importFileModel;
        if (importFileModel) {
          const branches = this.sessionContext.getBranches();
          let branchTmp;
          if (branches) {
            branchTmp = branches.filter(
              b => b.name === importFileModel.branchName
            )[0];
            if (branchTmp) {
              importNotification.fkRefBranch = branchTmp.id;
            }
          }

          const gridTerritories = this.sessionContext.getGridTerritories();
          let gridTerritoryTmp;
          if (gridTerritories) {
            gridTerritoryTmp = gridTerritories.filter(
              gt => gt.name === importFileModel.gridTerritoryName
            )[0];
          }
          if (gridTerritoryTmp) {
            importNotification.fkRefGridTerritory = gridTerritoryTmp.id;
          }

          importNotification.notificationText =
            importFileModel.notificationText;
        }

        importNotification.fkRefNotificationStatus = StatusEn.open;

        const dialogRefEntry = this.dialog.open(
          EntryComponent,
          this.dialogConfig
        );
        dialogRefEntry.componentInstance.isInstructionDialog = false;
        dialogRefEntry.componentInstance.user = this.user;
        dialogRefEntry.componentInstance.isEditDialog = false;

        dialogRefEntry.componentInstance.notification = importNotification;
        dialogRefEntry.afterClosed().subscribe(() => {});
      }
    });
  }

  openDialogLogout() {
    this.dialogConfig.data = this;
    const dialogRef = this.dialog.open(LogoutComponent, this.dialogConfig);
    dialogRef.afterClosed().subscribe(() => {});
  }

  isSpecialUser(): boolean {
    return !!this.user && this.user.specialUser;
  }
}
