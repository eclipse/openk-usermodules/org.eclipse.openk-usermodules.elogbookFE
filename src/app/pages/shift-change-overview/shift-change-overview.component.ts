/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, HostBinding, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, catchError, take, takeUntil } from 'rxjs';
import { BannerMessageStatusEn, ErrorType } from '../../common/enums';
import { slideInOpacity } from '../../common/router.animations';
import { SessionContext } from '../../common/session-context';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';

@Component({
  selector: 'app-shift-change-overview',
  templateUrl: './shift-change-overview.component.html',
  styleUrls: ['./shift-change-overview.component.css'],
  animations: [slideInOpacity()],
})
export class ShiftChangeOverviewComponent implements OnDestroy {
  @HostBinding('@slideInOpacity') get slideInOpacity() {
    return '';
  }
  public responsibilitiesContainer: TerritoryResponsibility[] = [];
  public shiftChangeTransactionId: number = -1;
  public readonly bannerMessageStatus = BannerMessageStatusEn;
  public endofSubscription$: Subject<void> = new Subject<void>();

  shiftChangeDateSelected(scTransId: number) {
    this.shiftChangeTransactionId = scTransId;
    this.retrieveHistoricalResponsibilities(scTransId);
  }

  constructor(
    private messageService: MessageService,
    private router: Router,
    public sessionContext: SessionContext,
    private responsibilityService: ResponsibilityService
  ) {}

  ngOnDestroy(): void {
    this.endofSubscription$.next();
    this.endofSubscription$.complete();
  }

  retrieveHistoricalResponsibilities(transactionId: number): void {
    this.responsibilityService
      .getHistoricalResponsibilities(transactionId)
      .pipe(
        take(1),
        takeUntil(this.endofSubscription$),
        catchError(() => {
          this.messageService.emitError(
            'Verantwortlichkeiten',
            ErrorType.retrieve
          );
          return [];
        })
      )
      .subscribe(resp => {
        this.responsibilitiesContainer = resp;
      });
  }

  gotToOverview() {
    this.router.navigate(['/overview']);
  }
}
