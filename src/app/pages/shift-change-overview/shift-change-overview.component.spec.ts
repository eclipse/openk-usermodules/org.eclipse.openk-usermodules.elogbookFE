/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ErrorType } from 'app/common/enums';
import { ResponsibilityComponent } from 'app/dialogs/responsibility/responsibility.component';
import { FutureNotificationsComponent } from 'app/lists/future-notifications/future-notifications.component';
import { HistoricalShiftChangesComponent } from 'app/lists/historical-shift-changes/historical-shift-changes.component';
import { NotificationService } from 'app/services/notification.service';
import { ReminderService } from 'app/services/reminder.service';
import { UserService } from 'app/services/user.service';
import { of, throwError } from 'rxjs';
import { SessionContext } from '../../common/session-context';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { MessageService } from '../../services/message.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { ShiftChangeOverviewComponent } from './shift-change-overview.component';
import { OpenNotificationsComponent } from 'app/lists/open-notifications/open-notifications.component';
import { FinishedNotificationsComponent } from 'app/lists/finished-notifications/finished-notifications.component';
import { FormsModule } from '@angular/forms';
import { DateTimeRangePickerComponent } from 'app/common-components/date-time-range-picker/date-time-range-picker.component';

describe('ShiftChangeOverviewComponent', () => {
  let component: ShiftChangeOverviewComponent;
  let fixture: ComponentFixture<ShiftChangeOverviewComponent>;
  let responsibilityService: ResponsibilityService;
  let messageService: MessageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        DateTimeRangePickerComponent,
      ],
      declarations: [
        ShiftChangeOverviewComponent,
        HistoricalShiftChangesComponent,
        ResponsibilityComponent,
        FutureNotificationsComponent,
        OpenNotificationsComponent,
        FinishedNotificationsComponent,
      ],
      providers: [
        MessageService,
        ResponsibilityService,
        SessionContext,
        NotificationService,
        ReminderService,
        UserService,
      ],
    });

    fixture = TestBed.createComponent(ShiftChangeOverviewComponent);
    component = fixture.componentInstance;
    responsibilityService = TestBed.inject(ResponsibilityService);
    messageService = TestBed.inject(MessageService);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  it('should retrieve historical responsibilities', () => {
    const mockResponsibilities: TerritoryResponsibility[] = [
      {
        id: 1,
        gridTerritoryDescription: 'responsibility1',
        responsibilityList: [],
      },
      {
        id: 2,
        gridTerritoryDescription: 'responsibility2',
        responsibilityList: [],
      },
    ];

    spyOn(
      responsibilityService,
      'getHistoricalResponsibilities'
    ).and.returnValue(of(mockResponsibilities));
    component.retrieveHistoricalResponsibilities(1);

    expect(component.responsibilitiesContainer).toEqual(mockResponsibilities);
  });

  it('should handle error when retrieving historical responsibilities', () => {
    spyOn(
      responsibilityService,
      'getHistoricalResponsibilities'
    ).and.returnValue(throwError('error'));
    spyOn(messageService, 'emitError');

    component.retrieveHistoricalResponsibilities(1);

    expect(messageService.emitError).toHaveBeenCalledWith(
      'Verantwortlichkeiten',
      ErrorType.retrieve
    );
    expect(component.responsibilitiesContainer).toEqual([]);
  });
});
