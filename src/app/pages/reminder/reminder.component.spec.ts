/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Overlay } from '@angular/cdk/overlay';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { AbstractListMocker } from 'app/lists/abstract-list/abstract-list.component.spec';
import { FinishedNotificationsMocker } from 'app/lists/finished-notifications/finished-notifications.component.spec';
import { FutureNotificationsMocker } from 'app/lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from 'app/lists/open-notifications/open-notifications.component.spec';
import { UserService } from 'app/services/user.service';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { EntryComponentMocker } from '../../dialogs/entry/entry.component.spec';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { CurrentRemindersComponent } from '../../lists/current-reminders/current-reminders.component';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { REMINDER_NOTIFICATIONS } from '../../test-data/reminder-notifications';
import { MockComponent } from '../../testing/mock.component';
import { ReminderComponent } from './reminder.component';
import { DateTimePickerComponent } from 'app/common-components/date-time-picker/date-time-picker.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('ReminderComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: ReminderComponent;
  let fixture: ComponentFixture<ReminderComponent>;
  let router: Router;
  let mockNotificationService: MockNotificationService;
  let mockReminderService: MockReminderService;
  let mockRespService: MockResponsibilityService;
  let messageService;

  const correctUser: User = {
    id: 44,
    username: 'carlo',
    name: 'Carlo Cottura',
    specialUser: true,
    itemName: 'carlo',
  };

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities = null;
    responsibilities = null;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    }
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
  }

  class MockReminderService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    currentReminders = [];

    reminderSearchFilter: ReminderSearchFilter = {
      reminderDate: '2017-08-17 15:00',
      responsibilityFilterList: [],
    };

    public getCurrentReminders(reminderSearchFilter: any) {
      console.log(reminderSearchFilter);

      this.loadCalled = true;
      const reminderService = new MockReminderService();
      reminderService.content = this.currentReminders;
      return reminderService;
    }
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getOpenNotifications(notificationType: string) {
      console.log(notificationType);
      this.loadCalled = true;
      return this;
    }
  }

  let mockUserService;

  beforeEach(async () => {
    messageService = new MessageService();
    mockRespService = new MockResponsibilityService();
    mockReminderService = new MockReminderService();
    mockNotificationService = new MockNotificationService();
    router = new FakeRouter() as any as Router;
    mockUserService = new MockUserService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {});

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        MatDialogModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        DateTimePickerComponent,
      ],
      declarations: [
        ReminderComponent,
        EntryComponent,
        ShiftChangeProtocolComponent,
        MainNavigationComponent,
        CurrentRemindersComponent,
        FormattedTimestampPipe,
        StringToDatePipe,
        MockComponent({
          selector: 'app-filter',
          inputs: [
            'shiftChangeProtocolConfirmed',
            'shiftChangeProtocolOpened',
            'filterExpanded',
          ],
        }),
        MockComponent({ selector: 'app-loading-spinner' }),
        EntryComponentMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({
          selector: 'app-reminder',
          inputs: ['withCheckboxes', 'withEditButtons', 'isCollapsible'],
        }),
        MockComponent({
          selector: 'app-responsibility',
          inputs: ['responsiblitySelection'],
        }),
        AbstractListMocker.getComponentMocks(),
        FinishedNotificationsMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-message-banner',
        }),
      ],
      providers: [
        { provide: Router, useValue: router },
        { provide: SessionContext, useValue: sessionContext },
        { provide: Overlay, useClass: Overlay },
        { provide: ReminderService, useClass: ReminderService },
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: MessageService, useValue: messageService },
        { provide: UserService, useValue: mockUserService },
        { provide: NotificationService, useValue: mockNotificationService },
        HttpClientModule,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReminderComponent);
    component = fixture.componentInstance;
    //fixture.detectChanges();
  });

  it('should init correctly with correct user', async () => {
    sessionContext.setCurrUser(correctUser);

    mockReminderService.currentReminders = [];
    fixture.detectChanges();
    expect(component.user?.id).toBe(correctUser.id);
  });

  it('should open a dialog to edit a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = REMINDER_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogEditEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(true);
    });
  });

  it('should open a dialog to view a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = REMINDER_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);
    });
  });
});
