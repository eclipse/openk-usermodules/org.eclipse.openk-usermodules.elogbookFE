/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, HostBinding, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { BannerMessage } from '../../common/banner-message';
import { BannerMessageStatusEn } from '../../common/enums';
import { slideInOpacity } from '../../common/router.animations';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { Notification } from '../../model/notification';
import { TerritoryResponsibility } from '../../model/territory-responsibility';
import { User } from '../../model/user';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.css'],
  animations: [slideInOpacity()],
})
export class ReminderComponent implements OnInit {
  @HostBinding('@slideInOpacity') get slideInOpacity() {
    return '';
  }
  public readonly bannerMessageStatus = BannerMessageStatusEn;
  private dialogConfig = new MatDialogConfig();
  public user: User | null = null;
  responsiblitiesRetrieveDone = false;
  public bannerMessage: BannerMessage = new BannerMessage();
  responsibilitiesContainer: TerritoryResponsibility[] = [];
  shiftChangeClosed = false;
  shiftChangeOpened = false;
  filterExpanded = false;

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public sessionContext: SessionContext
  ) {}

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
    this.filterExpanded = this.sessionContext.getFilterExpansionState();
  }

  openDialogEditEntry(notification: Notification) {
    const notificationObj = Object.assign(new Notification(), notification);
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
    dialogRef.componentInstance.isPresenceReminderDialog =
      notificationObj.isTypePresenceCheck();
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isEditDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openDialogLookUpEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isReadOnlyDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }
}
