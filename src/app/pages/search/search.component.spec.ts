/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { DebugElement, EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { BrowserDynamicTestingModule } from '@angular/platform-browser-dynamic/testing';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { click } from '../../testing/index';

import { Overlay } from '@angular/cdk/overlay';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { SearchResultService } from 'app/services/search-result.service';
import { UserService } from 'app/services/user.service';
import { USERSETTING_1 } from 'app/test-data/user-settings';
import { MainNavigationComponent } from '../../common-components/main-navigation/main-navigation.component';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { ShiftChangeProtocolComponent } from '../../dialogs/shift-change-protocol/shift-change-protocol.component';
import { AbstractListMocker } from '../../lists/abstract-list/abstract-list.component.spec';
import { FinishedNotificationsMocker } from '../../lists/finished-notifications/finished-notifications.component.spec';
import { FutureNotificationsMocker } from '../../lists/future-notifications/future-notifications.component.spec';
import { OpenNotificationsMocker } from '../../lists/open-notifications/open-notifications.component.spec';
import { SearchResultListComponent } from '../../lists/search-result-list/search-result-list.component';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { SEARCH_RESULT_NOTIFICATIONS } from '../../test-data/search-result-notifications';
import { USERS } from '../../test-data/users';
import { MockComponent } from '../../testing/mock.component';
import { SearchComponent } from './search.component';

class FakeRouter {
  navigate(commands: any[]) {
    return commands[0];
  }
}

describe('SearchComponent', () => {
  const sessionContext: SessionContext = new SessionContext();
  let component: SearchComponent;
  let fixture: ComponentFixture<SearchComponent>;
  let mockNotificationService: MockNotificationService;
  let mockNotificationSearchSortService: MockNotificationSearchSortService;
  let messageService;
  const routerStub: FakeRouter = {
    navigate: jasmine.createSpy('navigate').and.callThrough(),
  };
  let mockUserService;

  const correctUser: User = {
    id: 44,
    username: 'carlo',
    name: 'Carlo Cottura',
    specialUser: true,
    itemName: 'carlo',
  };

  class MockResponsibilityService extends AbstractMockObservableService {
    plannedResponsibilities = null;
    responsibilities = null;

    getPlannedResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.plannedResponsibilities;
      return resptService;
    }
    getResponsibilities() {
      const resptService = new MockResponsibilityService();
      resptService.content = this.responsibilities;
      return resptService;
    }
  }

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    getNotificationVersions(incidentId: number) {
      return incidentId;
    }
  }

  class MockNotificationSearchSortService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    searchResultNotifications = SEARCH_RESULT_NOTIFICATIONS;
  }

  class MockDialogRef extends AbstractMockObservableService {
    close() {}
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  beforeEach(async () => {
    messageService = new MessageService();
    mockNotificationService = new MockNotificationService();
    mockNotificationSearchSortService = new MockNotificationSearchSortService();
    mockUserService = new MockUserService();

    TestBed.overrideModule(BrowserDynamicTestingModule, {});

    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        MatDialogModule,
        HttpClientTestingModule,
      ],
      declarations: [
        SearchComponent,
        EntryComponent,
        ShiftChangeProtocolComponent,
        MainNavigationComponent,
        SearchResultListComponent,
        FormattedTimestampPipe,
        StringToDatePipe,
        MockComponent({ selector: 'app-entry' }),
        MockComponent({
          selector: 'app-responsibility',
          inputs: ['responsiblitySelection'],
        }),
        MockComponent({ selector: 'input', inputs: ['options'] }),
        MockComponent({
          selector: 'app-search',
          inputs: ['withCheckboxes', 'withEditButtons', 'isCollapsible'],
        }),
        MockComponent({
          selector: 'app-autocomplete',
          inputs: ['notification'],
          outputs: ['notification'],
        }),
        MockComponent({ selector: 'app-loading-spinner' }),
        MockComponent({ selector: 'app-date-time-picker' }),
        AbstractListMocker.getComponentMocks(),
        FinishedNotificationsMocker.getComponentMocks(),
        OpenNotificationsMocker.getComponentMocks(true),
        FutureNotificationsMocker.getComponentMocks(),
        MockComponent({
          selector: 'app-message-banner',
        }),
        MockComponent({
          selector: 'app-remove-button',
          inputs: ['nullableAttribute'],
          outputs: ['nullableAttribute'],
        }),
      ],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: MatDialogRef, useClass: MockDialogRef },
        { provide: Overlay, useClass: Overlay },
        { provide: NotificationService, useValue: mockNotificationService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: ReminderService, useClass: ReminderService },
        { provide: ResponsibilityService, useClass: MockResponsibilityService },
        { provide: MessageService, useValue: messageService },
        SearchResultService,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchComponent);
    component = fixture.componentInstance;
    sessionContext.clearStorage();
    sessionContext.setCurrUser(USERS[0]);
    component.sessionContext = sessionContext;
  });

  it('should init correctly with correct user', async () => {
    sessionContext.setCurrUser(correctUser);

    mockNotificationSearchSortService.searchResultNotifications = [];
    fixture.detectChanges();
    expect(component.user?.id).toBe(correctUser.id);
  });

  it('should should navigate to Overview after click at homebbutton', () => {
    spyOn(component, 'goToOverview').and.callThrough();

    fixture.detectChanges();

    fixture.whenStable().then(() => {
      const button =
        fixture.debugElement.nativeElement.querySelector('button#homebutton');
      button.click();
      fixture.detectChanges();
      expect(component.goToOverview).toHaveBeenCalled();
      expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
    });
  });

  it('should navigate to overview on home-button click', () => {
    component.goToOverview();
    expect(routerStub.navigate).toHaveBeenCalledWith(['/overview']);
  });

  it('should open a dialog to view a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = SEARCH_RESULT_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogLookUpEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(false);
    });
  });

  it('should open an dialog to edit a notification on button click', async () => {
    let entryOpend = false;
    let dialog: any;
    const testNotification = SEARCH_RESULT_NOTIFICATIONS[0];

    sessionContext.setCurrUser(correctUser);

    fixture.detectChanges();

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'setNotification');
      entryOpend = true;
    });
    component.openDialogEditEntry(testNotification);

    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(entryOpend).toBe(true);
      expect(dialog.isEditDialog).toBe(true);
    });
  });

  it('should delete search parameters after Suche löschen button pressed', done => {
    let dialog: any;
    const globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
    globalSearchFilter.searchString = 'Ab1224ag';
    globalSearchFilter.responsibilityForwarding = 'Karl Heinz';
    globalSearchFilter.statusOpenSelection = false;
    globalSearchFilter.statusInWorkSelection = false;
    globalSearchFilter.statusDoneSelection = false;
    globalSearchFilter.statusClosedSelection = false;
    globalSearchFilter.fkRefBranch = 2;
    globalSearchFilter.fkRefGridTerritory = 1;
    globalSearchFilter.fastSearchSelected = false;

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'deleteSearch');
    });

    fixture.whenStable().then(() => {
      const button = fixture.debugElement.nativeElement.querySelector(
        'button#deletesearch'
      );
      button.click();
      fixture.detectChanges();

      expect(component.globalSearchFilter.searchString).toBe('');
      expect(component.globalSearchFilter.responsibilityForwarding).toBe('');

      expect(component.globalSearchFilter.statusOpenSelection).toBe(true);
      expect(component.globalSearchFilter.statusInWorkSelection).toBe(true);
      expect(component.globalSearchFilter.statusDoneSelection).toBe(true);
      expect(component.globalSearchFilter.statusClosedSelection).toBe(true);
      expect(component.globalSearchFilter.fkRefBranch).toBe(-1);
      expect(component.globalSearchFilter.fkRefGridTerritory).toBe(-1);
      expect(component.globalSearchFilter.fastSearchSelected).toBe(true);

      done();
    });
  });

  it('should execute fast search and store it in the session context', () => {
    let dialog: any;
    const globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
    globalSearchFilter.searchString = 'a';
    globalSearchFilter.statusOpenSelection = true;
    globalSearchFilter.fastSearchSelected = true;

    component.dialog.afterOpened.subscribe(value => {
      dialog = value.componentInstance;
      spyOn(dialog, 'search');
    });

    fixture.whenStable().then(() => {
      const button =
        fixture.debugElement.nativeElement.querySelector('button#search');
      button.click();
      fixture.detectChanges();
      expect(component.currentSearchFilter).toBe(true);
      expect(sessionContext.getGlobalSearchFilter().fastSearchSelected).toBe(
        true
      );
    });
  });

  it('should call search function after search button pressed', async () => {
    spyOn(component, 'search').and.callThrough();
    sessionContext.setUsersSettings(USERSETTING_1);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
      currentSearchFilter.searchString = 'Test';
      currentSearchFilter.responsibilityForwarding = 'Max';
      currentSearchFilter.statusOpenSelection = false;
      currentSearchFilter.statusInWorkSelection = false;
      currentSearchFilter.statusDoneSelection = false;
      currentSearchFilter.statusClosedSelection = false;
      currentSearchFilter.fkRefBranch = 1;
      currentSearchFilter.fkRefGridTerritory = 2;
      currentSearchFilter.fastSearchSelected = false;
      component.currentSearchFilter = currentSearchFilter;
      const de: DebugElement = fixture.debugElement.queryAll(
        By.css('form button:not(#deletesearch)')
      )[1];
      click(de);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        fixture.detectChanges();
        expect(component.search).toHaveBeenCalled();
        expect(component.sessionContext.getGlobalSearchFilter()).not.toBeNull();
        expect(
          component.sessionContext.getGlobalSearchFilter().searchString
        ).toBe(currentSearchFilter.searchString);
      });
    });
  });
});
