/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, HostBinding, OnInit } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Priority } from 'app/model/priority';
import { BannerMessage } from '../../common/banner-message';
import { BannerMessageStatusEn } from '../../common/enums';
import { slideInOpacity } from '../../common/router.animations';
import { SessionContext } from '../../common/session-context';
import { EntryComponent } from '../../dialogs/entry/entry.component';
import { Branch } from '../../model/branch';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { GridTerritory } from '../../model/gridterritory';
import { Notification } from '../../model/notification';
import { Status } from '../../model/status';
import { User } from '../../model/user';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [slideInOpacity()],
})
export class SearchComponent implements OnInit {
  @HostBinding('@slideInOpacity') get slideInOpacity() {
    return '';
  }
  private dialogConfig = new MatDialogConfig();
  public readonly bannerMessageStatus = BannerMessageStatusEn;
  public user: User | null = null;
  public bannerMessage: BannerMessage = new BannerMessage();
  gridTerritories: GridTerritory[] | null = null;
  branches: Branch[] | null = null;
  statuses: Status[] | null = null;
  priorities: Priority[] | null = null;
  public globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
  public currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();

  constructor(
    private router: Router,
    public dialog: MatDialog,
    public sessionContext: SessionContext
  ) {}

  ngOnInit() {
    this.dialogConfig.disableClose = true;
    this.user = this.sessionContext.getCurrUser();
    this.gridTerritories = this.sessionContext.getMasterGridTerritories();
    this.branches = this.sessionContext.getBranches();
    this.statuses = this.sessionContext.getStatuses();
    this.priorities = this.sessionContext.getPriorities();
    this.currentSearchFilter = this.sessionContext.getGlobalSearchFilter();
  }

  public goToOverview() {
    this.router.navigate(['/overview']);
  }

  openDialogLookUpEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isReadOnlyDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }

  openDialogEditEntry(notification: Notification) {
    const dialogRef = this.dialog.open(EntryComponent, this.dialogConfig);
    const notificationObj = Object.assign(new Notification(), notification);
    dialogRef.componentInstance.user = this.user;
    dialogRef.componentInstance.isInstructionDialog = notification.adminFlag;
    dialogRef.componentInstance.isPresenceReminderDialog =
      notificationObj.isTypePresenceCheck();
    dialogRef.componentInstance.setNotification(notification);
    dialogRef.componentInstance.isEditDialog = true;
    dialogRef.afterClosed().subscribe(() => {});
  }

  search() {
    this.globalSearchFilter = Object.assign({}, this.currentSearchFilter);
    this.sessionContext.setGlobalSearchFilter(this.globalSearchFilter);
  }

  deleteSearch() {
    this.sessionContext.setInitGlobalSearchFilterValues();
    this.currentSearchFilter = this.sessionContext.getGlobalSearchFilter();
    this.globalSearchFilter = this.sessionContext.getGlobalSearchFilter();
  }
}
