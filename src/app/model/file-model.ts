/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
export class NotificationFileModel {
  fileName: string | null = null;
  creationDate: string | null = null;
  creator?: string;
  type?: string;
  size?: number = -1;

  data?: string;

  branchName?: string;
  gridTerritoryName?: string;
  notificationText?: string;

  public constructor(init?: Partial<NotificationFileModel>) {
    Object.assign(this, init);
  }
}
