/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Globals } from 'app/common/globals';
export class Notification {
  id: number | string = -1;
  selected: boolean = false;
  incidentId: number = -1;
  fkRefNotificationPriority: number | null = null;
  status: string | null = null;
  fkRefBranch: number = -1;
  fkRefGridTerritory: number = -1;
  fkRefNotificationStatus: number = -1;
  beginDate: string | null = null;
  notificationText: string | undefined = '';
  freeText: string | null = null;
  freeTextExtended: string | null = null;
  responsibilityForwarding: string | null = null;
  responsibilityForwardingUuid: string | null = null;
  responsibilityControlPoint: string | null = null;
  reminderDate: string | null = null;
  futureDate: string | null = null;
  expectedFinishDate: string | null = null;
  finishedDate: string | null = null;
  createDate: string | null = null;
  modDate: string | null = null;
  modUser: string | null = null;
  createUser: string | null = null;
  version: number = -1;
  clerk: string | null = null;
  adminFlag: boolean = false;
  type: string | null = null;

  decoratorNotificationVersions?: Notification[];
  historyOpen?: boolean;

  public isTypePresenceCheck() {
    if (this.type && this.type === Globals.NOTIFICATION_TYPE_PRESENCE_CHECK) {
      return true;
    }
    return false;
  }
}
