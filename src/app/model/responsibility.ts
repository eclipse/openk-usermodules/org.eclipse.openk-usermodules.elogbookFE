/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
export class Responsibility {
  id: number = -1;
  responsibleUser: string | null = null;
  newResponsibleUser: string | null = null;
  branchName: string | null = null;
  isActive: boolean = false;
  username: string | null = this.responsibleUser;

  public constructor(init?: Partial<Responsibility>) {
    Object.assign(this, init);
  }
}
