/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
export class GlobalSearchFilter {
  searchString: string | null = null;
  responsibilityForwarding: string | null = null;
  responsibilityForwardingUuid: string | null = null;

  statusOpenSelection: boolean = false;
  statusInWorkSelection: boolean = false;
  statusDoneSelection: boolean = false;
  statusClosedSelection: boolean = false;

  fkRefBranch: number = -1;
  fkRefGridTerritory: number = -1;
  fastSearchSelected: boolean = false;
  fkRefNotificationPriority: number = -1;
}
