/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Branch } from './branch';
import { GridTerritory } from './gridterritory';

export class HistoricalResponsibility {
  id: number = -1;
  responsibleUser: string | null = null;
  formerResponsibleUser: string | null = null;
  transferDate: string | null = null;
  transactionId: number = -1;
  createDate: string | null = null;
  createUser: string | null = null;
  modDate: string | null = null;
  modUser: string | null = null;
  refGridTerritory: GridTerritory = new GridTerritory();
  RefBranch: Branch = new Branch();

  public constructor(init?: Partial<HistoricalResponsibility>) {
    Object.assign(this, init);
  }
}
