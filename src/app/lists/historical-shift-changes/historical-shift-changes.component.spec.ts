/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import {
  DatePickerRange,
  DateTimeRangePickerComponent,
} from 'app/common-components/date-time-range-picker/date-time-range-picker.component';
import * as moment from 'moment';
import { of } from 'rxjs';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { DateRange } from '../../model/date-range';
import { ResponsibilityService } from '../../services/responsibility.service';
import { HISTORICAL_SCHIFTCHANGES } from '../../test-data/historical-shiftchanges';
import { click } from '../../testing/index';
import { MockComponent } from '../../testing/mock.component';
import { HistoricalShiftChangesComponent } from './historical-shift-changes.component';

describe('HistoricalShiftChangesComponent', () => {
  let component: HistoricalShiftChangesComponent;
  let fixture: ComponentFixture<HistoricalShiftChangesComponent>;
  let mockRespService: MockResponsibilityService;

  class MockResponsibilityService extends AbstractMockObservableService {
    loadCalled = false;

    getHistoricalShiftChangeList() {
      this.loadCalled = true;
      return of([]);
    }
  }

  let sessionContext: SessionContext;

  beforeEach(async () => {
    sessionContext = new SessionContext();
    mockRespService = new MockResponsibilityService();
    TestBed.configureTestingModule({
      imports: [FormsModule, DateTimeRangePickerComponent],
      declarations: [
        HistoricalShiftChangesComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] }),
      ],
      providers: [
        { provide: ResponsibilityService, useValue: mockRespService },
        { provide: SessionContext, useClass: SessionContext },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalShiftChangesComponent);
    component = fixture.componentInstance;
  });

  it('should call setDefaultDateRange and set the startdate 7 days in the past', () => {
    fixture.detectChanges();
    component.startDate = new Date();
    component.endDate = new Date();
    fixture.detectChanges();
    const tmpDate = new Date();
    tmpDate.setDate(component.endDate.getDate() - 7);
    tmpDate.setHours(0);
    tmpDate.setMinutes(0);
    tmpDate.setSeconds(0);

    console.log(tmpDate);
    console.log(component.startDate);

    fixture.detectChanges();
    const differenceInMilliseconds = Math.abs(
      component.startDate.getTime() - tmpDate.getTime()
    );
    const differenceInDays = differenceInMilliseconds / 86400000;
    expect(differenceInDays).toBeCloseTo(7, -1);
  });

  it('should call retrieveHistoricalResponsibilities and set historicalResponsibilities', async () => {
    spyOn(component, 'retrieveHistoricalResponsibilities').and.callThrough();
    const resps = HISTORICAL_SCHIFTCHANGES;
    mockRespService.content = resps;
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      console.log(mockRespService.loadCalled);
      expect(component.retrieveHistoricalResponsibilities).toHaveBeenCalled();
      expect(component.historicalResponsibilities.length).toBe(
        resps.historicalResponsibilities.length
      );
    });
  });

  it('should call selectShiftChange on click', async () => {
    const resps = HISTORICAL_SCHIFTCHANGES;
    mockRespService.content = resps;
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // wait for async getCurrentReminders
      fixture.detectChanges(); // update view with array
      const des = fixture.debugElement.queryAll(By.css('tr'));
      component.shiftChangeSelectedEmitter.subscribe(
        (transactionId: number) => {
          expect(transactionId).toBe(
            resps.historicalResponsibilities[0].transactionId
          );
          expect(component.selectedRow).toBe(0);
        }
      );
      //3rd 'tr' is one of the rows where the click listener is subscribed on
      click(des[2]);
    });
  });

  it('should store the picked date range in sessioncontext', async () => {
    const mockEvent: DatePickerRange = {
      startDate: moment('2017-09-03T22:00:00.000Z'),
      endDate: moment('2017-09-06T22:00:00.000Z'),
    };
    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // wait for async getResponsibilities
      const dateRange: DateRange | null = sessionContext.getDateRange(
        Globals.DATE_RANGE_HISTORY
      );
      expect(dateRange?.dateFrom).toEqual(mockEvent.startDate.toISOString());
      expect(dateRange?.dateTo).toEqual(mockEvent.endDate.toISOString());
    });
  });

  it('should get the stored date range from sessioncontext', async () => {
    spyOn(component, 'setDefaultDateRange').and.callThrough();
    const mockEvent: DatePickerRange = {
      startDate: moment('2017-09-03T22:00:00.000Z'),
      endDate: moment('2017-09-06T22:00:00.000Z'),
    };

    const startDate = mockEvent.startDate.toDate();
    const endDate = mockEvent.endDate.toDate();

    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // wait for async getResponsibilities
      expect(component.setDefaultDateRange).toHaveBeenCalled();
      expect(component.startDate).toEqual(startDate);
      expect(component.endDate).toEqual(endDate);
    });
  });
});
