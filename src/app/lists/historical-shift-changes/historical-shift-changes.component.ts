/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { DatePickerRange } from 'app/common-components/date-time-range-picker/date-time-range-picker.component';
import { Subject, catchError, take, takeUntil } from 'rxjs';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { DateRange } from '../../model/date-range';
import { HistoricalResponsibility } from '../../model/historical-responsibility';
import { ResponsibilitySearchFilter } from '../../model/responsibility-search-filter';
import { ResponsibilityService } from '../../services/responsibility.service';

@Component({
  selector: 'app-historical-shift-changes',
  templateUrl: './historical-shift-changes.component.html',
  styleUrls: ['./historical-shift-changes.component.css'],
})
export class HistoricalShiftChangesComponent implements OnInit, OnDestroy {
  @Output() shiftChangeSelectedEmitter = new EventEmitter<number>();

  historicalResponsibilities: HistoricalResponsibility[] = [];
  responsibilitySearchFilter = new ResponsibilitySearchFilter();
  selectedRow: number = 0;
  endDate: Date = new Date();
  startDate: Date = new Date();

  private endOfSubscription$ = new Subject<void>();

  constructor(
    protected responsibilityService: ResponsibilityService,
    protected sessionContext: SessionContext
  ) {}

  ngOnInit() {
    this.setDefaultDateRange();
    this.retrieveHistoricalResponsibilities();
  }

  ngOnDestroy(): void {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  retrieveHistoricalResponsibilities(): void {
    this.responsibilitySearchFilter.transferDateFrom =
      this.startDate.toISOString();
    this.responsibilitySearchFilter.transferDateTo = this.endDate.toISOString();

    this.responsibilityService
      .getHistoricalShiftChangeList(this.responsibilitySearchFilter)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          console.log(error);
          return [];
        })
      )
      .subscribe(resp => {
        this.historicalResponsibilities = resp.historicalResponsibilities;
      });
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange | null = this.sessionContext.getDateRange(
      Globals.DATE_RANGE_HISTORY
    );
    if (dateRange && dateRange.dateFrom && dateRange.dateTo) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      this.endDate = new Date();
      this.startDate = new Date();
      this.startDate.setDate(this.endDate.getDate() - 7);

      this.startDate.setHours(0);
      this.startDate.setMinutes(0);
      this.startDate.setSeconds(0);

      this.endDate.setHours(23);
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(59);
    }
  }

  selectShiftChange(
    index: number,
    historicalResponsibility: HistoricalResponsibility
  ) {
    this.selectedRow = index;
    if (historicalResponsibility.transactionId) {
      this.shiftChangeSelectedEmitter.emit(
        historicalResponsibility.transactionId
      );
    } else {
      console.warn(
        'No transactionId found for historicalResponsibility',
        historicalResponsibility
      );
    }
  }

  storeDateRange(range: DatePickerRange): void {
    this.startDate = range.startDate.toDate();
    this.endDate = range.endDate.toDate();
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = range.startDate.toISOString();
    dateRange.dateTo = range.endDate.toISOString();

    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_HISTORY);
  }

  mapUserName(shortUsr: string | null | undefined): string | null {
    if (!shortUsr) {
      console.warn('shortUsr is null');
      return null;
    }
    const userMap = this.sessionContext.getUserMap();
    return userMap
      ? this.sessionContext.getUserMap().findAndRenderUser(shortUsr)
      : shortUsr;
  }
}
