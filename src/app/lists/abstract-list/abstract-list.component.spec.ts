/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
/* tslint:disable:no-unused-variable */
import { EventEmitter, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UserService } from 'app/services/user.service';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { Notification } from '../../model/notification';
import { NotificationSearchFilter } from '../../model/notification-search-filter';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import {
  DUMMY_CREATED_NOTIFICATION,
  DUMMY_NOTIFICATION,
  FINISHED_NOTIFICATIONS,
} from '../../test-data/notifications';
import { MockComponent } from '../../testing/mock.component';
import { AbstractListComponent } from './abstract-list.component';
import { of, throwError } from 'rxjs';

export class AbstractListMocker {
  public static getComponentMocks() {
    return [
      MockComponent({
        selector: 'app-abstract-list',
        inputs: [
          'withCheckboxes',
          'withEditButtons',
          'isCollapsible',
          'stayHidden',
          'gridId',
          'enforceShowReadOnly',
        ],
      }),
    ];
  }
}
describe('AbstractListComponent', () => {
  let component: AbstractListComponent;
  let fixture: ComponentFixture<AbstractListComponent>;
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    public getFinishedNotifications(
      notificationSearchFilter: NotificationSearchFilter
    ) {
      console.log(notificationSearchFilter);

      this.loadCalled = true;
      return of([]);
    }

    public getNotificationVersions(notification: Notification) {
      console.log(notification);

      return of([]);
    }
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  class MockReminderService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getCurrentReminders() {
      // notificationSearchFilter: NotificationSearchFilter
      this.loadCalled = true;
      return this;
    }
  }

  let sessionContext: any;
  let mockReminderService;
  let mockService: any;
  let messageService: any;
  let mockUserService;

  beforeEach(async () => {
    sessionContext = new SessionContext();
    mockService = new MockNotificationService();
    mockReminderService = new MockReminderService();
    messageService = new MessageService();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        AbstractListComponent,
        StringToDatePipe,
        FormattedDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'input', inputs: ['options'] }),
      ],
      providers: [
        { provide: MessageService, useValue: messageService },
        { provide: NotificationService, useValue: mockService },
        { provide: ReminderService, useValue: mockReminderService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AbstractListComponent);
    component = fixture.componentInstance;
  });

  it('should call getNotifications after new notification added', async () => {
    const spy = spyOn(component, 'onItemAdded');
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit([]);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // wait for async getNotifications
      fixture.detectChanges(); // update view with array
      expect(spy).toHaveBeenCalled();
    });
  });

  it('should call getNotificationVersions on retrieveNotificationVersions', async () => {
    mockService.content = FINISHED_NOTIFICATIONS;
    const spy = spyOn(mockService, 'getNotificationVersions').and.returnValue(
      of([])
    );
    // 3 toggle = 1 toggle
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    component.toggleHistoryTab(DUMMY_NOTIFICATION);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      // wait for async getNotifications
      fixture.detectChanges(); // update view with array
      expect(spy).toHaveBeenCalled();
      // clean up
      DUMMY_NOTIFICATION.decoratorNotificationVersions = [];
    });
  });

  it('should set error on retrieveNotificationVersions failure', async () => {
    let hasBeenCalled = false;
    messageService.errorOccured$.subscribe(() => (hasBeenCalled = true));
    spyOn(mockService, 'getNotificationVersions').and.returnValue(
      throwError(() => {})
    );
    mockService.error = 'VERSION_ERROR';
    component.retrieveNotificationVersions(DUMMY_NOTIFICATION);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(hasBeenCalled).toBeTruthy();
    });
  });

  it('should call getHistoricalNotifications after change of shiftChangeTransactionId', async () => {
    spyOn(component, 'getHistoricalNotifications');
    component.notificationSearchFilter = new NotificationSearchFilter();
    component.shiftChangeTransactionId = 666;
    const compUnconvered: any = component;
    compUnconvered.onShiftChangeTransactionChanged();

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId).toBe(
      component.shiftChangeTransactionId
    );
  });

  it('should send "onLookUpNotification"', async () => {
    const testNotification = DUMMY_CREATED_NOTIFICATION;
    let notId: number | string = -1;
    component.lookUpNotificationEmitter.subscribe(not => (notId = not.id));
    component.lookUpNotification(testNotification);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(notId).toBe(testNotification.id);
    });
  });

  it('should call changeAllSelection correctly', () => {
    component.notifications = FINISHED_NOTIFICATIONS;
    component.notifications[1].selected = false;
    component.selectAll = true;
    component.changeAllSelection();
    expect(component.notifications[1].selected).toBe(true);

    component.selectAll = false;
    component.changeAllSelection();
    expect(component.notifications[1].selected).toBe(false);
  });

  it('should call selectChanged correctly', () => {
    // first call empty
    component.notifications = [];
    component.selectionChanged();
    component.selectAll = false;
    component.notifications = FINISHED_NOTIFICATIONS;
    component.notifications[0].selected = true;
    component.notifications[1].selected = true;
    component.notifications[2].selected = true;
    component.selectionChanged();
    expect(component.selectAll).toBe(true);
    component.notifications[1].selected = false;
    component.selectionChanged();
    expect(component.selectAll).toBe(false);
  });

  it('should call getHistoricalNotifications on ngOnChanges event', () => {
    spyOn(component, 'getHistoricalNotifications').and.callThrough();
    component.notificationSearchFilter = new NotificationSearchFilter();
    component.shiftChangeTransactionId = 396;
    component.ngOnChanges({
      shiftChangeTransactionId: new SimpleChange(null, 396, false),
    });

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId).toBe(
      component.shiftChangeTransactionId
    );
  });

  it('should return a new ListHelperTool', () => {
    expect(component.getListHelperTool()).toBeTruthy();
  });
});
