/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { UserSettings } from 'app/model/user-settings';
import { UserService } from 'app/services/user.service';
import { Subject, catchError, takeUntil } from 'rxjs';
import { ErrorType } from '../../common/enums';
import { Globals } from '../../common/globals';
import { ListHelperTool } from '../../common/list-helper-tool';
import { SessionContext } from '../../common/session-context';
import { Notification } from '../../model/notification';
import { NotificationSearchFilter } from '../../model/notification-search-filter';
import { ResponsibilitySearchFilter } from '../../model/responsibility-search-filter';
import { SortingState } from '../../model/sorting-state';
import { User } from '../../model/user';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';

@Component({
  selector: 'app-abstract-list',
  templateUrl: './abstract-list.component.html',
  styleUrls: ['./abstract-list.component.css'],
})
export class AbstractListComponent implements OnInit, OnChanges, OnDestroy {
  @Output() editNotificationEmitter = new EventEmitter<Notification>();
  @Output() lookUpNotificationEmitter = new EventEmitter<Notification>();
  @Input() shiftChangeTransactionId: number = -1;
  @Input() withCheckboxes = false;
  @Input() withDatePicker = true;
  @Input() withEditButtons = true;
  @Input() isCollapsible = true;
  @Input() stayHidden = true;
  @Input() gridId: string | null = null;
  @Input() enforceShowReadOnly = false;
  showSpinner = false;
  globals = Globals;
  selectAll = false;
  defaultList: Notification[] = [];
  notifications: Notification[] = [];
  currentDate = new Date();
  responsibilitySearchFilter = new ResponsibilitySearchFilter();
  notificationSearchFilter = new NotificationSearchFilter();
  endDate: Date = new Date();
  startDate: Date = new Date();
  minDate: Date = new Date();
  maxDate: Date = new Date();
  user: User | null = null;
  collapseStateId: string | null = null;

  sortingState: SortingState = new SortingState();
  protected endOfSubscription$ = new Subject<void>();

  constructor(
    protected messageService: MessageService,
    protected notificationService: NotificationService,
    protected reminderService: ReminderService,
    public sessionContext: SessionContext,
    protected userService: UserService
  ) {}

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['shiftChangeTransactionId'] !== undefined &&
      changes['shiftChangeTransactionId'].currentValue !== undefined
    ) {
      this.onShiftChangeTransactionChanged();
    }
  }

  ngOnDestroy() {
    this.endOfSubscription$.next();
    this.endOfSubscription$.complete();
  }

  ngOnInit() {
    this.init();
  }
  protected init() {
    this.setDefaultDateRange();
    this.user = this.sessionContext.getCurrUser();
    this.collapseStateId = this.gridId + this.globals.COLLAPSE_STATE_ID_EXT;

    const oldStayHiddenVal = this.sessionContext.getCollapseState(
      this.collapseStateId
    );
    if (oldStayHiddenVal != null) {
      this.stayHidden = oldStayHiddenVal;
    }

    this.messageService.matrixFilterChanged$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe((filterList: number[]) => {
        this.onFilterSelectionChanged(filterList);
      });

    this.notificationService.itemAdded$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.onItemAdded());

    this.notificationService.itemChanged$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.onItemChanged());

    this.reminderService.itemAdded$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.onItemAdded());

    this.reminderService.itemChanged$
      .pipe(takeUntil(this.endOfSubscription$))
      .subscribe(() => this.onItemChanged());
  }

  protected setDefaultDateRange(): void {}

  protected onShiftChangeTransactionChanged() {
    this.notificationSearchFilter = new NotificationSearchFilter();
    this.notificationSearchFilter.historicalFlag = true;
    this.notificationSearchFilter.shiftChangeTransactionId =
      this.shiftChangeTransactionId;
    this.getHistoricalNotifications();
  }

  protected onFilterSelectionChanged(responsibilityFilterList: number[]) {
    this.notificationSearchFilter = new NotificationSearchFilter();
    this.notificationSearchFilter.responsibilityFilterList =
      responsibilityFilterList;
    this.getNotifications();
  }

  protected onItemChanged(): void {}

  onItemAdded(): void {}

  protected setNotifications(newNotifications: Notification[]) {
    this.notifications = newNotifications;
    this.defaultList = Object.assign(
      new Array<Notification>(),
      newNotifications
    );

    newNotifications.forEach(notification => {
      if (!notification.incidentId) {
        console.error('incidentId is not set');
        return;
      }
      notification.historyOpen =
        this.sessionContext.getNotificationHistoryExpansionStateById(
          notification.incidentId
        );
      if (notification.historyOpen) {
        this.retrieveNotificationVersions(notification);
      }
    });

    this.processSortingState();
  }

  protected processSortingState() {
    if (!this.gridId) {
      console.warn('gridId is not set');
      return;
    }
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();
    let sortingState;

    if (userSettings.sortingStateMap instanceof Map) {
      sortingState = userSettings.sortingStateMap.get(this.gridId);
    }

    //Default for open notifications
    if (
      this.gridId === Globals.SORTING_STATE_GRID_ID_OPEN_NOTS &&
      !sortingState
    ) {
      sortingState = new SortingState();
      sortingState.column = 'beginDate';
      sortingState.isDesc = false;
      sortingState.counter = 2;
    }

    //Default for others notifications
    if (!sortingState) {
      sortingState = new SortingState();
      sortingState.defaultState = true;
    }

    this.sortingState = sortingState;
    this.sort(undefined);
  }

  protected setNotificationVersions(
    newNotificationsVersions: Notification[],
    notification: Notification
  ) {
    notification.decoratorNotificationVersions =
      newNotificationsVersions.filter(
        notificationsVersion => notificationsVersion.id !== notification.id
      );
  }

  getNotifications(): void {}
  getHistoricalNotifications(): void {}

  retrieveNotificationVersions(notification: Notification): void {
    if (!notification.incidentId) {
      console.error('IncidentId is not set');
      return;
    }
    this.notificationService
      .getNotificationVersions(notification.incidentId)
      .pipe(
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          console.log(error);
          this.messageService.emitError(
            'Meldungsversionen',
            ErrorType.retrieve
          );
          return [];
        })
      )
      .subscribe((nots: Notification[]) =>
        this.setNotificationVersions(nots, notification)
      );
  }

  getNotificationVersions(
    notification: Notification
  ): Notification[] | undefined {
    return notification.decoratorNotificationVersions;
  }

  editNotification(notification: Notification) {
    this.editNotificationEmitter.emit(notification);
  }

  toggleCollapse() {
    if (!this.collapseStateId) {
      console.error('collapseStateId is not set');
      return;
    }
    let currentState = this.sessionContext.getCollapseState(
      this.collapseStateId
    );
    if (!currentState) {
      currentState = this.stayHidden;
    }
    this.sessionContext.setCollapseState(!currentState, this.collapseStateId);
  }

  toggleHistoryTab(notification: Notification) {
    notification.historyOpen = !notification.historyOpen;

    if (!notification.incidentId) {
      console.error('IncidentId is not set');
      return;
    }
    this.sessionContext.setNotificationHistoryExpansionStateById(
      notification.incidentId,
      notification.historyOpen
    );

    if (notification.historyOpen) {
      this.retrieveNotificationVersions(notification);
    }
  }

  lookUpNotification(notification: Notification) {
    this.lookUpNotificationEmitter.emit(notification);
  }

  changeAllSelection(): void {
    if (this.selectAll) {
      for (const info of this.notifications) {
        info.selected = true;
      }
    } else {
      for (const info of this.notifications) {
        info.selected = false;
      }
    }
  }

  selectionChanged(): void {
    if (this.notifications) {
      let allSelected = true;
      for (const info of this.notifications) {
        if (!info.selected) {
          allSelected = false;
          break;
        }
      }
      this.selectAll = allSelected;
    }
  }

  isSpecialUser(): boolean {
    return !!this.user && this.user.specialUser;
  }

  isEnforceShowReadOnly(): boolean {
    return this.enforceShowReadOnly;
  }

  public sort(column: string | undefined) {
    if (column) {
      this.sortingState.isDesc = !this.sortingState.isDesc;
      if (
        this.sortingState.column !== undefined &&
        this.sortingState.column !== column
      ) {
        this.sortingState.counter = 1;
        this.sortingState.isDesc = true;
      } else {
        this.sortingState.counter++;
      }
      this.sortingState.column = column;
    }

    if (
      (this.sortingState.defaultState && !column) ||
      (this.sortingState.counter > 0 && this.sortingState.counter % 3 === 0)
    ) {
      this.sortingState.counter = 0;
      this.notifications = Object.assign(
        new Array<Notification>(),
        this.defaultList
      );
      this.sortingState.defaultState = true;
      this.sortingState.isDesc = false;
    } else {
      this.sortingState.defaultState = false;
      this.sortNotifications();
    }

    if (!column || !this.gridId) return;

    this.updateUserSettingsSorting();
  }

  updateUserSettingsSorting() {
    const userSettings: UserSettings = this.sessionContext.getUsersSettings();

    if (!userSettings.sortingStateMap) {
      userSettings.sortingStateMap = new Map<string, SortingState>();
    }
    if (this.gridId) {
      userSettings.sortingStateMap.set(this.gridId, this.sortingState);
    } else {
      console.warn('GridId is not set');
    }
    this.sessionContext.setUsersSettings(userSettings);

    this.userService
      .postUserSettings(userSettings)
      .pipe(
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          console.log(error);
          return [];
        })
      )
      .subscribe();
  }

  private sortNotifications() {
    if (!this.notifications) return;
    const direction = this.sortingState.isDesc ? 1 : -1;
    this.notifications.sort((a, b) => {
      let a1, b1;
      if (this.sortingState.column) {
        a1 = this.getColumnValue(this.sortingState.column, a);
        b1 = this.getColumnValue(this.sortingState.column, b);
      } else {
        console.error('Column does not exist');
      }
      if (a1 == null) {
        return 1 * direction;
      }
      if (b1 == null) {
        return -1 * direction;
      } else if (a1 < b1) {
        return -1 * direction;
      } else if (a1 > b1) {
        return 1 * direction;
      } else {
        return 0;
      }
    });
  }

  private getColumnValue(columnName: string, notification: Notification) {
    switch (columnName) {
      case 'fkRefBranch':
        if (!notification.fkRefBranch) {
          console.error('fkRefBranch is not set');
          return;
        }
        const branch = this.sessionContext.getBrancheById(
          notification.fkRefBranch
        );

        return branch && branch.name ? branch.name.toLowerCase() : null;
      case 'fkRefNotificationStatus':
        if (!notification.fkRefNotificationStatus) {
          console.error('fkRefNotificationStatus is not set');
          return;
        }
        const status = this.sessionContext.getStatusById(
          notification.fkRefNotificationStatus
        );

        return status && status.name ? status.name.toLowerCase() : null;
      case 'fkRefGridTerritory':
        if (!notification.fkRefGridTerritory) {
          console.error('fkRefGridTerritory is not set');
          return;
        }
        const territory = this.sessionContext.getGridTerritoryById(
          notification.fkRefGridTerritory
        );

        return territory && territory.name
          ? territory.name.toLowerCase()
          : null;
      case 'fkRefNotificationPriority':
        if (notification.fkRefNotificationPriority) {
          const priority = this.sessionContext.getPrioById(
            notification.fkRefNotificationPriority
          );
          return priority && priority.weighting
            ? priority.weighting * -1
            : null;
        }
      default:
        return (notification as any)[columnName]
          ? (notification as any)[columnName].toLowerCase()
          : null;
    }
  }

  public getListHelperTool() {
    return new ListHelperTool();
  }
}
