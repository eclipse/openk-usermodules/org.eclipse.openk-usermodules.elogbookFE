/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component } from '@angular/core';
import { DatePickerRange } from 'app/common-components/date-time-range-picker/date-time-range-picker.component';
import { Notification } from 'app/model/notification';
import { catchError, take, takeUntil } from 'rxjs';
import { ErrorType } from '../../common/enums';
import { Globals } from '../../common/globals';
import { DateRange } from '../../model/date-range';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-future-notifications',
  templateUrl: './future-notifications.component.html',
  styleUrls: [
    './future-notifications.component.css',
    '../abstract-list/abstract-list.component.css',
  ],
})
export class FutureNotificationsComponent extends AbstractListComponent {
  column: string | null = null;

  private getFutureNotifications(): void {
    this.notificationService
      .getFutureNotifications(this.notificationSearchFilter)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          console.log(error);
          this.messageService.emitError(
            'Zukünftige Meldungen',
            ErrorType.retrieve
          );
          return [];
        })
      )
      .subscribe((nots: Notification[]) => {
        this.setNotifications(nots);
        this.showSpinner = false;
      });
    this.showSpinner = true;
  }

  getNotifications(): void {
    this.notificationSearchFilter.dateFrom = this.startDate.toISOString();
    this.notificationSearchFilter.dateTo = this.endDate.toISOString();
    this.getFutureNotifications();
  }

  getHistoricalNotifications(): void {
    this.getFutureNotifications();
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange | null = this.sessionContext.getDateRange(
      Globals.DATE_RANGE_FUTURE
    );
    if (dateRange && dateRange.dateFrom && dateRange.dateTo) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      this.startDate = new Date();
      this.startDate.setHours(24, 0, 0, 0);
      this.minDate = this.startDate;

      this.endDate = new Date();
      this.endDate.setMonth(this.endDate.getMonth() + 1);
      this.endDate.setHours(0, 0, 0, 0);
      this.endDate.setHours(48);
    }
  }

  onItemAdded(): void {
    this.getNotifications();
  }

  onItemChanged(): void {
    this.getNotifications();
  }

  storeDateRange(range: DatePickerRange): void {
    this.startDate = range.startDate.toDate();
    this.endDate = range.endDate.toDate();
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = range.startDate.toISOString();
    dateRange.dateTo = range.endDate.toISOString();

    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_FUTURE);
  }
}
