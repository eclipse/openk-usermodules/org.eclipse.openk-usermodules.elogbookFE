/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { EventEmitter, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { DateTimePickerComponent } from 'app/common-components/date-time-picker/date-time-picker.component';
import { NotificationSearchFilter } from 'app/model/notification-search-filter';
import { UserService } from 'app/services/user.service';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { StatusEn } from '../../common/enums';
import { SessionContext } from '../../common/session-context';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { Notification } from '../../model/notification';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import {
  DUMMY_CREATED_NOTIFICATION,
  DUMMY_UPDATED_NOTIFICATION,
  OPEN_NOTIFICATIONS,
} from '../../test-data/notifications';
import { click } from '../../testing/index';
import { MockComponent } from '../../testing/mock.component';
import { OpenNotificationsComponent } from './open-notifications.component';

export class OpenNotificationsMocker {
  public static getComponentMocks(sortingYN: boolean = false) {
    const ret: any[] = [
      MockComponent({
        selector: 'app-open-notifications',
        inputs: [
          'responsiblitySelection',
          'withCheckboxes',
          'withEditButtons',
          'isCollapsible',
          'stayHidden',
          'enforceShowReadOnly',
          'gridId',
          'shiftChangeTransactionId',
          'withDatePicker',
        ],
      }),
    ];
    if (sortingYN) {
      ret.push(SortingComponentMocker.getComponentMocks());
    }
    return ret;
  }
}

describe('OpenNotificationsComponent', () => {
  let component: OpenNotificationsComponent;
  let fixture: ComponentFixture<OpenNotificationsComponent>;

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    public getOpenNotifications(notificationType: string) {
      console.log(notificationType);

      this.loadCalled = true;
      return this;
    }
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  let mockService: any;
  let messageService: any;
  let mockUserService: any;

  beforeEach(async () => {
    mockService = new MockNotificationService();
    messageService = new MessageService();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        OpenNotificationsComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'app-loading-spinner' }),
        SortingComponentMocker.getComponentMocks(),
      ],
      providers: [
        { provide: ReminderService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: NotificationService, useValue: mockService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: UserService, useValue: mockUserService },
        { provide: ResponsibilityService },
        DateTimePickerComponent,
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OpenNotificationsComponent);
    component = fixture.componentInstance;
  });

  it('should raise edit emitter event when EDIT clicked', async () => {
    const resps = OPEN_NOTIFICATIONS;
    component.ngOnInit();

    component.editNotificationEmitter.subscribe(
      (notification: Notification) => expect(notification).toBe(resps[2])
      // the only one with status 'erledigt'! 'geschlossene' können nicht editiert werden
    );
    mockService.subscribe(() => {
      messageService.matrixFilterChanged$.emit(mockService.content);
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        // wait for async getFinishedNotifications
        // component.showSpinner = true;
        fixture.detectChanges(); // update view with array
        component.editNotificationEmitter.subscribe(
          (notification: Notification) => expect(notification).toBe(resps[0])
        );
        const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        click(des[0]);
      });
    });
    mockService.content = resps;
  });

  it('should retrieve all open notifications', async () => {
    const resps = OPEN_NOTIFICATIONS;

    component.ngOnInit();

    mockService.subscribe(() => {
      messageService.matrixFilterChanged$.emit(mockService.content);
      fixture.whenStable().then(() => {
        const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        expect(des.length).toBe(resps.length);
      });
    });

    mockService.content = resps;
    fixture.detectChanges();
  });

  it('should call getOpenNotifications after new notification added', async () => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => {
      // wait for async getOpenNotifications
      fixture.detectChanges(); // update view with array
      expect(mockService.loadCalled).toBe(true);
    });
  });

  it('should call getOpenNotifications after notification modified', async () => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemChanged$.emit(DUMMY_UPDATED_NOTIFICATION);
    fixture.whenStable().then(() => {
      // wait for async getOpenNotifications
      fixture.detectChanges(); // update view with array

      expect(mockService.loadCalled).toBe(true);
    });
  });

  it('should call getHistoricalNotifications on ngOnChanges event', () => {
    spyOn(component, 'getHistoricalNotifications').and.callThrough();
    component.notificationSearchFilter = new NotificationSearchFilter();
    component.shiftChangeTransactionId = 396;
    component.ngOnChanges({
      shiftChangeTransactionId: new SimpleChange(null, 396, false),
    });

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId).toBe(
      component.shiftChangeTransactionId
    );
  });

  it('should getStatusClass correctly by id', () => {
    spyOn(component, 'getStatusClassById').and.callThrough();
    expect(component.getStatusClassById(3)).toBe('finished');
    expect(component.getStatusClassById(0)).toBe('');
  });

  it('should getReminderStatusClass correctly', () => {
    const reminderDateToday = new Date();
    const reminderDateYesterday = new Date(
      new Date().setDate(reminderDateToday.getDate() - 1)
    );
    const reminderDateTomorrow = new Date(
      new Date().setDate(reminderDateToday.getDate() + 1)
    );
    spyOn(component, 'getReminderStatusClass').and.callThrough();
    expect(
      component.getReminderStatusClass(
        reminderDateToday.toISOString(),
        StatusEn.open
      )
    ).toBe('current-reminder');
    expect(
      component.getReminderStatusClass(
        reminderDateToday.toISOString(),
        StatusEn.inWork
      )
    ).toBe('current-reminder');
    expect(
      component.getReminderStatusClass(
        reminderDateToday.toISOString(),
        StatusEn.done
      )
    ).toBe('');
    expect(
      component.getReminderStatusClass(
        reminderDateToday.toISOString(),
        StatusEn.closed
      )
    ).toBe('');

    expect(
      component.getReminderStatusClass(
        reminderDateYesterday.toISOString(),
        StatusEn.open
      )
    ).toBe('current-reminder');
    expect(
      component.getReminderStatusClass(
        reminderDateYesterday.toISOString(),
        StatusEn.closed
      )
    ).toBe('');
    expect(
      component.getReminderStatusClass(
        reminderDateTomorrow.toISOString(),
        StatusEn.open
      )
    ).toBe('');
    expect(
      component.getReminderStatusClass(
        reminderDateTomorrow.toISOString(),
        StatusEn.closed
      )
    ).toBe('');
  });
});
