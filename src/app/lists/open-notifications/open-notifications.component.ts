/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component } from '@angular/core';
import { ErrorType, StatusEn } from '../../common/enums';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-open-notifications',
  templateUrl: './open-notifications.component.html',
  styleUrls: [
    './open-notifications.component.css',
    '../abstract-list/abstract-list.component.css',
  ],
})
export class OpenNotificationsComponent extends AbstractListComponent {
  public column: string | null = null;

  protected init() {
    super.init();
  }
  getNotifications(): void {
    this.notificationService
      .getOpenNotifications(this.notificationSearchFilter)
      .subscribe(
        nots => {
          this.setNotifications(nots);
          this.showSpinner = false;
        },
        error => {
          console.log(error);
          this.messageService.emitError('Offene Meldungen', ErrorType.retrieve);
        }
      );
    this.showSpinner = true;
  }

  getHistoricalNotifications(): void {
    this.getNotifications();
  }

  onItemAdded(): void {
    this.getNotifications();
  }

  onItemChanged(): void {
    this.getNotifications();
  }

  public getStatusClassById(fkRefStatus: number): string {
    switch (fkRefStatus) {
      case StatusEn.done:
        return 'finished';
      default:
        break;
    }
    return '';
  }

  public getReminderStatusClass(
    reminderDate: string,
    fkRefStatus: number
  ): string {
    let isReminder = false;
    const isOpenOrInWork =
      fkRefStatus === StatusEn.open || fkRefStatus === StatusEn.inWork;

    if (isOpenOrInWork && reminderDate != null && reminderDate.length > 0) {
      isReminder = Date.parse(reminderDate) <= Date.now();
    } else {
      isReminder = false;
    }

    if (isReminder) {
      return 'current-reminder';
    }
    return '';
  }
}
