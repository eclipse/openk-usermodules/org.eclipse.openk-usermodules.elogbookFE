/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component } from '@angular/core';
import { DatePickerRange } from 'app/common-components/date-time-range-picker/date-time-range-picker.component';
import { catchError, take, takeUntil } from 'rxjs';
import { ErrorType, StatusEn } from '../../common/enums';
import { Globals } from '../../common/globals';
import { DateRange } from '../../model/date-range';
import { Notification } from '../../model/notification';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-finished-notifications',
  templateUrl: './finished-notifications.component.html',
  styleUrls: [
    './finished-notifications.component.css',
    '../abstract-list/abstract-list.component.css',
  ],
})
export class FinishedNotificationsComponent extends AbstractListComponent {
  column: string | null = null;

  private getFinishedNotifications(): void {
    this.notificationService
      .getFinishedNotifications(this.notificationSearchFilter)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          console.log(error);
          this.messageService.emitError(
            'Geschlossene Meldungen',
            ErrorType.retrieve
          );
          return [];
        })
      )
      .subscribe((nots: Notification[]) => {
        this.setNotifications(nots);
        this.showSpinner = false;
      });
    this.showSpinner = true;
  }

  getNotifications(): void {
    this.notificationSearchFilter.dateFrom = this.startDate.toISOString();
    this.notificationSearchFilter.dateTo = this.endDate.toISOString();
    this.getFinishedNotifications();
  }

  getHistoricalNotifications(): void {
    this.getFinishedNotifications();
  }

  setDefaultDateRange(): void {
    const dateRange: DateRange | null = this.sessionContext.getDateRange(
      Globals.DATE_RANGE_PAST
    );
    if (dateRange && dateRange.dateFrom && dateRange.dateTo) {
      this.startDate = new Date(dateRange.dateFrom);
      this.endDate = new Date(dateRange.dateTo);
    } else {
      this.startDate = new Date();
      this.startDate.setHours(this.startDate.getHours() - 168);
      this.startDate.setHours(0);
      this.startDate.setMinutes(0);
      this.startDate.setSeconds(0);
      this.endDate = new Date();
      this.endDate.setHours(23);
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(59);
    }
  }

  onItemAdded(): void {
    this.getNotifications();
  }

  onItemChanged(): void {
    this.getNotifications();
  }

  isStatusClosed(notification: Notification): boolean {
    return notification.fkRefNotificationStatus === StatusEn.closed;
  }

  storeDateRange(range: DatePickerRange): void {
    this.startDate = range.startDate.toDate();
    this.endDate = range.endDate.toDate();
    const dateRange: DateRange = new DateRange();
    dateRange.dateFrom = range.startDate.toISOString();
    dateRange.dateTo = range.endDate.toISOString();

    this.sessionContext.setDateRange(dateRange, Globals.DATE_RANGE_PAST);
  }
}
