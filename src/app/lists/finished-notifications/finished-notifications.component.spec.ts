/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { EventEmitter, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import {
  DatePickerRange,
  DateTimeRangePickerComponent,
} from 'app/common-components/date-time-range-picker/date-time-range-picker.component';
import { UserService } from 'app/services/user.service';
import * as moment from 'moment';
import { of } from 'rxjs';
import { FormattedDatePipe } from '../../common-components/pipes/formatted-date.pipe';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { Globals } from '../../common/globals';
import { SessionContext } from '../../common/session-context';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { DateRange } from '../../model/date-range';
import { Notification } from '../../model/notification';
import { NotificationSearchFilter } from '../../model/notification-search-filter';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import {
  DUMMY_CREATED_NOTIFICATION,
  DUMMY_UPDATED_NOTIFICATION,
  FINISHED_NOTIFICATIONS,
} from '../../test-data/notifications';
import { click } from '../../testing/index';
import { MockComponent } from '../../testing/mock.component';
import { FinishedNotificationsComponent } from './finished-notifications.component';

export class FinishedNotificationsMocker {
  public static getComponentMocks() {
    return [
      MockComponent({
        selector: 'app-finished-notifications',
        inputs: [
          'responsiblitySelection',
          'withCheckboxes',
          'withEditButtons',
          'isCollapsible',
          'stayHidden',
          'gridId',
          'enforceShowReadOnly',
          'shiftChangeTransactionId',
          'withDatePicker',
        ],
      }),
    ];
  }
}

describe('FinishedNotificationsComponent', () => {
  let component: FinishedNotificationsComponent;
  let fixture: ComponentFixture<FinishedNotificationsComponent>;

  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;

    public getFinishedNotifications(
      notificationSearchFilter: NotificationSearchFilter
    ) {
      this.loadCalled = true;
      console.log(notificationSearchFilter);

      return of([]);
    }
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  let mockService: any;
  let messageService;
  let sessionContext: SessionContext;
  let mockUserService;

  beforeEach(async () => {
    mockService = new MockNotificationService();
    messageService = new MessageService();
    sessionContext = new SessionContext();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule, DateTimeRangePickerComponent],
      declarations: [
        FinishedNotificationsComponent,
        StringToDatePipe,
        FormattedDatePipe,
        FormattedTimestampPipe,
        MockComponent({ selector: 'app-loading-spinner' }),
        SortingComponentMocker.getComponentMocks(),
        MockComponent({ selector: 'input', inputs: ['options'] }),
      ],
      providers: [
        { provide: MessageService, useValue: messageService },
        { provide: ReminderService, useValue: mockService },
        { provide: NotificationService, useValue: mockService },
        { provide: SessionContext, useValue: sessionContext },
        { provide: UserService, useValue: mockUserService },
        { provide: ResponsibilityService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FinishedNotificationsComponent);
    component = fixture.componentInstance;
  });

  it('should raise edit emitter event when EDIT clicked', async () => {
    const notifications = FINISHED_NOTIFICATIONS;
    component.ngOnInit();

    component.editNotificationEmitter.subscribe(
      (notification: Notification) =>
        expect(notification).toBe(notifications[2])
      // the only one with status 'erledigt'! 'geschlossene' können nicht editiert werden
    );
    mockService.subscribe(() => {
      fixture.detectChanges();
      fixture.whenStable().then(() => {
        // wait for async getFinishedNotifications
        const des = fixture.debugElement.queryAll(By.css('.btn-primary'));
        click(des[0]);
      });
    });
    mockService.content = notifications;
  });

  it('should retrieve all finished notifications', async () => {
    const notifications = FINISHED_NOTIFICATIONS;

    mockService.subscribe(() => {
      fixture.whenStable().then(() => {
        const des = fixture.debugElement.queryAll(
          By.css('.notification_row_testable')
        );
        expect(des.length).toBe(notifications.length);
      });
    });

    mockService.content = notifications;
    fixture.detectChanges();
  });

  it('should call getFinishedNotifications after new notification added', async () => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);

    fixture.whenStable().then(() => {
      // wait for async getFinishedNotifications
      fixture.detectChanges(); // update view with array
      expect(mockService.loadCalled).toBe(true);
    });
  });

  it('should call getFinishedNotifications after notification modified', async () => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load shoul be triggered by emit and not by init
    mockService.itemChanged$.emit(DUMMY_UPDATED_NOTIFICATION);
    fixture.whenStable().then(() => {
      // wait for async getFinishedNotifications
      fixture.detectChanges(); // update view with array

      expect(mockService.loadCalled).toBe(true);
    });
  });

  it('should call getHistoricalNotifications on ngOnChanges event', () => {
    spyOn(component, 'getHistoricalNotifications').and.callThrough();
    component.notificationSearchFilter = new NotificationSearchFilter();
    component.shiftChangeTransactionId = 396;
    component.ngOnChanges({
      shiftChangeTransactionId: new SimpleChange(null, 396, false),
    });

    expect(component.getHistoricalNotifications).toHaveBeenCalled();
    expect(component.notificationSearchFilter.shiftChangeTransactionId).toBe(
      component.shiftChangeTransactionId
    );
  });

  it('should store the picked date range in sessioncontext', async () => {
    const mockEvent: DatePickerRange = {
      startDate: moment('2017-09-03T22:00:00.000Z'),
      endDate: moment('2017-09-06T22:00:00.000Z'),
    };
    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // wait for async getResponsibilities
      const dateRange: DateRange | null = sessionContext.getDateRange(
        Globals.DATE_RANGE_PAST
      );
      expect(dateRange?.dateFrom).toEqual(mockEvent.startDate.toISOString());
      expect(dateRange?.dateTo).toEqual(mockEvent.endDate.toISOString());
    });
  });

  it('should get the stored date range from sessioncontext', () => {
    spyOn(component, 'setDefaultDateRange').and.callThrough();
    const mockEvent: DatePickerRange = {
      startDate: moment('2017-09-03T22:00:00.000Z'),
      endDate: moment('2017-09-06T22:00:00.000Z'),
    };

    const startDate = mockEvent.startDate.toDate();
    const endDate = mockEvent.endDate.toDate();

    component.storeDateRange(mockEvent);
    fixture.detectChanges();

    fixture.whenStable().then(() => {
      // wait for async getResponsibilities
      expect(component.setDefaultDateRange).toHaveBeenCalled();
      expect(component.startDate).toEqual(startDate);
      expect(component.endDate).toEqual(endDate);
    });
  });
});
