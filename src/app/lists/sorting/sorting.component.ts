/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sorting',
  templateUrl: './sorting.component.html',
})
export class SortingComponent {
  @Input() columnName: string | null | undefined = undefined;
  @Input() initColumnName: string | null | undefined = undefined;
  @Input() defaultState: boolean | undefined = undefined;
  @Input() isDesc: boolean | undefined = undefined;
}
