/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Observable, of } from 'rxjs';
import { ErrorType } from '../../common/enums';
import { FilterMatrix } from '../../model/controller-model/filter-matrix';
import { ReminderSearchFilter } from '../../model/reminder-search-filter';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';

@Component({
  selector: 'app-current-reminders',
  templateUrl: './current-reminders.component.html',
  styleUrls: [
    './current-reminders.component.css',
    '../abstract-list/abstract-list.component.css',
  ],
})
export class CurrentRemindersComponent
  extends AbstractListComponent
  implements OnInit
{
  column: string | null = null;
  currentTime$: Observable<Date> = of(new Date());
  private reminderSearchFilter: ReminderSearchFilter =
    new ReminderSearchFilter();

  ngOnInit() {
    this.init();
    this.getNotifications();
  }

  getNotifications(): void {
    this.reminderSearchFilter = new ReminderSearchFilter();
    this.reminderSearchFilter.reminderDate = moment(
      this.currentDate
    ).toISOString();
    const filterMatrixSession = this.sessionContext.getfilterMatrix();
    if (filterMatrixSession) {
      const filterMatrix: FilterMatrix = new FilterMatrix(
        filterMatrixSession.responsibilityContainerMatrix
      );
      this.reminderSearchFilter.responsibilityFilterList =
        filterMatrix.getNumFilterList();
    }

    this.reminderService
      .getCurrentReminders(this.reminderSearchFilter)
      .subscribe(
        nots => {
          this.setNotifications(nots);
          this.showSpinner = false;
        },
        error => {
          console.log(error);
          this.messageService.emitError(
            'Aktuelle Erinnerungen',
            ErrorType.retrieve
          );
        }
      );
    this.showSpinner = true;
  }

  setDefaultDateRange(): void {
    this.startDate = new Date();
    this.startDate.setHours(this.startDate.getHours() - 24);
    this.startDate.setHours(0);
    this.startDate.setMinutes(0);
    this.startDate.setSeconds(0);
    this.endDate = new Date();
    this.endDate.setHours(23);
    this.endDate.setMinutes(59);
    this.endDate.setSeconds(59);
  }

  onItemChanged(): void {
    this.getNotifications();
  }
}
