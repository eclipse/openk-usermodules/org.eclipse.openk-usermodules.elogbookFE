/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UserService } from 'app/services/user.service';
import { ErrorType, StatusEn } from '../../common/enums';
import { SessionContext } from '../../common/session-context';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { Notification } from '../../model/notification';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { SearchResultService } from '../../services/search-result.service';
import { AbstractListComponent } from '../abstract-list/abstract-list.component';
import { take, takeUntil, catchError } from 'rxjs';

@Component({
  selector: 'app-search-result-list',
  templateUrl: './search-result-list.component.html',
  styleUrls: [
    './search-result-list.component.css',
    '../abstract-list/abstract-list.component.css',
  ],
})
export class SearchResultListComponent
  extends AbstractListComponent
  implements OnChanges
{
  column: string | null = null;

  @Input()
  globalSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
  oldNotification: Notification | null = null;
  incidentIdChange = true;

  spinCtrl = false;
  constructor(
    protected messageService: MessageService,
    protected searchResultService: SearchResultService,
    protected notificationService: NotificationService,
    protected reminderService: ReminderService,
    public sessionContext: SessionContext,
    protected userService: UserService
  ) {
    super(
      messageService,
      notificationService,
      reminderService,
      sessionContext,
      userService
    );
  }

  public isStatusClassFinished(fkRefStatus: number | null): boolean {
    switch (fkRefStatus) {
      case StatusEn.done:
        return true;
      default:
        return false;
    }
  }

  getSearchResults(): void {
    this.searchResultService
      .getSearchResults(this.globalSearchFilter)
      .pipe(
        take(1),
        takeUntil(this.endOfSubscription$),
        catchError(error => {
          this.showSpinner = false;
          console.log(error);
          this.messageService.emitError(
            'Suchergebnisliste',
            ErrorType.retrieve
          );
          return [];
        })
      )
      .subscribe(resultNotifications => {
        this.setNotifications(resultNotifications);
        this.showSpinner = false;
      });
    this.showSpinner = true;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes['globalSearchFilter'] !== undefined &&
      changes['globalSearchFilter'].currentValue !== undefined
    ) {
      this.oldNotification = null;
      this.incidentIdChange = false;
      this.getSearchResults();
    }
  }

  onItemChanged(): void {
    this.getSearchResults();
  }

  isBranchGas(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'gas';
  }

  isBranchPower(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'power';
  }

  isBranchWater(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'water';
  }

  isBranchHeating(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'heating';
  }

  isBranchCentralFaultMonitoring(branchId: number): boolean {
    return this.sessionContext.getBranchClassById(branchId) === 'cfm';
  }
}
