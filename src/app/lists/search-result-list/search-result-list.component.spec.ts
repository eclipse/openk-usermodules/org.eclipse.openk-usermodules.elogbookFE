/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 ******************************************************************************
 * Copyright © 2017-2018 PTA GmbH.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 *
 *     http://www.eclipse.org/legal/epl-v10.html
 *
 ******************************************************************************
 */
import { EventEmitter } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { UserService } from 'app/services/user.service';
import { USERSETTING_1 } from 'app/test-data/user-settings';
import { of } from 'rxjs';
import { FormattedTimestampPipe } from '../../common-components/pipes/formatted-timestamp.pipe';
import { StringToDatePipe } from '../../common-components/pipes/string-to-date.pipe';
import { AbstractMockObservableService } from '../../common/abstract-mock-observable.service';
import { SessionContext } from '../../common/session-context';
import { SortingComponentMocker } from '../../lists/sorting/sorting.component.spec';
import { GlobalSearchFilter } from '../../model/global-search-filter';
import { Notification } from '../../model/notification';
import { MessageService } from '../../services/message.service';
import { NotificationService } from '../../services/notification.service';
import { ReminderService } from '../../services/reminder.service';
import { ResponsibilityService } from '../../services/responsibility.service';
import { SearchResultService } from '../../services/search-result.service';
import { BRANCHES } from '../../test-data/branches';
import { GRIDTERRITORIES } from '../../test-data/gridterritories';
import {
  DUMMY_CREATED_NOTIFICATION,
  OPEN_NOTIFICATIONS,
} from '../../test-data/notifications';
import { SEARCH_RESULT_NOTIFICATIONS2 } from '../../test-data/search-result-notifications';
import { STATUSES } from '../../test-data/statuses';
import { MockComponent } from '../../testing/mock.component';
import { SearchResultListComponent } from './search-result-list.component';

describe('SearchResultListComponent', () => {
  let component: SearchResultListComponent;
  let fixture: ComponentFixture<SearchResultListComponent>;
  class MockNotificationService extends AbstractMockObservableService {
    itemChanged$ = new EventEmitter();
    itemAdded$ = new EventEmitter();
    loadCalled = false;
    public getOpenNotifications(notificationType: string) {
      console.log(notificationType);

      this.loadCalled = true;
      return this;
    }
  }

  class MockUserService extends AbstractMockObservableService {
    getUsers() {
      return this;
    }

    getUserSettings() {
      return this;
    }

    postUserSettings() {
      return this;
    }
  }

  class MockSearchResultService extends AbstractMockObservableService {
    public getSearchResults() {
      return of([]);
    }
  }
  let messageService: any;
  let sessionContext: SessionContext;
  let mockService: any;
  let mockSearchResultService: any;
  let mockUserService;

  beforeEach(async () => {
    messageService = new MessageService();
    sessionContext = new SessionContext();
    mockService = new MockNotificationService();
    mockSearchResultService = new MockSearchResultService();
    mockUserService = new MockUserService();

    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [
        SearchResultListComponent,
        StringToDatePipe,
        FormattedTimestampPipe,
        SortingComponentMocker.getComponentMocks(),
        MockComponent({ selector: 'app-loading-spinner' }),
      ],
      providers: [
        { provide: ReminderService, useValue: mockService },
        { provide: MessageService, useValue: messageService },
        { provide: SearchResultService, useValue: mockSearchResultService },
        { provide: NotificationService, useValue: mockService },
        { provide: UserService, useValue: mockUserService },
        { provide: SessionContext, useClass: SessionContext },
        { provide: ResponsibilityService },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResultListComponent);
    component = fixture.componentInstance;
    //component.territoryBranchFilterEmitter = new EventEmitter();
    //MessageService.matrixFilterChanged
  });

  it('should raise edit emitter event when EDIT clicked', async () => {
    const notifications = OPEN_NOTIFICATIONS;
    mockService.content = notifications;
    component.ngOnInit();
    mockService.subscribe(() => {
      fixture.detectChanges();
      //component.territoryBranchFilterEmitter.emit(mockService.content);
      //MessageService.matrixFilterChanged

      fixture.whenStable().then(() => {
        // wait for async method
        fixture.detectChanges(); // update view with array
        component.editNotificationEmitter.subscribe(
          (notification: Notification) =>
            expect(notification).toBe(notifications[0])
        );
      });
    });
  });

  it('should call getSearchResultNotifications after new notification added', async () => {
    fixture.detectChanges();
    mockService.loadCalled = false; //Load should be triggered by emit and not by init
    mockService.itemAdded$.emit(DUMMY_CREATED_NOTIFICATION);
    fixture.whenStable().then(() => {
      fixture.detectChanges(); // update view with array
    });
  });

  it('should set class power on notifications with same branch', async () => {
    sessionContext.setBranches(BRANCHES);
    sessionContext.setGridTerritories(GRIDTERRITORIES);
    sessionContext.setStatuses(STATUSES);
    sessionContext.setUsersSettings(USERSETTING_1);
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      const notifications = SEARCH_RESULT_NOTIFICATIONS2;
      mockSearchResultService.content = notifications;

      const currentSearchFilter: GlobalSearchFilter = new GlobalSearchFilter();
      currentSearchFilter.searchString = 'Test';
      currentSearchFilter.responsibilityForwarding = 'Max';
      currentSearchFilter.statusOpenSelection = false;
      currentSearchFilter.statusInWorkSelection = false;
      currentSearchFilter.statusDoneSelection = false;
      currentSearchFilter.statusClosedSelection = false;
      currentSearchFilter.fkRefBranch = 1;
      currentSearchFilter.fkRefGridTerritory = 2;
      currentSearchFilter.fastSearchSelected = false;
      component.globalSearchFilter = currentSearchFilter;
      let dePower, deGas, deHeating, deWater;

      component.isCollapsible = true;
      component.stayHidden = false;

      component.showSpinner = false;
      mockSearchResultService.subscribe(() => {
        fixture.detectChanges();
        messageService.matrixFilterChanged$.emit(
          mockSearchResultService.content
        );
        fixture.whenStable().then(() => {
          fixture.detectChanges();
          component.getSearchResults();

          component.showSpinner = false;
          fixture.componentRef.changeDetectorRef.detectChanges();

          fixture.detectChanges();

          dePower = fixture.debugElement.nativeElement.querySelector(
            'td.notification-tab-branch.power > span'
          );
          deGas = fixture.debugElement.nativeElement.querySelector(
            'td.notification-tab-branch.gas > span'
          );
          deHeating = fixture.debugElement.nativeElement.querySelector(
            'td.notification-tab-branch.heating > span'
          );
          deWater = fixture.debugElement.nativeElement.querySelector(
            'td.notification-tab-branch.water > span'
          );
          expect(dePower.innerText.toString()).toContain('S');
          expect(deGas.innerText.toString()).toContain('G');
          expect(deHeating.innerText.toString()).toContain('F');
          expect(deWater.innerText.toString()).toContain('W');
        });
      });
    });
  });
});
