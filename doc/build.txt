>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
Frontend:
>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

Voraussetzungen:
----------------
NodeJs (bei uns in der Version 6.10.0)
(Chrome Browser)


Bauen:
------
Im Rootpath des Frontend-Projektes:
>npm install --save-dev @angular/cli@latest
>npm install
>ng test --single-run

ggf.:
>ng build -prod
>ng test --code-coverage --single-run

(Erzeugt im Unterverzeichnis "coverage" die CodeAbdeckung in Form von HTMLs->"index.html". In "unittests" liegt 
unter "Chrome*" die Datei test-results.xml)